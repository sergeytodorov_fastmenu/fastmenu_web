<div class="fmn-overlay fmn-otl mod-new">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">

		<div class="overlay-header">
			<h3 class="header-title">Order Type & Location</h3>
			<a href="#" class="header-close"><span class="fmn-icon-delete"></span></a>
		</div>
		<div class="overlay-content">
				


				<div><a href="dynamic.php?page=otlEmpty" class="secondary">OTL Empty</a></div>
				<div><a href="dynamic.php?page=otlDeliveryNotLoggedIn" class="secondary">OTL Delivery Not Logged-In</a></div>
				<div><a href="dynamic.php?page=otlDeliveryLoggedIn" class="secondary">OTL Delivery Logged-In</a></div>
				<div><a href="dynamic.php?page=otlTakeAway" class="secondary">OTL Take-Away</a></div>			
				<div><a href="dynamic.php?page=otlTakeAwaySingleLocation" class="secondary">OTL Take-Away Single Location</a></div>



		</div><!-- overlay-content -->
		<div class="overlay-footer">
		</div><!-- overlay-footer -->

	</div><!-- fmn-overlay-box -->

</div><!-- fmn-overlay -->