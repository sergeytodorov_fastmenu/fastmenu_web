

<div class="debug debug-overlay" id="debug-pages">
	
	<div class="debug-overlay-col">

		<span>Main</span>
<!--
			<a href="dynamic.php?page=root" class="secondary">Root</a>
			<a href="dynamic.php?page=branches" class="secondary">Branches</a>
			<a href="dynamic.php?page=locations" class="secondary">Locations</a>
			<a href="dynamic.php?page=start" class="secondary">Start</a>
-->
			<a href="dynamic.php?page=menus" class="secondary">Menus</a>	
			<a href="dynamic.php?page=categories" class="secondary">Categories</a>
			<a href="dynamic.php?page=dishes&view=tilesCaptionInside" class="secondary">Dishes</a>
			<a href="dynamic.php?page=dishes-new" class="secondary">Dishes New</a>
			<a href="dynamic.php?page=details" class="secondary">Details</a>
			<a href="dynamic.php?page=details-all" class="secondary">Details All</a>
		<span></span>
		<span>Basket</span>
			<a href="dynamic.php?page=basket" class="secondary">Basket</a>
			<a href="dynamic.php?page=basket-empty" class="secondary">Basket Empty</a>
		<span></span>
		<span>Checkout</span>
<!-- 			<a href="dynamic.php?page=checkout-delivery-address" class="secondary">1.  Address</a> -->
			<a href="dynamic.php?page=checkout-complete-address&view=guest" class="secondary">1. Complete Address</a>
			<a href="dynamic.php?page=checkout-order-details&view=core" class="secondary">2. Order Details</a>
			<a href="dynamic.php?page=checkout-contact-info" class="secondary">3. Contact Info</a>
			<a href="dynamic.php?page=checkout-payment-details&view=core" class="secondary">4. Payment Details</a>
			<a href="dynamic.php?page=review-order" class="secondary">5. Review Order</a>
			<a href="dynamic.php?page=thank-you" class="secondary">Thank You</a>
		<span></span>
		<span>Account</span>
			<a href="dynamic.php?page=account-main" class="secondary">Account</a>
			<a href="dynamic.php?page=account-personal-info" class="secondary">Personal Info</a>
			<a href="dynamic.php?page=account-login-methods" class="secondary">Login Methods</a>
			<a href="dynamic.php?page=account-password" class="secondary">Password</a>
			<a href="dynamic.php?page=account-my-addresses" class="secondary">My Addresses</a>
			<a href="dynamic.php?page=account-edit-address" class="secondary">Edit Address</a>
			<a href="dynamic.php?page=account-my-orders" class="secondary">My Orders</a>
			<a href="dynamic.php?page=account-order-details" class="secondary">Orders Details</a>
			<a href="dynamic.php?page=account-payment-details" class="secondary">Payment Details</a>
			<a href="dynamic.php?page=account-payment-defaults" class="secondary">Payment Defaults</a>
			<a href="dynamic.php?page=account-company-invoice-details" class="secondary">Invoice Details</a>
			<a href="dynamic.php?page=account-company-ordering" class="secondary">Company Ordering</a>
		<span></span>

	</div>
	
	<div class="debug-overlay-col">
		<span>Order Type & Location</span>
<!-- 		<a href="dynamic.php?page=otl">OTL</a> -->
		<a href="dynamic.php?page=otlEmpty" class="secondary">Empty</a>
		<a href="dynamic.php?page=otlDeliveryNotLoggedIn" class="secondary">Delivery Not Logged-In</a>
		<a href="dynamic.php?page=otlDeliveryLoggedIn" class="secondary">Delivery Logged-In</a>
		<a href="dynamic.php?page=otlTakeAway" class="secondary">Take-Away</a>
		<a href="dynamic.php?page=otlTakeAwaySingleLocation" class="secondary">Take-Away Single Location</a>
		<span></span>
		<span>Login & Registration</span>
			<a href="dynamic.php?page=login" class="secondary">Login</a>
			<a href="dynamic.php?page=register" class="secondary">Register</a>
			<a href="dynamic.php?page=register-company-details" class="secondary">Register Company Details</a>
			<a href="dynamic.php?page=request-access" class="secondary">Request Access</a>
			<a href="dynamic.php?page=enter-code" class="secondary">Enter Code</a>
			<a href="dynamic.php?page=code-contact-us" class="secondary">Contact Us for Code</a>
		<span></span>
		<span>Other</span>
		<a href="dynamic.php?page=reserve-or-preorder" class="secondary">Reserve or Preorder</a>
		<a href="dynamic.php?page=reservation" class="secondary">Reservation</a>
		<a href="dynamic.php?page=elements" class="secondary">Elements</a>
		<a href="dynamic.php?page=events" class="secondary">Event Announcements</a>
		<a href="dynamic.php?page=event-details" class="secondary">Event Details</a>
		<a href="dynamic.php?page=promos" class="secondary">Promo Announcements</a>
		<a href="dynamic.php?page=promo-details" class="secondary">Promo Details</a>
		<span></span>
		<span>Overlays</span>
<!-- 		<a href="dynamic.php?page=overlay" class="secondary">Overlays</a> -->
		<a href="dynamic.php?page=overlayModifiers" class="secondary">Modifiers</a>
		<a href="dynamic.php?page=overlayModifiersAll" class="secondary">Modifiers All</a>
		<a href="dynamic.php?page=overlayFilter" class="secondary">Filter</a>
		<a href="dynamic.php?page=overlayPlainContent" class="secondary">Plain Content</a>

	</div>
	
</div>

<div class="debug debug-panel debug-panel-top-left">
	<a href="javascript:void(0);" id="debug-trigger-pages">Pages</a>
	<? if ($views == "available") { ?>
		<a href="javascript:void(0);" id="debug-trigger-views">Views</a>
	<? } ?>
	<? if ($fmn_module_otl == "available") { ?>
		<a href="javascript:void(0);" id="debug-trigger-otl">OTL</a>
	<? } ?>

</div>

<!--
<div class="debug debug-panel debug-panel-bottom-right">
	<span class="debug-viewportSize"></span>
	<a href="javascript:void(0);" id="debug-trigger-overlay-grid">Grid</a>
</div>

<div class="debug debug-overlay-grid" id="debug-overlay-grid">
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
	<div class="debug-overlay-grid-line"></div>
</div>
-->

<script>
						
	$(document).ready(function() {

		debugGridVisible = 0;
        $("#debug-trigger-overlay-grid").click(function () {
	        if (debugGridVisible == 0) { 
				$("#debug-overlay-grid").css("display", "block");
				debugGridVisible = 1;
				console.log("showing grid");
			} else if (debugGridVisible == 1) {
				$("#debug-overlay-grid").css("display", "none");
				debugGridVisible = 0;
				console.log("hiding grid");
			}
        });
        
        
        debugPagesVisible = 0;
        
        $("#debug-trigger-pages").click(function () {
	        if (debugPagesVisible == 0) { 
				$("#debug-pages").css("display", "block");
				debugPagesVisible = 1;
			} else if (debugPagesVisible == 1) {
				$("#debug-pages").css("display", "none");
				debugPagesVisible = 0;
			}
        });
        
        debugViewsVisible = 0;
        
        $("#debug-trigger-views").click(function () {
	        if (debugViewsVisible == 0) { 
				$("#debug-views").css("display", "block");
				debugViewsVisible = 1;
			} else if (debugViewsVisible == 1) {
				$("#debug-views").css("display", "none");
				debugViewsVisible = 0;
			}
        });
        
        debugOtlVisible = 0;
        
        $("#debug-trigger-otl").click(function () {
	        if (debugOtlVisible == 0) { 
				$("#debug-otl").css("display", "block");
				debugOtlVisible = 1;
			} else if (debugViewsVisible == 1) {
				$("#debug-otl").css("display", "none");
				debugOtlVisible = 0;
			}
        });        
        
        $("#debug-pages").click(function () {
			$("#debug-pages").css("display", "none");
			debugPagesVisible = 0;
        });
        
        $("#debug-views").click(function () {
			$("#debug-views").css("display", "none");
			debugViewsVisible = 0;
        });
        
        $("#debug-otl").click(function () {
			$("#debug-otl").css("display", "none");
			debugOtlVisible = 0;
        });
        
	});
	
    $(window).on('resize load', function () { 
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		$(".debug-viewportSize").text( windowWidth + "×" + windowHeight + "px "/*  + breakPoint */);
    });

	$(document).ready(function() {
        $(".debug-clickTap").click(function() {
	        alert('Tap!');
        });
	});
	
</script>