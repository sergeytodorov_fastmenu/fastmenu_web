<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">

	<div class="fmn-blocks m-straightLinks m-twoCols">
			
		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-personal-info" class="i-block">
				<span class="e-icon fmn-icon-profile"></span><h4>Personal Info</h4>
			</a>
		</div>
		
		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-login-methods" class="i-block">
				<span class="e-icon fmn-icon-keys"></span><h4>Login Methods</h4>
			</a>
		</div>
		
		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-password" class="i-block">
				<span class="e-icon fmn-icon-password"></span><h4>Password</h4>
			</a>
		</div>
		
		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-my-addresses" class="i-block">
				<span class="e-icon fmn-icon-address-book"></span><h4>My Addresses</h4>
			</a>
		</div>
		
		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-my-orders" class="i-block">
				<span class="e-icon fmn-icon-details"></span><h4>My Orders</h4>
			</a>
		</div>

		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-payment-details" class="i-block">
				<span class="e-icon fmn-icon-wallet"></span><h4>Payment Details</h4>
			</a>
		</div>
		
		<div class="i-block-wrap">
			<a href="dynamic.php?page=account-company-ordering" class="i-block">
				<span class="e-icon fmn-icon-office"></span><h4>Company Ordering</h4>
			</a>
		</div>

	</div>

	<div class="fmn-blocks m-straightLinks m-twoCols" style="margin-top: 20px;">
				
		<div class="i-block-wrap" style="text-align: left;">
			<a href="dynamic.php?page=root" class="i-block">
				<span class="e-icon fmn-icon-exit"></span><h4>Log Out</h4>
			</a>
		</div>
		
	</div><!-- fmn-blocks -->

</div>