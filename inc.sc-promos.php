<div class="fmn-screen-content-wrap m-reducedWidth-oneHalf">

	<div class="fmn-announcements">
		
		<? for ($i = 0; $i < 3; $i++) { ?>
		
		<div class="i-announcement-wrap">
			
			<div class="i-announcement">
				
				<img src="images/sample-category.jpg">
				


				<h2>Promo Announcement Title</h2>

				<div class="n-dateTimeLocation">
					<span class="n-dateTime">from August 1 to August 30 2016</span> <span class="n-location">at all locations</span>
				</div>

				<div class="n-actions">
					<a href="#" class="fmn-button m-invert">Go to Menu</a>
				</div>
				
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
				
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>

				<p><a href="<?=$fmn_next_page_url?>">Read more...</a></p>				
				
			</div>
			
		</div>
		
		<? } ?>
		
	</div>

</div>
