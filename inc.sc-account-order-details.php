<?
	$views = "available";
	
	$allowedViews = array(
    	"delivery" => array( 
	    	"visibleView" => "delivery"
    	),
    	"takeAway" => array( 
	    	"visibleView" => "takeAway"
    	),
    	"delayedDelivery" => array( 
	    	"visibleView" => "delayedDelivery"
    	),
    	"all" => array( 
	    	"visibleView" => "all"
    	)
	);

	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];
	
?>

<script>

	$(document).ready(function() {
		visibleView = "<?=$fmn_visibleView?>";
		defaultView = "delivery";
		console.log(visibleView);
		console.log(defaultView);
		if (visibleView == "") {
			console.log("here!");	
			$(".debug-view").addClass("debug-view-hidden");
			$(".debug-view-"+defaultView).removeClass("debug-view-hidden");	
			$(".debug-view-link").removeClass("selected");
			$(".debug-view-link-"+defaultView).addClass("selected");
		} else {
			$(".debug-view").addClass("debug-view-hidden");
			$(".debug-view-"+visibleView).removeClass("debug-view-hidden");	
			$(".debug-view-link").removeClass("selected");
			$(".debug-view-link-"+visibleView).addClass("selected");

		}
	});
	
	function setView(myView) {
		$(".debug-view").addClass("debug-view-hidden");
		$(".debug-view-"+myView).removeClass("debug-view-hidden");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+myView).addClass("selected");			
	}

</script>

<style>
	.debug-view-hidden {
		display: none !important;
	}
</style>

<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('delivery');" class="debug-view-link debug-view-link-delivery">Delivery <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('delayedDelivery');" class="debug-view-link debug-view-link-delayedDelivery">Delayed Delivery <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('takeAway');" class="debug-view-link debug-view-link-takeAway">Take Away <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('all');" class="debug-view-link debug-view-link-all">All <span class="debug-visible">Current</span></a>
</div>








<!--


			<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delivery">

				<h3 class="fmn-section-title mod-huge"><span>Order Status</span></h3>

				<div class="fmn-steps">
					
					<a href="dynamic.php?page=checkout-complete-address&view=guest" class="i-step num-1 mod-completed">
						<span class="step-graphic">
							<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="3.5 13.5 3.5 23.5 9.5 23.5 9.5 16.5 14.5 16.5 14.5 23.5 20.5 23.5 20.5 14"></polyline>
						            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="0.5 13 12 1.5 23.5 13"></polyline>
						            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="16 2.5 19.5 2.5 19.5 6"></polyline>
							    </g>
							</svg>
							<? include("icon.steps-checkmark.php"); ?>
						</span>
						<span class="step-label">Submitted</span>
					</a>
					
					<span class="i-separator num-1"><? include("icon.steps-arrow.php"); ?></span>
					
					<a href="dynamic.php?page=checkout-order-details&view=core" class="i-step num-2 mod-completed">
						<span class="step-graphic">
							<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						            <circle id="Oval" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" cx="12" cy="12" r="11.5"></circle>
						            <path d="M18,18 L13.057,13.059" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <circle id="Oval" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" cx="12" cy="12" r="1.5"></circle>
						            <path d="M11.8,5 L11.8,10.583" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
							    </g>
							</svg>
							<? include("icon.steps-checkmark.php"); ?>
						</span>
						<span class="step-label">Confirmed</span>
					</a>
					
					<span class="i-separator num-2"><? include("icon.steps-arrow.php"); ?></span>
					
					<a href="dynamic.php?page=checkout-contact-info" class="i-step num-3 mod-completed">
						<span class="step-graphic">
							<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="3.5" y="0.5" width="17" height="23"></rect>
						            <path d="M3.5,0.5 L1.5,0.5 C0.95,0.5 0.5,0.95 0.5,1.5 L0.5,22.5 C0.5,23.05 0.95,23.5 1.5,23.5 L3.5,23.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <path d="M13,12 L13,13.5 L15.538,14.226 C16.067,14.377 16.5,14.95 16.5,15.5 L16.5,18.5 L6.5,18.5 L6.5,15.5 C6.5,14.95 6.933,14.377 7.461,14.226 L10,13.5 L10,12" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
						            <ellipse id="Oval" stroke="#000000" stroke-linejoin="round" cx="11.5" cy="9.271" rx="3" ry="3.272"></ellipse>
						            <path d="M14.469,8.676 C13.969,9.176 12.539,9.174 12,8.171 C11,9.171 9.375,9.171 8.566,8.6" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
						            <path d="M20.5,2 L21.5,2 C22.6,2 23.5,2.9 23.5,4 L23.5,7 L20.5,7" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
						            <polyline id="Shape" stroke="#000000" stroke-linejoin="round" points="20.5 7 23.5 7 23.5 12 20.5 12"></polyline>
						            <polyline id="Shape" stroke="#000000" stroke-linejoin="round" points="20.5 12 23.5 12 23.5 17 20.5 17"></polyline>
						            <path d="M20.5,17 L23.5,17 L23.5,20 C23.5,21.1 22.6,22 21.5,22 L20.5,22" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
							    </g>
							</svg>
							<? include("icon.steps-checkmark.php"); ?>
						</span>
						<span class="step-label">Kitchen</span>
					</a>
					
					<span class="i-separator num-3"><? include("icon.steps-arrow.php"); ?></span>
					
					<a href="dynamic.php?page=checkout-payment-details&view=core" class="i-step num-4 mod-current">
						<span class="step-graphic">
							<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						            <path d="M1.297,1.453 L14.59,4.772 C16.191,5.172 17.5,6.849 17.5,8.5 L17.5,20.5 C17.5,22.15 16.186,23.19 14.58,22.812 L3.42,20.188 C1.814,19.809 0.5,18.15 0.5,16.5 L0.5,3.5 C0.5,1.849 1.85,0.5 3.5,0.5 L20.5,0.5 C22.15,0.5 23.5,1.849 23.5,3.5 L23.5,14.5 C23.5,16.15 22.15,17.5 20.5,17.5 L17.5,17.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <path d="M9.5,3.5 L19.5,3.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <circle id="Oval" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" cx="13" cy="13.5" r="2"></circle>
						            <path d="M17.5,8.5 L19.5,8.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
							    </g>
							</svg>
							<? include("icon.steps-checkmark.php"); ?>
						</span>
						<span class="step-label">On Its Way</span>
					</a>
					
					<span class="i-separator num-4"><? include("icon.steps-arrow.php"); ?></span>
					
					<a href="dynamic.php?page=checkout-payment-details&view=core" class="i-step num-5 mod-upcoming">
						<span class="step-graphic">
							<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="7 5 4 8 2.5 6.5"></polyline>
						            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="7 10.5 4 13.5 2.5 12"></polyline>
						            <polygon id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="9.5 21.5 0.5 21.5 0.5 2.5 14.5 2.5 14.5 16.5"></polygon>
						            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="9.5 21.5 9.5 16.5 14.5 16.5"></polyline>
						            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="17.499" y="8.5" width="4.001" height="10.5"></rect>
						            <path d="M21.498,1.501 C22.603,1.501 23.498,2.396 23.498,3.5 L23.5,11.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <path d="M19.5,21 L19.5,23.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="17.499" y="0.5" width="3.999" height="8"></rect>
						            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="18.5" y="19" width="2" height="2"></rect>
						            <path d="M8.5,7.5 L12,7.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						            <path d="M8.5,12.5 L12,12.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
							    </g>
							</svg>
							<? include("icon.steps-checkmark.php"); ?>
						</span>
						<span class="step-label">Completed<span>
					</a>
	
				</div>
				
				<div class="fmn-statusMobile">

					<div class="statusMobile-title"><h3>Your order is:</h3></div>
									
					<div class="fmn-steps mod-statusMobile statusMobile-Steps">
						
						<div class="i-step mod-completed">
							<span class="status-checkmark"><? include("icon.steps-checkmark.php"); ?></span><span class="step-label">Submitted</span>
						</div>
						<div class="i-step mod-completed">
							<span class="status-checkmark"><? include("icon.steps-checkmark.php"); ?></span><span class="step-label">Confirmed</span>
						</div>
						<div class="i-step mod-completed">
							<span class="status-checkmark"><? include("icon.steps-checkmark.php"); ?></span><span class="step-label">Kitchen</span>
						</div>
						<div class="i-step mod-current">
							<span class="status-arrow"><? include("icon.steps-arrow.php"); ?></span><span class="step-label">On Its Way</span>
						</div>
						
					</div>
				
				</div>

			</div>
			
			
			
			
-->
			
				
							
							

							
							

							
							

							
							
							
							

			
			
			<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delivery">

<!-- 				<h3 class="fmn-section-title mod-huge"><span>Order Status</span></h3> -->

				<div class="fmn-orderStatus">
					
					<div class="orderStatus-title"><h3>Your order is:</h3></div>
	
					<div class="fmn-steps mod-orderStatus orderStatus-steps">
						
						<span class="i-step num-1 mod-completed">
							<span class="step-graphic">
								<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
								        <polygon points="4.5 23.5 6.5 21.5 8.5 23.5 10.5 21.5 12.5 23.5 14.5 21.5 16.5 23.5 18.5 21.5 20.5 23.5 20.5 0.5 18.5 2.5 16.5 0.5 14.5 2.5 12.5 0.5 10.5 2.5 8.5 0.5 6.5 2.5 4.5 0.5"></polygon>
								        <path d="M7.5,6.5 L13.5,6.5"></path>
								        <path d="M7.5,9.5 L11.5,9.5"></path>
								        <path d="M7.5,12.5 L12.5,12.5"></path>
								        <path d="M7.5,15.5 L10.5,15.5"></path>
								        <path d="M16.5,6.5 L17.5,6.5"></path>
								        <path d="M16.5,9.5 L17.5,9.5"></path>
								        <path d="M16.5,12.5 L17.5,12.5"></path>
								        <path d="M16.5,15.5 L17.5,15.5"></path>
								    </g>
								</svg>
								
								<? include("icon.steps-checkmark.php"); ?>
							</span>
							<span class="step-label">Submitted</span>
						</span>
						
						<span class="i-separator num-1"><? include("icon.steps-arrow.php"); ?></span>
						
						<span class="i-step num-2 mod-completed">
							<span class="step-graphic">
								<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
								        <path d="M19,15.5 C19.829,15.5 20.5,16.172 20.5,17 C20.5,17.829 19.829,18.5 19,18.5 L18,18.5 C18.829,18.5 19.5,19.172 19.5,20 C19.5,20.829 18.829,21.5 18,21.5 C18,21.5 14,21.5 11.5,21.5 C7.5,21.5 8.5,19.5 0.5,19.5 C0.5,19 0.5,10.5 0.5,10.5 L3.5,10.5 C7,10.5 11,7.278 11,2.5 C11,0.919 14,0.719 14,3.719 C14,5.719 13,9.5 13,9.5 L21,9.5 C21.829,9.5 22.5,10.172 22.5,11 C22.5,11.829 21.829,12.5 21,12.5 L20,12.5 C20.829,12.5 21.5,13.172 21.5,14 C21.5,14.829 20.829,15.5 20,15.5 L19,15.5"></path>
								    </g>
								</svg>
								<? include("icon.steps-checkmark.php"); ?>
							</span>
							<span class="step-label">Confirmed</span>
						</span>
						
						<span class="i-separator num-2"><? include("icon.steps-arrow.php"); ?></span>
						
						<span class="i-step num-3 mod-completed">
							<span class="step-graphic">
								<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
								        <path d="M17.533,3.062 C16.934,3.062 16.371,3.212 15.879,3.477 C15.405,1.796 13.362,0.562 11.53,0.562 C9.698,0.562 7.629,1.796 7.154,3.477 C6.662,3.212 6.099,3.062 5.502,3.062 C3.568,3.062 2.001,4.629 2.001,6.562 C2.001,8.495 3.5,10.062 5.5,10.062 L5.5,23.5 L17.5,23.5 L17.5,10.062 C19.5,10.062 21.031,8.495 21.031,6.562 C21.031,4.629 19.465,3.062 17.533,3.062 Z"></path>
								        <path d="M5.5,19.5 L17.5,19.5"></path>
								        <path d="M8.5,15.5 L8.5,19.5"></path>
								        <path d="M11.5,14.5 L11.5,19.5"></path>
								        <path d="M14.5,15.5 L14.5,19.5"></path>
								    </g>
								</svg>
								
								<? include("icon.steps-checkmark.php"); ?>
							</span>
							<span class="step-label">Kitchen</span>
						</span>
						
						<span class="i-separator num-3"><? include("icon.steps-arrow.php"); ?></span>
						
						<span href="dynamic.php?page=checkout-payment-details&view=core" class="i-step num-4 mod-current">
							<span class="step-graphic">
								<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
								        <circle cx="5.5" cy="5.5" r="5"></circle>
								        <polyline points="7.5 6.5 5.5 6.5 5.5 3"></polyline>
								        <polyline points="16.5 12.5 21 12.5 23.5 16.5 23.5 19.5 20.913 19.5"></polyline>
								        <polyline points="10.913 19.5 16.511 19.5 16.5 9.5 11.5 9.5"></polyline>
								        <polyline points="5.5 12.5 5.5 19.5 8.087 19.5"></polyline>
								        <circle cx="19.5" cy="20" r="1.5"></circle>
								        <circle cx="9.5" cy="20" r="1.5"></circle>
								        <path d="M0.5,14.5 L3.5,14.505"></path>
								        <path d="M2,16.5 L3.5,16.505"></path>
								        <path d="M3,18.5 L3.5,18.505"></path>
								        <path d="M23.5,16.5 L16.5,16.5"></path>
								        <path d="M16.511,19.5 L18.093,19.5"></path>
								    </g>
								</svg>
								<? include("icon.steps-checkmark.php"); ?>
							</span>
							<span class="step-label">On Its Way</span>
						</span>
						
						<span class="i-separator num-4"><? include("icon.steps-arrow.php"); ?></span>
						
						<span href="dynamic.php?page=checkout-payment-details&view=core" class="i-step num-5 mod-upcoming">
							<span class="step-graphic">
								<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
								        <polygon points="20 2.25 7.5 14.75 4 11.25 0.5 14.75 7.5 21.75 23.5 5.75"></polygon>
								    </g>
								</svg>
								<? include("icon.steps-checkmark.php"); ?>
							</span>
							<span class="step-label">Completed<span>
						</span>
		
					</div>
					<div class="orderStatus-track">
						<a href="#">
							<svg class="fmn-commonIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
							    <g>
							        <circle cx="11.5" cy="11.5" r="8"></circle>
							        <circle cx="11.5" cy="11.5" r="3.5"></circle>
							        <line x1="11.5" y1="0.5" x2="11.5" y2="3.5"></line>
							        <line x1="11.5" y1="19.5" x2="11.5" y2="22.5"></line>
							        <line x1="22.5" y1="11.5" x2="19.5" y2="11.5"></line>
							        <line x1="3.5" y1="11.5" x2="0.5" y2="11.5"></line>
							    </g>
							</svg>
							<span>Track your order with Taxistars</span>
						</a>
					</div>
					
					
				</div>
				
<!--
				<div class="fmn-statusMobile">

					<div class="statusMobile-title"><h3>Your order is:</h3></div>
									
					<div class="fmn-steps mod-statusMobile statusMobile-Steps">
						
						<div class="i-step mod-completed">
							<span class="status-checkmark"><? include("icon.steps-checkmark.php"); ?></span><span class="step-label">Submitted</span>
						</div>
						<div class="i-step mod-completed">
							<span class="status-checkmark"><? include("icon.steps-checkmark.php"); ?></span><span class="step-label">Confirmed</span>
						</div>
						<div class="i-step mod-completed">
							<span class="status-checkmark"><? include("icon.steps-checkmark.php"); ?></span><span class="step-label">Kitchen</span>
						</div>
						<div class="i-step mod-current">
							<span class="status-arrow"><? include("icon.steps-arrow.php"); ?></span><span class="step-label">On Its Way</span>
						</div>
						
					</div>
				
				</div>
-->

			</div>





	<div class="fmn-screen-content-wrap m-fullWidth m-checkout m-reviewOrder">
		
		<div class="fmn-screen-content-wrap-columns">

			<div class="fmn-screen-content-wrap-column m-oneHalf">




				
				<div class="fmn-summary">

					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delivery">
						<h3 class="fmn-section-title"><span>Delivery Address</span></h3>
						<p class="fmn-section-data">
							Sofia, Bulgaria<br>
							Liulin, bl. 700<br>
							entr. A, fl. 3, apt. 30<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Name:</span> John Smith<br>
							<span class="data-label">Phone:</span> 0888 876543<br>
							<span class="data-label">Email:</span> john@johnsmith.com<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Address Notes:</span> Ring the third bell<br>
						</p>
					</div><!-- fmn-summary-section -->

					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delayedDelivery">
						<h3 class="fmn-section-title"><span>Delivery Address</span></h3>
						<p class="fmn-section-data">
							Sofia, Bulgaria<br>
							11, Ivan Vazov Str.<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Name:</span> John Smith<br>
							<span class="data-label">Phone:</span> 0888 876543<br>
							<span class="data-label">Email:</span> john@johnsmith.com<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Address Notes:</span> Ring the third bell<br>
						</p>
					</div><!-- fmn-summary-section -->
					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-takeAway">
						<h3 class="fmn-section-title"><span>Take Away</span></h3>
						<p class="fmn-section-data">
							<span class="data-label">From:</span> Flying Lotus<br>
							<span class="data-note">
								7, Main Street, Taito, Tokyo, Japan<br>
								accepts orders between 09:30 — 20:00<br>
							</span>
						</p>
					</div><!-- fmn-summary-section -->
					
					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delivery debug-view-takeAway">
						<h3 class="fmn-section-title"><span>Order Details</span></h3>
						<p class="fmn-section-data">
							<span class="data-label">Date:</span> Monday, September 19<br>						
							<span class="data-label">Estimated hour:</span> 10:30<br>
							<span class="data-label">Order notes:</span> Please deliver this in a gold plated freezer box.
						</p>
					</div><!-- fmn-summary-section -->
					
					
<!--
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-closed">
						<h3 class="fmn-section-title"><span>Order Details</span><a href="#" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="e-highlight">
								Unfortunately all our locations are closed for the day. Please note the next available day below.<br>
								Date: Monday, September 19<br>
							</span>
							Estimated hour: 10:30<br>
							Order notes: Please deliver this in a gold plated freezer box.
						</p>
					</div><!-- fmn-summary-section -->
-->
					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delayedDelivery">
						<h3 class="fmn-section-title"><span>Order Details</span></h3>
						<p class="fmn-section-data">
							<span class="data-label">Date:</span> Monday, September 19<br>
						
						<p class="fmn-section-data mod-highlight">
							Please note the earliest possible hour for fulfilling your order.<br>
							<span class="data-label">Estimated hour:</span> 10:30<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Order notes:</span> Please deliver this in a gold plated freezer box.
						</p>
					</div><!-- fmn-summary-section -->
					



					<div class="fmn-section fmn-summary-section contactInfo debug-view debug-view-takeAway">
						<h3 class="fmn-section-title"><span>Contact Info</span><a href="dynamic.php?page=checkout-contact-info" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="data-label">Name:</span> John Smith<br>
							<span class="data-label">Phone:</span> 0888 876543<br>
							<span class="data-label">Email:</span> john@johnsmith.com<br>
						</p>
					</div><!-- fmn-summary-section -->





					
					<div class="fmn-section fmn-summary-section paymentDetails">
						<h3 class="fmn-section-title"><span>Payment Details</span></h3>
						<p class="fmn-section-data">
							<span class="data-label">Payment Method:</span> Credit Card<br>
							<span class="data-note">(You’ll be prompted for your card details after order is submitted.)</span><br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Invoice Details:</span> John Smith Co., 1010201301, John Smith, Sofia 1000, 11, Ivan Vazov Str.
						</p>
					</div><!-- fmn-summary-section -->
					
										
					
				</div><!--fmn-summary -->

			</div><!-- fmn-screen-content-wrap-column -->



			<div class="fmn-screen-content-wrap-column m-oneHalf">

				<div class="fmn-summary">
					
					<div class="fmn-section fmn-summary-section">
						
						<h3 class="fmn-section-title mod-huge"><span>Basket</span></h3>
						
						

						<div class="fmn-basket m-readOnly">
			
							<div class="fmn-list">
								
								<div class="i-list-line m-header">
									<span class="i-list-line-cell m-label"></span>
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln1"><input id="cx-ln1" type="checkbox"></label><label for="cx-ln1" class="e-mobileLabel">Select All</label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo">photo</span>
										<span class="i-list-line-cell m-name">name and details</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price">price</span>
										<span class="i-list-line-cell m-qty">qty</span>
										<span class="i-list-line-cell m-lineTotal">total</span>
									</span>
									<span class="i-list-line-cell m-actions"></span>
								</div>
								
								<div class="i-list-line m-row">
<!--
									<span class="i-list-line-cell m-label">
										<span class="i-list-line-cell-label m-justAdded">Just Added!</span>
									</span>
-->
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln2"><input id="cx-ln2" type="checkbox"></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo"><img src="images/sample-dish-green.jpg"></span>
										<span class="i-list-line-cell m-name">
											<span>Green Salad with a Convoluted Long Name</span>
											<span class="e-detail">(no onions, but otherwise super long details)</span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€18.45</span> <span class="e-special">€11.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											2
										</span>
										<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span>€23.90</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
								<div class="i-list-line m-row m-noLongerAvailable debug-view debug-view-all">
									<span class="fmn-labels">
									 	<span class="i-label">
										 	<span class="fmn-noLongerAvailable">
											 	<span class="e-label">This item is no longer available.</span>
										 	</span>
									 	</span>
								 	</span>
<!--
									<span class="i-list-line-cell m-label">
										<span class="i-list-line-cell-label m-noLongerAvailable">This item is no longer available.</span>
									</span>
-->
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln3"><input id="cx-ln3" type="checkbox"></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-name">
											<span>Pizza Margheritta</span>
											<span class="e-detail">(small + garlic sauce)</span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€18.45</span> <span class="e-special">€11.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											2
										</span>
										<span class="i-list-line-cell m-lineTotal">—</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
								<div class="i-list-line m-row m-adjustedQuantity debug-view debug-view-all">
									<span class="fmn-labels">
									 	<span class="i-label">
										 	<span class="fmn-adjustedQuantity">
											 	<span class="e-label">Item Quantity Adjusted</span>
										 	</span>
									 	</span>
								 	</span>
<!--
									<span class="i-list-line-cell m-label">
										<span class="i-list-line-cell-label m-adjustedQuantity">Item Quantity Adjusted</span>
									</span>
-->
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln4"><input id="cx-ln4" type="checkbox" checked=""></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo"><img src="images/sample-category-tile-veal.jpg"></span>
										<span class="i-list-line-cell m-name">
											<span>Veal Pepper Steak</span>
											<span class="e-detail">(medium rare)</span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											1
										</span>
										<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span>€23.90</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
								<div class="i-list-line m-row">
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln5"><input id="cx-ln5" type="checkbox"></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo"><img src="images/sample-category-tile-fish.jpg"></span>
										<span class="i-list-line-cell m-name">
											<span>Cod Fillet</span>
											<span class="e-detail"></span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											2
										</span>
										<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span>€23.90</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
							</div><!-- fmn-list -->
							
	
	
	
							<div class="fmn-basket-footer m-readOnly">
								
								<div class="fmn-basket-footer-secondaryActions">
									<a href="#" class="tap fmn-button m-small m-invert m-destructive">Delete Selected</a>
								</div>
								
								<div class="fmn-basket-footer-data">
									
									<div class="i-basket-footer-wrap m-basket">
										<div class="i-basket-footer-line m-basket">
											<span class="i-basket-footer-line-cell m-label">Basket</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€40.00</span></span></span>
										</div>
										<div class="i-basket-footer-line m-discount debug-view debug-view-all">
											<span class="i-basket-footer-line-cell m-label">
												XX% discount for XXXXXX orders above €XX.XX*<br>
												<span class="e-note">
													<span class="e-special">You are just €XX.XX away from a XX% discount.</span> <a href="#">See details</a>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€4.00</span></span></span>
										</div>
<!--
										<div class="i-basket-footer-line subtotal">
											<span class="i-basket-footer-line-cell m-label">Sub-total</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€36.00</span></span></span>
										</div>
-->
									</div>
									<div class="i-basket-footer-wrap m-delivery">
										<div class="i-basket-footer-line m-delivery">
											<span class="i-basket-footer-line-cell m-label">
												Delivery to XXXXXX for orders above €XX.XX<br>
												<span class="e-note">
													<span class="e-special">Your sub-total is just €XX.XX away from a special €XX.XX delivery fee.</span><br>
													<a href="#">Specify Another Location</a>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">€2.00</span></span></span>
										</div>
									</div>
									<div class="i-basket-footer-wrap m-promoCode debug-view debug-view-all">
										<div class="i-basket-footer-line m-promoCode">
											<span class="i-basket-footer-line-cell m-label">Promo Code</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€4.00</span></span></span>
										</div>
									</div>

									<div class="i-basket-footer-wrap m-total">
										<div class="i-basket-footer-line m-total">
											<span class="i-basket-footer-line-cell m-label">
												Total
												<span class="e-note">
													<span class="e-special">You save €38.00</span>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€38.00</span></span></span>
										</div>
									</div>
									
									<div class="i-basket-footer-wrap m-note">
										<div class="i-basket-footer-line m-note">
											<span class="i-basket-footer-line-span">
												* Items which are already discounted are not further discounted by order total promos
											</span>
										</div>
										
									</div>
								
								</div>
								
							</div><!-- fmn-basket-footer -->
																		
						</div><!-- fmn-basket -->

					</div><!-- fmn-summary-section -->

				</div><!-- fmn-summary -->

			</div><!-- fmn-screen-content-wrap-column --> 
			







		</div><!-- fmn-screen-content-wrap-columns -->
		
	</div><!-- fmn-screen-content-wrap -->


