<svg class="graphic-checkmark" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g>
        <circle cx="11" cy="11" r="9.58333333"></circle>
        <polyline points="15.1666667 8.08333333 8.91666667 13.9166667 6.83333333 11.8333333"></polyline>
    </g>
</svg>