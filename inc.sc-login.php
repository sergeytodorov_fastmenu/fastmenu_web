	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">

		<div class="fmn-screen-intro m-center">
			The cooperative you are trying to access (<b>HP Bulgaria</b>) is private. Please login or register to authenticate.
		</div>

		<div class="fmn-screen-intro m-center">
			You have selected to start a <b>Company Order</b>. Please login or register to authenticate. Note that if your account is not associated with a company you will be prompted for company code.
		</div>

		
		<div class="fmn-screen-content-wrap-columns m-spaceBelow">

				<div class="fmn-screen-content-wrap-column m-threeFifths">
					
					<h2 class="m-secondaryTitle">Enter with Your Account</h2>

					<div class="fmn-form">
								
						<div class="fmn-form-option">
							<label>
								Your Login
								<span class="fmn-form-option-details">If you have an account for our mobile apps, it's valid here as well</span>
							</label>
							<div class="fmn-form-field">
								<input type="text" class="m-maxwidth">
							</div>
						</div>
						<div class="fmn-form-option">
							<label>Password</label>
							<div class="fmn-form-field">
								<input type="text" class="m-maxwidth">
							</div>
							<div class="fmn-form-option-link">
								<a href="#">Forgot your password?</a>
							</div>
						</div>
						<div class="fmn-form-actions">
							<button class="m-large">Login</button>
						</div>		
						
					</div><!-- fmn-form -->

				</div>

				<div class="fmn-screen-content-wrap-column m-twoFifths">

					<h2 class="m-secondaryTitle">Or You Can Also...</h2>
<!--

					<div class="fmn-blocks m-oneCol">
						<div class="i-block-wrap">
							<div class="i-block">
								<p>
									<br>
									<a href="dynamic.php?page=register" class="t-large">Register Now</a>
								</p>
							</div>
						</div>
						<div class="i-block-wrap">
							<div class="i-block">
								<p>
									You can also<br>
									<a href="#" class="c-color-facebook t-large">Enter with Facebook</a>
								</p>
							</div>
						</div>
						<div class="i-block-wrap">
							<div class="i-block">
								<p>
									In a hurry or don't want to register?<br>
									<a href="dynamic.php?page=checkout-personal-info" class="t-large">Order as a Guest</a>
								</p>
							</div>
						</div>
					</div>
-->
					
					<div class="fmn-blocks m-straightLinks m-small m-oneCol">
			
						<div class="i-block-wrap">
							<a href="dynamic.php?page=account-personal-info" class="i-block">
								<span class="e-icon fmn-icon-sign-up"></span><h4>Register Now</h4>
							</a>
						</div>
						
						<div class="i-block-wrap">
							<a href="dynamic.php?page=account-login-methods" class="i-block">
								<span class="e-icon fmn-icon-facebook"></span><h4>Enter with Facebook</h4>
							</a>
						</div>
						
						<div class="i-block-wrap">
							<a href="dynamic.php?page=account-password" class="i-block">
								<span class="e-icon fmn-icon-one-time"></span><h4>Order as Guest</h4>
							</a>
						</div>
						
					</div><!-- fmn-blocks -->
										
				</div>
		</div>
		
	</div><!-- screenWrap -->