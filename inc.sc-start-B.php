<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">

	<div class="fmn-blocks m-straightLinks m-twoCols">
			
		<div class="i-block-wrap">
			<a href="<?=$fmn_next_page_url?>" class="i-block">
				<span class="e-icon fmn-icon-delivery"></span><span class="e-icon fmn-icon-take-away"></span><br>
				<h4>Delivery or Take-away</h4>
			</a>
		</div>
		
		<div class="i-block-wrap">
			<a href="<?=$fmn_next_page_url?>" class="i-block">
				<span class="e-icon fmn-icon-menu"></span><span class="e-icon fmn-icon-pre-order"></span><br>
				<h4>Pre-order with Reservation</h4>
			</a>
		</div>
		
	</div><!-- fmn-blocks -->

</div>


<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">
	
	
	<div class="fmn-form-option">
		<label>Select Order Type</label>
		<div class="fmn-form-buttonArray">
			<a class="fmn-button m-invert m-selected m-giant m-withIcon" href="#"><span><span class="e-icon fmn-icon-delivery"></span>Delivery</span></a>
			<a class="fmn-button m-invert m-giant m-withIcon" href="#"><span class="e-icon fmn-icon-take-away"></span><span>Take-away</span></a>
			<a class="fmn-button m-invert m-giant m-withIcon" href="#"><span class="e-icon fmn-icon-menu"></span><span>Pre-order</span></a>
		</div>
	</div>

</div>

<div class="fmn-screen-content-wrap m-reducedWidth-oneThird">

	<div class="fmn-form debug-view-1">
		
		<div class="fmn-form-option">
			<label>Select Order Type</label>
			<div class="fmn-form-buttonArray">
				<a class="fmn-button m-invert m-selected m-withIcon" href="#"><span><span class="e-icon fmn-icon-delivery"></span>Delivery</span></a>
				<a class="fmn-button m-invert m-withIcon" href="#"><span class="e-icon fmn-icon-take-away"></span><span>Take-away</span></a>
				<a class="fmn-button m-invert m-withIcon" href="#"><span class="e-icon fmn-icon-menu"></span><span>Pre-order</span></a>
			</div>
		</div>
	
	
	
		<div class="fmn-form-option">
			
			<label>Select Address</label>
			<div class="fmn-form-radioArray">
				
				<div class="e-value m-selected">
					<span class="e-field"><input type="radio" id="radio1"></span>
					<label for="radio1">
						<span class="e-title">Home (main)</span>
						<span class="e-details">
							11, Ivan Vazov Str., Center, Sofia 1000<br>
							John Smith, 0888 876 543, john@johnsmith.com
						</span>
					</label>
				</div>
				<div class="e-value">
					<span class="e-field"><input type="radio" id="radio2"></span>
					<label for="radio2">
						<span class="e-title">Home (when Jane’s at home)</span>
						<span class="e-details">
							11, Ivan Vazov Str., Center, Sofia 1000<br>
							Jane Smith, 0888 123 456, jane@johnsmith.com<br>
							Call mobile so we can provide access to lobby
						</span>
					</label>
				</div>
				<div class="e-value">
					<span class="e-field"><input type="radio" id="radio3"></span>
					<label for="radio3">
						<span class="e-title">Office</span>
						<span class="e-details">
							7, Nikolai Kopernik Str., Geo Milev, Sofia 1000<br>
							John Smith, 0888 876 543, john@johnsmith.com
						</span>
					</label>
				</div>
				<div class="e-value">
					<span class="e-field"><input type="radio" id="radio4"></span>
					<label for="radio4">
						<span class="e-title">Another Address</span>
						<select style="">
							<option>Mitte</option>
							<option selected>Prenzlauer Berg</option>
						</select>
					</label>
				</div>
				
			</div>
		</div>
		
		
								
	</div>
	

</div>

<div class="fmn-actions m-right">
		
	<a  href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Next: Delivery Address</a>
	
</div><!-- fmn-actions -->