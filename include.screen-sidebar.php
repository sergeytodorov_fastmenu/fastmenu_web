<div class="fmn-sidebar-header">
	<h2 class="fmn-title sideTitle"><?=$fmn_sidebar_title?></h2>
</div>

<div class="fmn-sidebar-nav">

<? if ($fmn_sidebar_type == "account") { ?>

	<a href="dynamic.php?page=account-personal-info" class="i-item">
	 	<span class="e-caption">Personal Info</span> 
	</a>
	<a href="dynamic.php?page=account-login-methods" class="i-item">
	 	<span class="e-caption">Login Methods</span> 
	</a>
	<a href="dynamic.php?page=account-password" class="i-item">
	 	<span class="e-caption">Password</span> 
	</a>
	<a href="dynamic.php?page=account-my-addresses" class="i-item">
	 	<span class="e-caption">My Addresses</span> 
	</a>
	<a href="dynamic.php?page=account-my-orders" class="i-item">
	 	<span class="e-caption">My Orders</span> 
	</a>
	<a href="dynamic.php?page=account-payment-details" class="i-item">
	 	<span class="e-caption">Payment Details</span> 
	</a>
	<a href="dynamic.php?page=account-company-ordering" class="i-item">
	 	<span class="e-caption">Company Ordering</span> 
	</a>
	
<? } else if ($fmn_sidebar_type == "locations") { ?>
					
	<a href="#" class="i-item">
	 	<span class="e-caption">Sofia</span> 
	</a>
	<a href="#" class="i-item current"> 
	 	<span class="e-caption">Plovdiv</span> 
	</a>
	<a href="#" class="i-item"> 
	 	<span class="e-caption">Burgas</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Varna</span> 
	</a>
	<a href="#" class="i-item m-wicon">
	 	<span class="e-caption">HP Bulgaria</span>
		<span class="e-icon fmn-icon-lock"></span>
	</a>
	<a href="#" class="i-item m-wicon">
	 	<span class="e-caption">Tech Huddle</span>
	 	<span class="e-icon fmn-icon-lock"></span>
	</a>
	<a href="#" class="i-item m-wicon">
	 	<span class="e-caption">SAP Bulgaria</span> 
		<span class="e-icon fmn-icon-lock"></span>
	</a>
		
<? } else if ($fmn_sidebar_type == "categories") { ?>
					
	<a href="#" class="i-item">
	 	<span class="e-caption">Specials</span> 
	</a>
	<a href="#" class="i-item current"> 
	 	<span class="e-caption">Main Menu</span> 
	</a>
	<a href="#" class="i-item"> 
	 	<span class="e-caption">Pizza</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Lunch Menu</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Drinks</span> 
	</a>

<? } else if ($fmn_sidebar_type == "dishes") { ?>

	<a href="#" class="i-item">
	 	<span class="e-caption">Anti-Pasti</span> 
	</a>
	<a href="#" class="i-item current">
	 	<span class="e-caption">Salads</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Pasta</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Pizza</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Pork</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Veal</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Chicken</span> 
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption">Fish</span> 
	</a>
	<a href="#" class="i-item">
		<span class="e-caption">Desserts</span> 
	</a>

<? } else if ($fmn_sidebar_type == "details") { ?>

	<a href="#" class="i-item">
		 	<span class="e-caption current"><!-- Classic Caesar Salad -->Маргарита Фунги</span> 
		 	<span class="e-price"><span class="fmn-common-price"><span class="e-from">от </span><span class="e-normal">7.45 лв</span></span></span>
	</a>
	<a href="#" class="i-item">
	 	<span class="e-caption"><!-- The Best Tzatziki North of the Rodopi Mountains -->Крема</span> 
		 	<span class="e-price"><span class="fmn-common-price"><span class="e-from">от </span><span class="e-normal">7.45 лв</span></span></span>	 	
	</a>
	<a href="#" class="i-item">
		 	<span class="e-caption"><!-- Soba Noodle Salad -->Прошуто и пармезан</span> 
		 	<span class="e-price"><span class="fmn-common-price"><span class="e-from">от </span><span class="e-original">7.45 лв</span> <span class="e-special">€6.45</span></span></span>
	</a>
	<a href="#" class="i-item">
		 	<span class="e-caption"><!-- Russian Salad -->Карбонара</span> 
		 	<span class="e-price"><span class="fmn-common-price"><span class="e-from">от </span><span class="e-normal">7.45 лв</span></span></span>
	</a>

<? } else if ($fmn_sidebar_type == "basket") { ?>

	<div class="fmn-quickBasket" style="display: block; position: static; width: 100%; border: 0 none transparent; box-shadow: none;">
					
					<div class="quickBasket-line">
						<span class="line-qty">2</span>
						<span class="line-times">×</span>
						<span class="line-name">
							Green Salad with a Convoluted Long Name
							<span class="name-detail">(no onions, but otherwise super long details)</span>
						</span>
						<span class="line-price fmn-common-price">
							<span class="e-original">€2318.45</span>
							<span class="e-special">€3311.95</span>
						</span>
					</div>
					<div class="quickBasket-line">
						<span class="line-qty">2</span>
						<span class="line-times">×</span>
						<span class="line-name">
							Veal Pepper Steak
							<span class="name-detail">(well done)</span>
						</span>
						<span class="line-price fmn-common-price">
							<span class="e-normal">€18.45</span>
						</span>
					</div>
					
					<div class="quickBasket-line">
						<span class="line-qty">2</span>
						<span class="line-times">×</span> 
						<span class="line-name">
							Veal Pepper Steak
							<span class="name-detail">(medium rare)</span>
						</span>
						<span class="line-price fmn-common-price">
							<span class="e-normal">€18.45</span>
						</span>
					</div>
					<div class="quickBasket-line">
						<span class="line-qty">20</span>
						<span class="line-times">×</span>
						<span class="line-name">				
							Cod Fillet
						</span>
						<span class="line-price fmn-common-price">
							<span class="e-normal">€18.45</span>
						</span>  
					</div>
					<div class="quickBasket-line">
						<span class="line-qty">2</span>
						<span class="line-times">×</span> 
						<span class="line-name">
							Green Salad with a Convoluted Long Name
							<span class="name-detail">(no onions, but otherwise super long details)</span>
						</span>
						<span class="line-price fmn-common-price">
							<span class="e-normal">€2318.45</span>
						</span>
					</div>
					
					<div class="quickBasket-total">
						<span class="total-label">Basket Total</span>
						<span class="total-amount fmn-common-price">
							<span class="e-normal">€19388.45</span>
						</span> 					
					</div>
					
					<a href="dynamic.php?page=basket" class="fmn-button">Continue</a>
					
					
					
				</div>
		
<? } ?>

</div>





