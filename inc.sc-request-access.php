<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">
	<div class="fmn-content-section m-center">
		<p>Your account does not have access to the private cooperative <b>HP Bulgaria</b>. You can request access and will be notified once the cooperative administrator approves you.</p>
		<p><span class="t-alert">Important!</span> Accounts can only have access to one single private cooperative. Requesting access to <b>HP Bulgaria</b> will revoke your access to <b>SAP Bulgaria</b>.</p>
		<p></p>
	</div>
</div>

<div class="fmn-actions m-center">
	<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Request Access Now</a>
</div><!-- fmn-actions -->
