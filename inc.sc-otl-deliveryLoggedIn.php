<script>

	
	function fmnSetCurrentAddress(theAddress) {
		$( ".fmn-address" ).removeClass( 'mod-selected' );
		$( String( '#fmn-address-' + theAddress) ).addClass('mod-selected');
	}

	function fmnSetAddressType(theType) {
		$( '#fmn-addressForm-fields-street' ).css('display', 'none');
		$( '#fmn-addressForm-fields-complex' ).css('display', 'none');
		$( String( '#fmn-addressForm-fields-' + theType) ).css('display', 'block');
	}

/*
	$(window).on('load', function () { 
		fmnSetAddressType('street');
	});
*/

</script>

<div class="fmn-overlay fmn-otl mod-new">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">
		
		<div class="overlay-header">
			<h3 class="header-title">Order Type & Location</h3>
			<a href="#" class="header-close"><span class="fmn-icon-delete"></span></a>
		</div>
		<div class="overlay-content">

		
						
			<div class="fmn-form">
				
				<div class="otl-fixWidth fmn-form-option">
					<label>Order Type</label>
					<div class="fmn-form-buttonArray m-fullWidth m-2">
						<a class="fmn-button m-invert m-selected" href="#"><span>Delivery</span></a>
						<a class="fmn-button m-invert" href="dynamic.php?page=otlTakeAway"><span>Take-away</span></a>
					</div>
				</div>
				
				<div class="fmn-form-radioArray">
					
					<label for="radio1" class="array-value fmn-address otl-fixWidth mod-selected" id="fmn-address-1" onclick="fmnSetCurrentAddress('1');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
							
								<span class="fm-radio">
									<label for="radio1">
										<input type="radio" name="address" id="radio1" checked=""/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
<!-- 							<input type="radio" id="radio1" name="address" checked=""> -->
							
							</span>
							<label class="fieldAndLabel-label" for="radio1">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									John Smith, 0888 876 543, john@johnsmith.com
								</span>
							</label>
						</div>
					</label>
					
					<label for="radio2" class="array-value fmn-address otl-fixWidth otl-invalidAddress" id="fmn-address-2" onclick="fmnSetCurrentAddress('2');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
								<span class="fm-radio">
									<label for="radio2">
										<input type="radio" name="address" id="radio2"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
<!-- 								<input type="radio" id="radio3" name="address"> -->
							</span>
							<label class="fieldAndLabel-label" for="radio2">
								<span class="label-title">7, Nikolai Kopernik Str.</span>
								<span class="label-details">
									John Smith, 0888 876 543, john@johnsmith.com
								</span>
								<span class="otl-invalidMessage">
									<span>This address is invalid or incomplete.</span>
									<span><a class="fmn-button m-smaller" href="dynamic.php?page=account-edit-address">Click to resolve</a></span>
								</span>
							</label>
						</div>
					</label>
					
					<label for="radio3" class="array-value fmn-address otl-fixWidth" id="fmn-address-3" onclick="fmnSetCurrentAddress('3');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio2" name="address"> -->
								<span class="fm-radio">
									<label for="radio3">
										<input type="radio" name="address" id="radio3"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio3">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									Jane Smith, 0888 123 456, jane@johnsmith.com note: Call mobile so we can provide access to lobby
								</span>
							</label>
						</div>
					</label>


<? /* 						
					<label for="radio3a" class="array-value fmn-address otl-fixWidth" id="fmn-address-3a" onclick="fmnSetCurrentAddress('3a');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio2" name="address"> -->
								<span class="fm-radio">
									<label for="radio1a">
										<input type="radio" name="address" id="radio3a"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio3a">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									Jane Smith, 0888 123 456, jane@johnsmith.com note: Call mobile so we can provide access to lobby
								</span>
							</label>
						</div>
					</label>
					
					<label for="radio3b" class="array-value fmn-address otl-fixWidth" id="fmn-address-3b" onclick="fmnSetCurrentAddress('3b');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio2" name="address"> -->
								<span class="fm-radio">
									<label for="radio3b">
										<input type="radio" name="address" id="radio3b"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio3b">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									Jane Smith, 0888 123 456, jane@johnsmith.com note: Call mobile so we can provide access to lobby
								</span>
							</label>
						</div>
					</label>
					
					<label for="radio3c" class="array-value fmn-address otl-fixWidth" id="fmn-address-3c" onclick="fmnSetCurrentAddress('3c');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio2" name="address"> -->
								<span class="fm-radio">
									<label for="radio3c">
										<input type="radio" name="address" id="radio3c"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio3c">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									Jane Smith, 0888 123 456, jane@johnsmith.com note: Call mobile so we can provide access to lobby
								</span>
							</label>
						</div>
					</label>
					
					<label for="radio3d" class="array-value fmn-address otl-fixWidth" id="fmn-address-3d" onclick="fmnSetCurrentAddress('3d');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio2" name="address"> -->
								<span class="fm-radio">
									<label for="radio3d">
										<input type="radio" name="address" id="radio3d"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio3d">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									Jane Smith, 0888 123 456, jane@johnsmith.com note: Call mobile so we can provide access to lobby
								</span>
							</label>
						</div>
					</label>
					
					<label for="radio3e" class="array-value fmn-address otl-fixWidth" id="fmn-address-3e" onclick="fmnSetCurrentAddress('3e');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio2" name="address"> -->
								<span class="fm-radio">
									<label for="radio3e">
										<input type="radio" name="address" id="radio3e"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio3e">
								<span class="label-title">11, Ivan Vazov Str.</span>
								<span class="label-details">
									Jane Smith, 0888 123 456, jane@johnsmith.com note: Call mobile so we can provide access to lobby
								</span>
							</label>
						</div>
					</label>


*/ ?>			
					
					
					<label for="radio4" class="array-value fmn-address otl-customAddress " id="fmn-address-4" onclick="fmnSetCurrentAddress('4');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio4" name="address" > -->
								<span class="fm-radio">
									<label for="radio4">
										<input type="radio" name="address" id="radio4"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
<!-- 										<span class="redi-radio-label">either this</span> -->
									</label>
								</span>
							</span>
							<label class="fieldAndLabel-label" for="radio4">
								<span class="label-title">Specify Another Address</span>
							</label>
						</div>
						
						<div class="value-function">
						
							<div class="fmn-locationAddress mod-otl">

								<div id="fmn-addressForm" class="locationAddress-addressForm">
									
									<div class="findAddress-instruction">Please specify you address using Street and Number or Residiential Complex, Block and Entrance</div>
									
									<div class="fmn-form">
									
										<? /* uncomment 
										<div class="fmn-form-error">
											This form contains errors. Please revise.
										</div>

										
										<div class="fmn-form-required">
											<span class="required-text">Required fields</span><span class="required-asterisk">*</span>
										</div>
										*/ ?>

										<? /* uncomment 
										<div class="fmn-form-option">
											<label>Country</label>
											<div class="fmn-form-field mod-select"><select><option>Bulgaria</option></select></div>
										</div>
										*/ ?>

<!--
										<div class="fmn-form-option">
											<label>City</label>
											<div class="fmn-form-field mod-select"><select><option>Plovdiv</option></select></div>
										</div>
-->


<!--
										<div class="fmn-form-option">
											<label>Specify address as:</label>
											<div class="i-basic-value">
												<label style="margin-bottom: 0;" onclick="fmnSetAddressType('street');"><input name="addressType" type="radio" checked=""> Street and Number</label>
											</div>
											<div class="i-basic-value">
												<label onclick="fmnSetAddressType('complex');"><input name="addressType" type="radio"> Residential Complex and Block</label>
											</div>
										</div>
-->

										<? /* uncomment 		
										<div class="addressForm-columns m-twoCols">
											<div class="columns-col m-twoThirds">
												<div class="fmn-form-option">
													<label>City</label>
													<div class="fmn-form-field mod-select"><select><option>Plovdiv</option></select></div>
												</div>
											</div>
											<div class="columns-col m-oneThird">
												<div class="fmn-form-option">
													<label>Postal Code</label>
													<div class="fmn-form-field"><input type="text"></div>
												</div>
											</div>
										</div>
										*/ ?>
																				
										<div id="fmn-addressForm-fields-street">
									
											<div class="addressForm-columns m-twoCols">
												<div class="columns-col m-twoThirds">
													<div class="fmn-form-option disable-mod-required">
														<label>Street</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option disable-mod-required">
														<label>Number</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
											</div>
										
										</div>
										
										<div id="fmn-addressForm-fields-complex">
									
											<div class="addressForm-columns m-twoCols">
												<div class="columns-col m-twoThirds">
													<div class="fmn-form-option disable-mod-required">
														<label>Residential Complex</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option disable-mod-required">
														<label>Block</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
											</div>
	
	
											<div class="addressForm-columns m-threeCols">
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option">
														<label>Еntrance</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option">
														<label>Floor</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option">
														<label>Apartment</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
											</div>
										
										</div>
										
										
										<? /* uncomment
										<div class="fmn-form-option">
											<label>Notes for This Address</label>
											<div class="fmn-form-field"><input type="text" placeholder="e.g. if we need to call for access"></div>
										</div>
										*/ ?>
										
										
										<!--
											
										<div class="fmn-form-option">
											<div class="i-basic-value">
												<label><input type="checkbox" checked=""> Save this address for future orders</label>
											</div>
										</div>
										
										<div class="fmn-form-option">
											<label>Address Label</label>
											<div class="fmn-form-field"><input type="text" placeholder="e.g. My Office"></div>
										</div>
					
										<div class="fmn-form-option">
											<div class="i-basic-value">
												<label><input type="checkbox"> Make it the main address for my profile</label>
											</div>
										</div>
										
										-->
										
									</div>
								
								</div>
								
								
								
								
								

								<div class="locationAddress-findAddress">

                                    <div class="findAddress-instruction">Please review your address location and apply corrections if needed</div>
                                    
									<div class="findAddress-map">
										<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d11831.96115109383!2d24.73210195!3d42.15049465!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbg!4v1518102743587" width="100%" height="240px" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								
								</div>

								
								
							</div>
						
						
						</div>
						
					</label>
					
				</div>

				
										
			</div>
		
					
			
		</div><!-- overlay-content -->
		<div class="overlay-footer">
			<div class="fmn-actions m-right mod-overlay">
				<a href="#">Cancel</a>
				<input type="submit" class="m-large" value="Confirm">
			</div><!-- fmn-actions -->
		</div><!-- overlay-footer -->

	</div><!-- fmn-overlay-box -->

</div><!-- fmn-overlay -->