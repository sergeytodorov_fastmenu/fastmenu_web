
<div class="fmn-screen-content-wrap m-reducedWidth-oneThird">

		<div class="fmn-content-section">
			<p>Your account is not associated with any company. To create a company order, please enter your company code below.</p>
			
		</div>

		<div class="fmn-form">
			
			<div class="fmn-form-option">
				<label>Company Code</label>
				<div class="fmn-form-field">
					<input type="text" value="" placeholder="Enter Company Code">
				</div>
				<div class="fmn-form-option-link">
					<a href="dynamic.php?page=code-contact-us">Don't have a code? Contact Us</a>
				</div>
			</div>
						
		</div>
		
</div>







<div class="fmn-actions m-center">
	<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Submit Code</a>
</div><!-- fmn-actions -->
