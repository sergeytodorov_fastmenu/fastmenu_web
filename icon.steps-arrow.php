<svg class="separator-icon" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g>
        <path d="M19,10 L0.928571429,10 L19,10 Z"></path>
        <polyline points="9.17857143 1 19 9.96296296 9.17857143 18.9259259"></polyline>
    </g>
</svg>