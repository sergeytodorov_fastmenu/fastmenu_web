<div class="fmn-screen-content">

	<div class="fmn-screen-messages">	
		<div class="fmn-message">
			<p>Testing message above tiles</p>
		</div>
	</div>

	<div class="fmn-screen-content-header m-center">
		
		<div class="fmn-form">						
			<div class="fmn-form-option">
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert" m-selected href="#"><span>All Cooperatives</span></a>
						<a class="fmn-button m-invert" href="#"><span>Public</span></a>
						<a class="fmn-button m-invert" href="#"><span>Private</span></a>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
	
	<div class="fmn-tiles">
	
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Sofia</span><br>
		 	</span>
		 	
		</div>
		</div>
	 
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Plovdiv</span><br>
		 	</span>
		 	
		</div>
		</div>
				
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
		 	<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Burgas</span><br>
		 	</span>
		 	
		</div>
		</div>
									
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Varna</span><br>
		 	</span>
		</div>
		</div>
		
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">HP Bulgaria</span><br>
		 	</span>
		 	<span class="e-info-bottomLeft">
			 	<span class="fmn-private">
				 	<span class="e-icon fmn-icon-lock"></span>
				 	<span class="e-label">Private</span>
			 	</span>
		 	</span>
		</div>
		</div>
		
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Tech Huddle</span><br>
		 	</span>
		 	<span class="e-info-bottomLeft">
			 	<span class="fmn-private">
				 	<span class="e-icon fmn-icon-lock"></span>
				 	<span class="e-label">Private</span>
			 	</span>
		 	</span>
		</div>
		</div>
		
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">SAP Bulgaria</span><br>
		 	</span>
		 	<span class="e-info-bottomLeft">
			 	<span class="fmn-private">
				 	<span class="e-icon fmn-icon-lock"></span>
				 	<span class="e-label">Private</span>
			 	</span>
		 	</span>
		</div>
		</div>

			
	</div><!-- tiles -->

</div><!-- fmn-screen-content -->