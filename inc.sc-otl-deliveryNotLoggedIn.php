<script>
	
/*
	function fmnSetCurrentAddress(theAddress) {
		$( ".fmn-address" ).removeClass( 'mod-selected' );
		$( String( '#fmn-address-' + theAddress) ).addClass('mod-selected');
	}

	function fmnSetAddressType(theType) {
		$( '#fmn-addressForm-fields-street' ).css('display', 'none');
		$( '#fmn-addressForm-fields-complex' ).css('display', 'none');
		$( String( '#fmn-addressForm-fields-' + theType) ).css('display', 'block');
	}

	$(window).on('load', function () { 
		fmnSetAddressType('street');
	});
*/
		
</script>

<div class="fmn-overlay fmn-otl mod-new">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">
		
		<div class="overlay-header">
			<h3 class="header-title">Order Type & Location</h3>
			<a href="#" class="header-close"><span class="fmn-icon-delete"></span></a>
		</div>
		<div class="overlay-content">
						
							
				
			<div class="fmn-form">
				
				<div class="otl-fixWidth fmn-form-option">
					<label>Order Type</label>
					<div class="fmn-form-buttonArray m-fullWidth m-2">
						<a class="fmn-button m-invert m-selected" href="#"><span>Delivery</span></a>
						<a class="fmn-button m-invert" href="dynamic.php?page=otlTakeAway"><span>Take-away</span></a>
					</div>
				</div>
				
				<div class="otl-loginInvite">
					Don't see your addresses? If you already have a profile with saved addresses,  <a href="dynamic.php?page=otlDeliveryLoggedIn">Login Now</a> to select from them. If you don't, please specify your location below.
				</div>



					
				<div class="fmn-form-radioArray">
					
					<label for="radio4" class="array-value fmn-address otl-customAddress mod-selected" id="fmn-address-4" onclick="fmnSetCurrentAddress('4');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field" style="display: none;"><input type="radio" id="radio4" name="address" checked=""></span>
							<label class="fieldAndLabel-label" for="radio4">
								<span class="label-title">Specify Location</span>
							</label>
						</div>
						
						<div class="value-function">
						
							<div class="fmn-locationAddress mod-otl">

								<div id="fmn-addressForm" class="locationAddress-addressForm">
									
									<div class="findAddress-instruction">Please specify you address using Street and Number or Residiential Complex, Block and Entrance</div>
									
									
									<div class="fmn-form">
<!--
										<div class="fmn-form-option">
											<label>City</label>
											<div class="fmn-form-field mod-select"><select><option>Plovdiv</option></select></div>
										</div>
-->

<!--
										<div class="fmn-form-option">
											<label>Specify address as:</label>
											<div class="i-basic-value">
												<label style="margin-bottom: 0;" onclick="fmnSetAddressType('street');"><input name="addressType" type="radio" checked=""> Street and Number</label>
											</div>
											<div class="i-basic-value">
												<label onclick="fmnSetAddressType('complex');"><input name="addressType" type="radio"> Residential Complex and Block</label>
											</div>
										</div>
-->
										
										<div id="fmn-addressForm-fields-street">
									
											<div class="addressForm-columns m-twoCols">
												<div class="columns-col m-twoThirds">
													<div class="fmn-form-option disable-mod-required">
														<label>Street</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option disable-mod-required">
														<label>Number</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
											</div>
										
										</div>
										
										<div id="fmn-addressForm-fields-complex">
									
											<div class="addressForm-columns m-twoCols">
												<div class="columns-col m-twoThirds">
													<div class="fmn-form-option disable-mod-required">
														<label>Residential Complex</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option disable-mod-required">
														<label>Block</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
											</div>
	
	

			
											<div class="addressForm-columns m-threeCols">
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option">
														<label>Еntrance</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option">
														<label>Floor</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
												<div class="columns-col m-oneThird">
													<div class="fmn-form-option">
														<label>Apartment</label>
														<div class="fmn-form-field"><input type="text"></div>
													</div>
												</div>
											</div>

										
										</div>
										
										
										<? /* uncomment
										<div class="fmn-form-option">
											<label>Notes for This Address</label>
											<div class="fmn-form-field"><input type="text" placeholder="e.g. if we need to call for access"></div>
										</div>
										*/ ?>
										
										
										<!--
											
										<div class="fmn-form-option">
											<div class="i-basic-value">
												<label><input type="checkbox" checked=""> Save this address for future orders</label>
											</div>
										</div>
										
										<div class="fmn-form-option">
											<label>Address Label</label>
											<div class="fmn-form-field"><input type="text" placeholder="e.g. My Office"></div>
										</div>
					
										<div class="fmn-form-option">
											<div class="i-basic-value">
												<label><input type="checkbox"> Make it the main address for my profile</label>
											</div>
										</div>
										
										-->
										
									</div>
								
								</div>
								
								
								
								
								

								<div class="locationAddress-findAddress">

                                    <div class="findAddress-instruction">Please review your address location and apply corrections if needed</div>
                                    
									<div class="findAddress-map">
										<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d11831.96115109383!2d24.73210195!3d42.15049465!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbg!4v1518102743587" width="100%" height="240px" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								
								</div>
								
								
							</div>
						
						
						</div>
						
					</label>
					
				</div>








			</div>
			
			
			
		</div><!-- overlay-content -->
		<div class="overlay-footer">
			<div class="fmn-actions m-right mod-overlay">
				<a href="#">Cancel</a>
				<input type="submit" class="m-large" value="Confirm">
			</div><!-- fmn-actions -->
		</div><!-- overlay-footer -->

	</div><!-- fmn-overlay-box -->

</div><!-- fmn-overlay -->