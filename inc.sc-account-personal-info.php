	<div class="fmn-screen-content-wrap m-reducedWidth-oneThirdWithSide">
		
		<div class="fmn-form">
			
			<div class="fmn-form-error">
				This form contains errors. Please revise.
			</div>
			
			<div class="fmn-form-option">
				<label>First Name</label>
				<div class="fmn-form-field">
					<input type="text">
				</div>
			</div>
			<div class="fmn-form-option">
				<label>Last Name
					<span class="fmn-form-option-error">This field is required.</span>
				</label>
				<div class="fmn-form-field">
					<input type="text" class="input-validation-error" value="Example of a field with error">
				</div>
			</div>
			<div class="fmn-form-option m-error">
				<label>
					Email Address
					<span class="fmn-form-option-error">Please enter a valid email address.</span>
				</label>
				<div class="fmn-form-field">
					<input type="text" class="error" value="ben@microsoft">
				</div>
			</div>
			
			<div class="fmn-form-option">
				<div class="i-basic-value">
					<label><input type="checkbox"> Subscribed to promos and special offers via email</label>
				</div>
			</div>						

		</div><!-- fmn-form -->
		
		
	</div><!-- screenWrap -->
	
	<div class="fmn-actions m-right">
		
		<button class="m-large" value="Checkout">Save</button>
		
	</div><!-- fmn-actions -->
