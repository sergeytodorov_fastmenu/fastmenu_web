<div class="fmn-screen-content">

	<div class="fmn-screen-messages">	
		<div class="fmn-message">
			<p>Testing message above tiles</p>
		</div>
	</div>
	
	<div class="fmn-filters">

		<div class="e-filtersapplied">
			
			<span class="e-caption">Showing: <b>Meats, Dairy & Eggs, Fish & Marine, Fruits & Vegetables, Bread, Grains</b></span><span class="e-links"><a class="e-link" href="#">Edit</a><a class="e-link" href="#">Clear Filter</a></span>
			
		</div>

		<div class="e-filtersapplied">
			
			<span class="e-caption">Showing: <b>All Farms</b></span><span class="e-links"><a class="e-link" href="#">Filter</a></span>
			
		</div>

		<div class="fmn-form">
			<div class="fmn-form-option m-nopadding">
				
				<div class="fmn-form-simpleArray mod-noHover">
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier" checked=""> <span>All Farms</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Meats</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Fruits & Vegetables</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Bread</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Grains</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Dairy & Eggs</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Fish & Marine</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Wine</span></label></div>
					<div class="i-arrayValue"><label><input type="checkbox" name="modifier"> <span>Drinks</span></label></div>
				</div>
				
<!--
				<div class="fmn-form-checkboxArray">
					
					<div class="e-values-wrap">
					
						<div class="e-value m-selected">
							<span class="e-field"><input type="checkbox" id="check1" name="" checked=""></span>
							<label for="check1">
								<span class="e-text">All Farms</span>
							</label>
						</div>
					
					</div>
					
					<div class="e-values-wrap">
					
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check2" name=""></span>
							<label for="check2">
								<span class="e-text">Meats</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check3" name=""></span>
							<label for="check3">
								<span class="e-text">Fruits & Vegetables</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check4" name=""></span>
							<label for="check4">
								<span class="e-text">Bread</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check5" name=""></span>
							<label for="check5">
								<span class="e-text">Grains</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check6" name=""></span>
							<label for="check6">
								<span class="e-text">Dairy & Eggs</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check7" name=""></span>
							<label for="check7">
								<span class="e-text">Fish & Marine</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check8" name=""></span>
							<label for="check8">
								<span class="e-text">Wine</span>
							</label>
						</div>
						
						<div class="e-value">
							<span class="e-field"><input type="checkbox" id="check9" name=""></span>
							<label for="check9">
								<span class="e-text">Drinks</span>
							</label>
						</div>
					
					</div>
				</div>
-->
			</div>
		</div>
		
	</div>
	
	<div class="fmn-tiles m-wfilters">
	
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-category-tile-desserts.jpg);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Specials</span><br>
		 	</span>
		 	
		</div>
		</div>
	 
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-category-tile-fish.jpg);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Main Menu</span><br>
		 	</span>
		 	
		</div>
		</div>
				
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-category-tile-pizza.jpg);"></span>
		 	<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Pizza</span><br>
		 	</span>
		 	
		</div>
		</div>
									
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-category-tile-veal.jpg);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Lunch Menu</span><br>
		 	</span>
		</div>
		</div>
		
<!--
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-category-tile-drinks.jpg);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
			<span class="e-info-topLeft">
			 	<span class="e-title">Drinks</span><br>
		 	</span>
		</div>
		</div>
-->
			
	</div><!-- tiles -->

</div><!-- fmn-screen-content -->