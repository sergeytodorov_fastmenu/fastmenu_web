
	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirdsWithSide">
		<div class="fmn-form">

			<div class="fmn-screen-content-wrap-columns m-spaceBelow">
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					
					<h2 class="m-secondaryTitle">Active Methods</h2>
					
					<div class="fmn-blocks m-oneCol m-justAction">
				
						<div class="i-block-wrap">
							<div class="i-block">
								<h4>Tanaka Sushi Login</h4>
							</div>
						</div>
						
						<div class="i-block-wrap">
							<div class="i-block">
								<h4>Facebook</h4>
								<a href="#" class="destructive">Deactivate</a>
							</div>
						</div>
						
					</div><!-- fmn-blocks -->
					
				</div>
				
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					
					<h2 class="m-secondaryTitle">Available for Activation</h2>
					
					<div class="fmn-blocks m-oneCol m-justAction">
															
						<div class="i-block-wrap">
							<div class="i-block">
								<h4>Google</h4>
								<button type="submit" href="#">Activate</button>
							</div>
						</div>
						
						<div class="i-block-wrap">
							<div class="i-block">
								<h4>Twitter</h4>
								<button type="submit" href="#">Activate</button>
							</div>
						</div>
						
					</div><!-- fmn-blocks -->
					
				</div>
				
			</div>

			
			

		</div><!-- fmn-form -->
		
	</div><!-- fmn-screen-content-wrap -->