<?
	$views = "available";
	
	$allowedViews = array(
    	"tilesCaptionInside" => array( 
	    	"visibleView" => "tilesCaptionInside"
    	),
    	"tilesCaptionOutside" => array( 
	    	"visibleView" => "tilesCaptionOutside"
    	),
    	"tilesCaptionOutsideIngredients" => array( 
	    	"visibleView" => "tilesCaptionOutsideIngredients"
    	),
    	"listPhotos" => array( 
	    	"visibleView" => "listPhotos"
    	),
    	"listTwoCols" => array( 
	    	"visibleView" => "listTwoCols"
    	)

	);

	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];

	$dishes = array(
		
		
/*
		array(		
			"name" => "Ориз Балдо 1 кг",
			"image" => "images/sample-dish-caesar.jpg",
			"special" => "no",
			"price" => "€8.45",
			"instock" => "yes",
			"in-basket" => "no",
			"singleSubitem" => "1 стек с 10 бр. по 1 кг"
		),
*/		
		array(						
			"name" => "Caprese",
			"image" => "images/sample-dish-caprese.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Tomatoes, Mozzarella, Basil, Oregano, Olive Oil, Salt",
			"with-sub-items" => "no",
			"sub-1-in-basket" => "yes",
			"sub-1-in-basket-count" => "3",
			"sub-1-timer" => "yes",
			"sub-1-special-ends" => "03:27:11",
			"hasDetails" => "no"
		),
		array(			
			"name" => "Greek Salad",
			"image" => "images/sample-dish-greek.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Tomatoes, Cucumbers, Onions, Feta Cheese, Olive Oil, Oregano, Chilli",
			"with-sub-items" => "no",
			"sub-1-out-of-stock" => "yes"
		),
		array(			
			"name" => "Tabouli",
			"image" => "images/sample-dish-tabouli.jpg",
			"special" => "yes",
			"instock" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Parsley, Onions, Tomatoes, Lemon, Olive Oil, Bulgur, Pepper"
		),

		array(			
			"name" => "Fattoush",
			"image" => "images/sample-dish-fattoush.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Pita Bread, Mixed Greens, Tomatoes, Radish, Onions, Cucumbers, Olive Oil"
		),

		array(			
			"name" => "Tzatziki",
			"image" => "images/sample-dish-tzatziki.jpg",
			"special" => "yes",
			"timer" => "no",			
			"special-ends" => "03:27:11",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Greek Yogurt, Cucumbers, Olive Oil"
		),

		array(			
			"name" => "Italian Salad",
			"image" => "images/sample-dish-italian.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Tomatoes, Cucumbers, Onions, Feta Cheese, Olive Oil, Oregano, Chilli"
		),

		array(			
			"name" => "Russian Salad",
			"image" => "images/sample-dish-russian.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Potatoes, Mayonnaise, Ham, Green Peas",
			"with-sub-items" => "no",
			"sub-1-timer" => "yes",
			"sub-1-special-ends" => "03:27:11",
			"pay-online" => "no"
		),
		array(		
			"name" => "Caesar Salad",
			"image" => "images/sample-dish-caesar.jpg",
			"special" => "no",
			"price" => "€8.45",
// 			"description" => "Awesome Caesar salad with a very long description to see how it works when we have a photo in the list with photo view. Need an even longer caption actually so I'll just continue writing meaningless words here until we get there. Oh.. even more, ok, more and more come on",
			"ingredients" => "Romaine Lettuce, Croutons, Cheese*, Olive Oil, Lemon Juice, Egg*, Worchestershire Sauce*",
			"instock" => "yes",
			"timer" => "no",			
			"special-ends" => "03:27:11",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"in-basket" => "yes",
			"in-basket-count" => "1",
			"with-sub-items" => "no",
			"pay-online" => "no"
		),

		array(			
			"name" => "Green Salad",
			"image" => "images/sample-dish-green.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Green Salad, Onions, Cucumbers, Egg, Olive Oil"
		),
		array(			
			"name" => "Soba Noodle Salad",
			"image" => "images/sample-dish-soba.jpg",
			"special" => "yes",
			"timer" => "no",	
			"special-ends" => "3 days",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Soba Noodles, Cilantro, Bell Peppers, Bean Sprouts, Grapeseed Oil, Soy Sauce"
		),
		array(			
			"name" => "Egg Salad",
			"image" => "images/sample-dish-egg.jpg",
			"special" => "yes",
			"timer" => "no",	
			"special-ends" => "01:17:32",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Eggs, Mayonnaise, Dijon Mustard, Paprika, Onions, Salt, Pepper",
			"pay-online" => "no"
		),
		
		
	/*	
		array(		
			"name" => "Classic Caesar Salad",
			"image" => "images/sample-dish-caesar.jpg",
			"special" => "no",
			"price" => "€8.45",
			"description" => "Awesome Caesar salad",
			"ingredients" => "Romaine Lettuce, Croutons, Cheese*, Olive Oil, Lemon Juice, Egg*, Worchestershire Sauce*"
		),
		array(			
			"name" => "The Best Tzatziki North of the Rodopi Mountains",
			"image" => "images/sample-dish-tzatziki.jpg",
			"special" => "yes",
			"timer" => "no",	
			"special-ends" => "03:27:11",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Greek Yogurt, Cucumbers, Olive Oil"
		),
		array(						
			"name" => "Caprese",
			"image" => "images/sample-dish-caprese.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Tomatoes, Mozzarella, Basil, Oregano, Olive Oil, Salt"
		),
		array(			
			"name" => "Greek Salad",
			"image" => "images/sample-dish-greek.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Tomatoes, Cucumbers, Onions, Feta Cheese, Olive Oil, Oregano, Chilli"
		),
		array(			
			"name" => "Russian Salad",
			"image" => "images/sample-dish-russian.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Potatoes, Mayonnaise, Ham, Green Peas"
		),
		array(			
			"name" => "Green Salad",
			"image" => "images/sample-dish-green.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Green Salad, Onions, Cucumbers, Egg, Olive Oil"
		),
		array(			
			"name" => "Tabouli",
// 			"image" => "images/sample-dish-tabouli.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Parsley, Onions, Tomatoes, Lemon, Olive Oil, Bulgur, Pepper"
		),
		array(			
			"name" => "Soba Noodle Salad",
			"image" => "images/sample-dish-soba.jpg",
			"special" => "yes",
			"timer" => "no",	
			"special-ends" => "3 days",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Soba Noodles, Cilantro, Bell Peppers, Bean Sprouts, Grapeseed Oil, Soy Sauce"
		),
		array(			
			"name" => "Fattoush",
			"image" => "images/sample-dish-fattoush.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Pita Bread, Mixed Greens, Tomatoes, Radish, Onions, Cucumbers, Olive Oil"
		)
	*/
	
	);
	
?>


<div class="fmn-screen-content">
	
<!--
	<div class="fmn-search-results">
		<span class="c-nobr">Showing results for:</span> &nbsp; &nbsp; <b>italian</b> &nbsp; &nbsp; with: <b>Beef, Zucchini, Pepper</b> &nbsp; &nbsp; without: <b>Garlic</b> &nbsp; &nbsp; <a href="search.php" class="c-nobr">Edit Search</a>
	</div>
-->

	
	<!-- scenario: add m-noAddToBasket to .fmn-products container to hide the Add to Basket shortcut -->
	
	
<!--
	<div class="fmn-screen-messages">
		<div class="fmn-message">
			<p>Testing message above tiles and lists</p>
		</div>
	</div>
-->
		
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	<div class="fmn-products debug-view-tiles debug-view-0 debug-view-tilesCaptionInside -m-noAddToBasket" style="display: block;"> 
		
		<div class="fmn-tiles m-transparentBlackPhotoLabels">
			
			<div class="fmn-products-title m-noPhoto DISABLEm-withPhoto" DISABLEstyle="background-image: url(images/sample-category.jpg);">
				<span class="e-caption">Mediterranean Salads</span>
			</div>

			<? for ($i = 0; $i < count($dishes); $i++) { ?>
			
					<? if ($i == 6) { ?>
						<div class="fmn-products-title m-noPhoto">
							<span class="e-caption">Other Salads</span>
						</div>
					<? } ?>

<!--
					<? if ($i == 13) { ?>
						<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
							<span class="e-caption">Exotic Salads</span>
						</div>
					<? } ?>
-->
			
					<div class="i-tile-wrap <? if ($dishes[$i]["instock"] == "no") { ?>m-outofstock<? } ?> <? if ($dishes[$i]["hasDetails"] == "no") { ?>m-noDetails<? } ?>">
					<div class="i-tile">
						
						<? if ($dishes[$i]["image"] != null ) { ?>
								<span class="e-photo" style="background-image: url(<?=$dishes[$i]["image"]?>);"></span>
						<? } else { ?>
								<span class="e-photo" style="background-image: url(images/no-photo.jpg);"></span>
						<? } /* end if */ ?>
						

					 	<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
						<span class="e-info-topLeft">
						 	<span class="e-title"><?=$dishes[$i]["name"]?></span><br>
						 	
							<? if ($dishes[$i]["special"] == "no") { ?>

									<span class="e-subtitle">
										<span class="fmn-common-price">
											<span class="e-normal"><?=$dishes[$i]["price"]?></span>
										</span>
									</span>

							<? } else if ($dishes[$i]["special"] == "yes") { ?>

									<span class="e-subtitle">
										<span class="fmn-common-price">
											<span class="e-original"><?=$dishes[$i]["price-original"]?></span> 
											<span class="e-special"><?=$dishes[$i]["price-special"]?></span>
										</span>
									</span>

							<? } /* end if */ ?>
						 	
						</span>
						<span class="e-info-bottomLeft fmn-labels">
							
							<? if ($dishes[$i]["in-basket"] == "yes") { ?>
	
								<span class="i-label">
								 	<span class="fmn-count-in-basket a-flash">
									 	<span class="e-icon fmn-icon-in-basket"></span>
									 	<span class="e-label"><?=$dishes[$i]["in-basket-count"]?></span>
								 	</span>
								</span>
	
							<? } /* end if */ ?>

							<? if ($dishes[$i]["timer"] == "yes") { ?>
								
								<span class="i-label">
									<span class="fmn-timer">
										<span class="e-icon fmn-icon-timer"></span>
										<span class="e-label"><?=$dishes[$i]["special-ends"]?></span>
									</span>
								</span>
								 	
							<? } /* end if */ ?>

						 	<? if ($dishes[$i]["instock"] == "no") { ?>
						 		
						 		<span class="i-label">
								 	<span class="fmn-outofstock">
									 	<span class="e-icon fmn-icon-forbidden"></span>
									 	<span class="e-label">Out of Stock</span>
								 	</span>
							 	</span>
						 	
						 	<? } /* end if */ ?>
						 	
							<? if ($dishes[$i]["pay-online"] == "yes") { ?>
							
							<span class="i-label">
							 	<span class="fmn-payOnline">
									<span class="e-icon fmn-icon-online-payment"></span>
								 	<span class="e-label">Pay Online</span>
							 	</span>
						 	</span>

							<? } /* end if */ ?>



					 	</span>
					 	<span class="e-options">
					 		<? /*
						 		keeping this here, commented out in case we need it, but feel free to delete from production code
						 		<a class="e-details" href="<?=$fmn_next_page_url?>" class=""><span class="fmn-icon-search"></span></a>
						 		*/ ?>
					 		<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
						 		<span class="e-icon fmn-icon-add-to-basket"></span>
						 		<span class="e-caption">Buy</span>
						 	</a>
					 	</span>
					 	
					</div><!-- tile -->
					</div><!-- tile-wrap -->

			<? } /* end for */ ?>  		
			
		</div><!-- tiles -->
		
	</div><!-- fmn-products -->
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 
	
	
	<div class="fmn-products debug-view-tiles debug-view-1 debug-view-tilesCaptionOutside -m-noAddToBasket" style="display: block;"> 
		
		<div class="fmn-tiles m-transparentBlackPhotoLabels">
			
			<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
				<span class="e-caption">Basic Salads</span>
			</div>

			<? for ($i = 0; $i < count($dishes); $i++) { ?>
			
					<? if ($i == 7) { ?>
						<div class="fmn-products-title m-noPhoto">
							<span class="e-caption">Mediterranean Salads</span>
						</div>
					<? } ?>

					<? if ($i == 13) { ?>
						<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
							<span class="e-caption">Exotic Salads</span>
						</div>
					<? } ?>
			
					<div class="i-tile-wrap <? if ($dishes[$i]["instock"] == "no") { ?>m-outofstock<? } ?> <? if ($dishes[$i]["hasDetails"] == "no") { ?>m-noDetails<? } ?>">
					<div class="i-tile">
						
						<? if ($dishes[$i]["image"] != null ) { ?>
								<span class="e-photo" style="background-image: url(<?=$dishes[$i]["image"]?>);"></span>
						<? } else { ?>
								<span class="e-photo" style="background-image: url(images/no-photo.jpg);"></span>
						<? } /* end if */ ?>
						
					 	<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>

						<span class="e-info-bottomLeft fmn-labels">

							<? if ($dishes[$i]["in-basket"] == "yes") { ?>
	
								<span class="i-label">
								 	<span class="fmn-count-in-basket a-flash">
									 	<span class="e-icon fmn-icon-in-basket"></span>
									 	<span class="e-label"><?=$dishes[$i]["in-basket-count"]?></span>
								 	</span>
								</span>
	
							<? } /* end if */ ?>

							<? if ($dishes[$i]["timer"] == "yes") { ?>
								
								<span class="i-label">
									<span class="fmn-timer">
										<span class="e-icon fmn-icon-timer"></span>
										<span class="e-label"><?=$dishes[$i]["special-ends"]?></span>
									</span>
								</span>
								 	
							<? } /* end if */ ?>

						 	<? if ($dishes[$i]["instock"] == "no") { ?>
						 		
						 		<span class="i-label">
								 	<span class="fmn-outofstock">
									 	<span class="e-icon fmn-icon-forbidden"></span>
									 	<span class="e-label">Out of Stock</span>
								 	</span>
							 	</span>
						 	
						 	<? } /* end if */ ?>
						
							<? if ($dishes[$i]["pay-online"] == "yes") { ?>
							
							<span class="i-label">
							 	<span class="fmn-payOnline">
									<span class="e-icon fmn-icon-online-payment"></span>
								 	<span class="e-label">Pay Online</span>
							 	</span>
						 	</span>

							<? } /* end if */ ?>

					 	</span>
					 	
					</div><!-- tile -->
					
					<div class="i-tile-under">
						
						<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
													
						<span class="e-title"><?=$dishes[$i]["name"]?></span>
						
						<span class="e-priceAndOptions">				

							<? if ($dishes[$i]["special"] == "no") { ?>
	
									<span class="e-subtitle">
										<span class="fmn-common-price">
											<span class="e-normal"><?=$dishes[$i]["price"]?></span>
										</span>
									</span>
	
							<? } else if ($dishes[$i]["special"] == "yes") { ?>
	
									<span class="e-subtitle">
										<span class="fmn-common-price">
											<span class="e-original"><?=$dishes[$i]["price-original"]?></span> 
											<span class="e-special"><?=$dishes[$i]["price-special"]?></span>
										</span>
									</span>
	
							<? } /* end if */ ?>
						
							<span class="e-options">
								<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
									<span class="e-icon fmn-icon-add-to-basket"></span>
									<span class="e-caption">Buy</span>
								</a>
							</span>
						
						</span>

						
					</div>
					
					</div><!-- tile-wrap -->

			<? } /* end for */ ?>  		
			
		</div><!-- tiles -->
		
	</div><!-- fmn-products -->
	
	
	
	
	
	<div class="fmn-products debug-view-tiles debug-view-2 debug-view-tilesCaptionOutsideIngredients -m-noAddToBasket" style="display: block;"> 
		
		<div class="fmn-tiles m-transparentBlackPhotoLabels">
			
			<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
				<span class="e-caption">Basic Salads</span>
			</div>

			<? for ($i = 0; $i < count($dishes); $i++) { ?>
			
					<? if ($i == 7) { ?>
						<div class="fmn-products-title m-noPhoto">
							<span class="e-caption">Mediterranean Salads</span>
						</div>
					<? } ?>

					<? if ($i == 13) { ?>
						<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
							<span class="e-caption">Exotic Salads</span>
						</div>
					<? } ?>
			
					<div class="i-tile-wrap <? if ($dishes[$i]["instock"] == "no") { ?>m-outofstock<? } ?> <? if ($dishes[$i]["hasDetails"] == "no") { ?>m-noDetails<? } ?>">
					<div class="i-tile">
						
						<? if ($dishes[$i]["image"] != null ) { ?>
								<span class="e-photo" style="background-image: url(<?=$dishes[$i]["image"]?>);"></span>
						<? } else { ?>
								<span class="e-photo" style="background-image: url(images/no-photo.jpg);"></span>
						<? } /* end if */ ?>
						
					 	<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>

						<span class="e-info-bottomLeft fmn-labels">

							<? if ($dishes[$i]["in-basket"] == "yes") { ?>
	
								<span class="i-label">
								 	<span class="fmn-count-in-basket a-flash">
									 	<span class="e-icon fmn-icon-in-basket"></span>
									 	<span class="e-label"><?=$dishes[$i]["in-basket-count"]?></span>
								 	</span>
								</span>
	
							<? } /* end if */ ?>

							<? if ($dishes[$i]["timer"] == "yes") { ?>
								
								<span class="i-label">
									<span class="fmn-timer">
										<span class="e-icon fmn-icon-timer"></span>
										<span class="e-label"><?=$dishes[$i]["special-ends"]?></span>
									</span>
								</span>
								 	
							<? } /* end if */ ?>

						 	<? if ($dishes[$i]["instock"] == "no") { ?>
						 		
						 		<span class="i-label">
								 	<span class="fmn-outofstock">
									 	<span class="e-icon fmn-icon-forbidden"></span>
									 	<span class="e-label">Out of Stock</span>
								 	</span>
							 	</span>
						 	
						 	<? } /* end if */ ?>
						
							<? if ($dishes[$i]["pay-online"] == "yes") { ?>
							
							<span class="i-label">
							 	<span class="fmn-payOnline">
									<span class="e-icon fmn-icon-online-payment"></span>
								 	<span class="e-label">Pay Online</span>
							 	</span>
						 	</span>

							<? } /* end if */ ?>

					 	</span>
					 	
					</div><!-- tile -->
					
					<div class="i-tile-under">
						
						<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
													
						<span class="e-title"><?=$dishes[$i]["name"]?></span>
						<span class="e-subtitle">
							<span class="e-ingredients"><?=$dishes[$i]["ingredients"]?></span>
						</span>
						<span class="e-priceAndOptions">				

							<? if ($dishes[$i]["special"] == "no") { ?>
	
									<span class="e-subtitle">
										<span class="fmn-common-price">
											<span class="e-normal"><?=$dishes[$i]["price"]?></span>
										</span>
									</span>
	
							<? } else if ($dishes[$i]["special"] == "yes") { ?>
	
									<span class="e-subtitle">
										<span class="fmn-common-price">
											<span class="e-original"><?=$dishes[$i]["price-original"]?></span> 
											<span class="e-special"><?=$dishes[$i]["price-special"]?></span>
										</span>
									</span>
	
							<? } /* end if */ ?>
							
							
						
							<span class="e-options">
								<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
									<span class="e-icon fmn-icon-add-to-basket"></span>
									<span class="e-caption">Buy</span>
								</a>
							</span>
						
						</span>

						
					</div>
					
					</div><!-- tile-wrap -->

			<? } /* end for */ ?>  		
			
		</div><!-- tiles -->
		
	</div><!-- fmn-products -->
	
	

	
	
	

	<div class="fmn-products debug-view-list-photo debug-view-3 debug-view-listPhotos -m-noAddToBasket DISABLEm-eachLineQty -m-noPhotos" style="display: none;">
		
		<div class="fmn-list m-transparentBlackPhotoLabels">
			
			
			<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
				<span class="e-caption">Basic Salads</span>
			</div>

			<? for ($i = 0; $i < count($dishes); $i++) { ?>
					
					
					<? if ($i == 3) { ?>
						<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
<!-- 						<div class="fmn-products-title m-noPhoto"> -->
							<span class="e-caption">Mediterranean Salads</span>
						</div>
					<? } ?>

<!--
					<? if ($i == 13) { ?>
						<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
							<span class="e-caption">Exotic Salads</span>
						</div>
					<? } ?>
-->

					<div class="i-list-line m-row <? if ($dishes[$i]["with-sub-items"] == "yes") { ?>m-withsubitems<? } ?>  <? if ($dishes[$i]["instock"] == "no") { ?>m-outofstock<? } ?> <? if ($dishes[$i]["hasDetails"] == "no") { ?>m-noDetails<? } ?>">
						
						<span class="i-list-line-wrap m-photoName">
						
							<? if ($dishes[$i]["image"] != null ) { ?>
									<span class="i-list-line-cell m-photo"><a href="<?=$fmn_next_page_url?>"><img src="<?=$dishes[$i]["image"]?>"></a></span>
							<? } else { ?>
									<span class="i-list-line-cell m-photo"><a href="<?=$fmn_next_page_url?>"><img src="images/no-photo.jpg"></a></span>
							<? } /* end if */ ?>
							
							
							<span class="i-list-line-cell m-name">
								
								<span class="fmn-labels">
								
							
									<? if ($dishes[$i]["in-basket"] == "yes") { ?>
	
									 	<span class="i-label">
										 	<span class="fmn-count-in-basket">
											 	<span class="e-icon fmn-icon-in-basket"></span>
											 	<span class="e-label"><?=$dishes[$i]["in-basket-count"]?></span>
										 	</span>
									 	</span>
	
									<? } /* end if */ ?>
								
									<? if ($dishes[$i]["timer"] == "yes") { ?>
	
									 	<span class="i-label">
										 	<span class="fmn-timer">
											 	<span class="e-icon fmn-icon-timer"></span>
											 	<span class="e-label"><?=$dishes[$i]["special-ends"]?></span>
										 	</span>
									 	</span>
	
									<? } /* end if */ ?>
									
									<? if ($dishes[$i]["instock"] == "no") { ?>
										
										<span class="i-label">
										 	<span class="fmn-outofstock">
											 	<span class="e-icon fmn-icon-forbidden"></span>
											 	<span class="e-label">Out of Stock</span>
										 	</span>
										</span>
								 	
								 	<? } /* end if */ ?>
								 	
									<? if ($dishes[$i]["pay-online"] == "yes") { ?>
							
									<span class="i-label">
									 	<span class="fmn-payOnline">
											<span class="e-icon fmn-icon-online-payment"></span>
										 	<span class="e-label">Pay Online</span>
									 	</span>
								 	</span>
		
									<? } /* end if */ ?>
								 	
							 	
							 	</span>

								<a href="<?=$fmn_next_page_url?>"><?=$dishes[$i]["name"]?></a>
								
								<? if ($dishes[$i]["singleSubitem"] != null ) { ?>	
									<span class="e-subitem-name">
										<?=$dishes[$i]["singleSubitem"]?>
									</span>
								<? } /* end if */ ?>								
															
								<span class="e-detail">
								
									<? if ($dishes[$i]["description"] != null ) { ?>
											<span class="e-dataLabel">description:</span> <?=$dishes[$i]["description"]?><br>
									<? } else { ?>
									
									<? } /* end if */ ?>

									<? if ($dishes[$i]["ingredients"] != null ) { ?>
											<span class="e-dataLabel">ingredients:</span> <?=$dishes[$i]["ingredients"]?>
									<? } else { ?>
									
									<? } /* end if */ ?>
									
								</span>

							</span>
						</span>
						
						<? if ($dishes[$i]["special"] == "no") { ?>

								<span class="i-list-line-cell m-price">
									<span class="e-mobileLabel">price:</span>
									<span class="fmn-common-price">
										<span class="e-normal"><?=$dishes[$i]["price"]?></span>
									</span>
									<span class="e-item-price">
										<span class="e-item-price-label">per item:</span>
										<span class="e-item-price-number">€5.00</span>
									</span>
								</span>

						<? } else if ($dishes[$i]["special"] == "yes") { ?>

								<span class="i-list-line-cell m-price">
									<span class="e-mobileLabel">price:</span>
									<span class="fmn-common-price">
										<span class="e-original"><?=$dishes[$i]["price-original"]?></span>
										<span class="e-special"><?=$dishes[$i]["price-special"]?></span>
									</span>
									<span class="e-item-price">
										<span class="e-item-price-label">per item:</span> 
										<span class="e-item-price-number m-special">€5.00</span>
									</span>
								</span>

						<? } /* end if */ ?>

						<span class="i-list-line-cell m-qty">
							<a href="#"><span class="fmn-icon-minus"></span></a><input class="m-small" type="text" value="1"><a href="#"><span class="fmn-icon-plus"></span></a>
						</span>

						
						<span class="i-list-line-cell m-actions">
							<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
								<span class="e-icon fmn-icon-add-to-basket"></span>
								<span class="e-caption">Buy</span>
							</a>
						</span>
						
						<div class="e-subitems">
							<div class="i-subitem">
								
								<span class="i-subitem-cell m-labels-name">
									<span class="e-subitem-name">Small</span>
								</span>
								<span class="i-subitem-cell m-price">
									<span class="e-mobileLabel">price:</span> 
									<span class="fmn-common-price">
										<span class="e-original">€18.45</span> 
										<span class="e-special">€11.95</span>
									</span>
									<span class="e-item-price">
										<span class="e-item-price-label">per item:</span> 
										<span class="e-item-price-number m-special">€5.00</span>
									</span>
								</span>
								<span class="i-subitem-cell m-qty"><a href="#"><span class="fmn-icon-minus"></span></a><input class="m-small" type="text" value="1"><a href="#"><span class="fmn-icon-plus"></span></a></span>
								<span class="i-subitem-cell m-add">
									<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
										<span class="e-icon fmn-icon-add-to-basket"></span>
										<span class="e-caption">Buy</span>
									</a>
								</span>
								
							</div>
							
							<div class="i-subitem ">
								
								<span class="i-subitem-cell m-labels-name">
									<span class="e-subitem-labels">
										<? if ($dishes[$i]["sub-1-in-basket"] == "yes") { ?>
											<span class="i-subitem-label">
											<span class="fmn-count-in-basket">
											 	<span class="e-icon fmn-icon-in-basket"></span>
											 	<span class="e-label"><?=$dishes[$i]["sub-1-in-basket-count"]?></span>
										 	</span>
										 	</span>
									 	<? } else { } ?>
									 	<? if ($dishes[$i]["sub-1-out-of-stock"] == "yes") { ?>
											<span class="i-subitem-label">
											<span class="fmn-outofstock">
											 	<span class="e-icon fmn-icon-forbidden"></span>
											 	<span class="e-label">Out of Stock</span>
										 	</span>
										 	</span>
									 	<? } else { } ?>
									 	<? if ($dishes[$i]["sub-1-timer"] == "yes") { ?>
											<span class="i-subitem-label">
											<span class="fmn-timer">
												<span class="e-icon fmn-icon-timer"></span>
												<span class="e-label"><?=$dishes[$i]["sub-1-special-ends"]?></span>
											</span>
											</span>
									 	<? } else { } ?>
								 	</span>
								 	<span class="e-subitem-name">Medium</span>
							 	</span>
								<span class="i-subitem-cell m-price"><span class="e-mobileLabel">price:</span> <span class="fmn-common-price"><span class="e-original">€18.45</span> <span class="e-special">€11.95</span></span></span>
								<span class="i-subitem-cell m-qty"><a href="#"><span class="fmn-icon-minus"></span></a><input class="m-small" type="text" value="1"><a href="#"><span class="fmn-icon-plus"></span></a></span>
								<span class="i-subitem-cell m-add">
									<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
										<span class="e-icon fmn-icon-add-to-basket"></span>
										<span class="e-caption">Buy</span>
									</a>
								</span>
								
							</div>
						</div>
						
					</div>
					
			<? } /* end for */ ?>  	
			
		</div><!-- fmn-list -->
		
	</div><!-- fmn-products -->




	
	
	
	
	
	
	
	
	
	
	<div class="fmn-products debug-view-list debug-view-4 debug-view-listTwoCols -m-noAddToBasket m-noPhotos" style="display: block;">
		
		<div class="fmn-list m-twoCol">
			
			
			<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
				<span class="e-caption">Basic Salads</span>
			</div>

			<? for ($i = 0; $i < count($dishes); $i++) { ?>

			
					<? if ($i == 7) { ?>
						<div class="fmn-products-title m-noPhoto">
							<span class="e-caption">Mediterranean Salads</span>
						</div>
					<? } ?>

					<? if ($i == 13) { ?>
						<div class="fmn-products-title m-withPhoto" style="background-image: url(images/sample-category.jpg);">
							<span class="e-caption">Exotic Salads</span>
						</div>
					<? } ?>



					<div class="i-list-line m-row <? if ($dishes[$i]["hasDetails"] == "no") { ?>m-noDetails<? } ?>">

						<span class="i-list-line-wrap m-photoName noPhoto">
																		
							<span class="i-list-line-cell m-name">

								<span class="fmn-labels">
								
									<? if ($dishes[$i]["in-basket"] == "yes") { ?>
	
										<span class="i-label">									 	
										 	<span class="fmn-count-in-basket">
											 	<span class="e-icon fmn-icon-in-basket"></span>
											 	<span class="e-label"><?=$dishes[$i]["in-basket-count"]?></span>
										 	</span>
										</span>
	
									<? } /* end if */ ?>
									
									<? if ($dishes[$i]["timer"] == "yes") { ?>
	
									 	<span class="i-label">
										 	<span class="fmn-timer">
											 	<span class="e-icon fmn-icon-timer"></span>
											 	<span class="e-label"><?=$dishes[$i]["special-ends"]?></span>
										 	</span>
									 	</span>
	
									<? } /* end if */ ?>
									
									<? if ($dishes[$i]["instock"] == "no") { ?>
										
										<span class="i-label">
										 	<span class="fmn-outofstock">
											 	<span class="e-icon fmn-icon-forbidden"></span>
											 	<span class="e-label">Out of Stock</span>
										 	</span>
										</span>
								 	
								 	<? } /* end if */ ?>
								 	
								
									<? if ($dishes[$i]["pay-online"] == "yes") { ?>
									
									<span class="i-label">
									 	<span class="fmn-payOnline">
											<span class="e-icon fmn-icon-online-payment"></span>
										 	<span class="e-label">Pay Online</span>
									 	</span>
								 	</span>
		
									<? } /* end if */ ?>
								 	
							 	
							 	</span>

								<a href="<?=$fmn_next_page_url?>"><?=$dishes[$i]["name"]?></a>
							
								<span class="e-detail">
								
									<? if ($dishes[$i]["description"] != null ) { ?>
											<span class="e-dataLabel">description:</span> <?=$dishes[$i]["description"]?><br>
									<? } else { ?>
									
									<? } /* end if */ ?>

									<? if ($dishes[$i]["ingredients"] != null ) { ?>
											<span class="e-dataLabel">ingredients:</span> <?=$dishes[$i]["ingredients"]?>
									<? } else { ?>
									
									<? } /* end if */ ?>
									
								</span>
								
								


							</span>
						</span>
						
						<? if ($dishes[$i]["special"] == "no") { ?>

								<span class="i-list-line-cell m-price">
									<span class="e-mobileLabel">price:</span>
									<span class="fmn-common-price">
										<span class="e-normal"><?=$dishes[$i]["price"]?></span>
									</span>
								</span>

						<? } else if ($dishes[$i]["special"] == "yes") { ?>

								<span class="i-list-line-cell m-price">
									<span class="e-mobileLabel">price:</span>
									<span class="fmn-common-price">
										<span class="e-original"><?=$dishes[$i]["price-original"]?></span>
										<span class="e-special"><?=$dishes[$i]["price-special"]?></span>
									</span>
								</span>

						<? } /* end if */ ?>
						
						<span class="i-list-line-cell m-actions">
							<a class="e-addToBasket debug-clickTap" href="javascript:void(0)">
								<span class="e-icon fmn-icon-add-to-basket"></span>
								<span class="e-caption">Buy</span>
							</a>
						</span>
					</div>
					
			<? } /* end for */ ?>  	
			
		</div><!-- fmn-list -->
		
	</div><!-- fmn-products -->


</div><!-- fmn-screen-content -->




<!-- <div class="m-smart-scroll" style="position: fixed; left: 40px; width: 100px; height: 100px; background-color: gold;"></div> -->



<style>

/*
	.debug-view-0,
	.debug-view-1,
	.debug-view-2,
	.debug-view-3,
	.debug-view-4 {
		padding-bottom: 100px !important;
		border-bottom: dotted 3px silver !important;
		margin-bottom: 100px !important;
	}
*/
	
</style>
					
<script>

	$(document).ready(function() {
		totalViews = 4;	
		visibleView = "<?=$fmn_visibleView?>";
		console.log(visibleView);
		for (i = 0; i <= totalViews; i++) {
			$(".debug-view-"+i).css("display", "none");
		}
		$(".debug-view-"+visibleView).css("display", "block");	
		$(".debug-view-link-"+visibleView).addClass("selected");

	});
/*
	
	function cycleView() {
		for (i = 0; i <= totalViews; i++) {
			$(".debug-view-"+i).css("display", "none");
			$(".debug-view-link-"+i).removeClass("selected");
		}
		if (visibleView < totalViews) {
			visibleView = visibleView+1;
		} else {
			visibleView = 1;
		}
		$(".debug-view-"+visibleView).css("display", "block");	
		$(".debug-view-link-"+visibleView).addClass("selected");	
	}
*/
	
	function setView(myView) {
		for (i = 0; i <= totalViews; i++) {
			$(".debug-view-"+i).css("display", "none");
			$(".debug-view-link-"+i).removeClass("selected");
		}
		$(".debug-view-"+myView).css("display", "block");	
		$(".debug-view-link-"+myView).addClass("selected");			
	}

</script>




<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('tilesCaptionInside');" class="debug-view-link debug-view-link-0 debug-view-link-tilesCaptionInside">Tiles caption inside<span class="debug-visible">Current</span></a>
	<a href="javascript:setView('tilesCaptionOutside');" class="debug-view-link debug-view-link-1 debug-view-link-tilesCaptionOutside">Tiles caption outside <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('tilesCaptionOutsideIngredients');" class="debug-view-link debug-view-link-2 debug-view-link-tilesCaptionOutsideIngredients">Tiles captions outside + ingredients<span class="debug-visible">Current</span></a>
	<a href="javascript:setView('listPhotos');" class="debug-view-link debug-view-link-3 debug-view-link-listPhotos">List with Photos <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('listTwoCols');" class="debug-view-link debug-view-link-4 debug-view-link-listTwoCols">List 2 Columns <span class="debug-visible">Current</span></a>
<!-- 	<a href="javascript:cycleView();">Next View</a> -->
</div>