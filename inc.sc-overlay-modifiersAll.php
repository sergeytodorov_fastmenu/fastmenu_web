<div class="fmn-overlay " style="">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">

		<a href="#" class="fmn-overlay-close"><span class="fmn-icon-delete"></span></a>
		
		<div class="debug-view-7">

			<div class="fmn-overlay-title">
				<h3>Caprese Salad with Long Title to Illustrate How Things Work</h3>
			</div>
			

			<div class="fmn-form fmn-details-overlay">
				
	
				
				
				<div class="fmn-details-options">
			
					<div class="fmn-form-option m-error">
						<label>Size</label>
						<div class="fmn-form-field">
							<span class="fmn-form-option-error">This is mandatory. Please select one</span>
							<div class="fmn-form-buttonArray">
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Small</span><span class="e-subCaption">€12.45</span></button>
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Medium</span><span class="e-subCaption">€18.45</span></button>
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Large</span><span class="e-subCaption">€24.45</span></button>
							</div>
						</div>
					</div>
					
					<div class="fmn-message m-alert">
						This item is out of stock.
					</div>
					
					<div class="fmn-form-option m-error">
						<label>What do you like?</label>
						<div class="fmn-form-field">
							<span class="fmn-form-option-error">This is mandatory. Please select one</span>
							<div class="fmn-form-buttonArray">
								<a class="fmn-button m-invert" href="#"><span>Gold</span></a>
								<a class="fmn-button m-invert" href="#"><span>Silver</span></a>
								<a class="fmn-button m-invert" href="#"><span>Platinum</span></a>
								<a class="fmn-button m-invert" href="#"><span>Diamonds</span></a>
							</div>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Additions</label>
						<div class="fmn-form-field">
							<div class="fmn-form-buttonArray">
								<a class="fmn-button m-invert m-twoLine" href="#"><span>Cheese</span><span class="e-subCaption">€2.45</span></a>
								<a class="fmn-button m-invert m-selected m-twoLine" href="#"><span>Ham</span><span class="e-subCaption">€1.45</span></a>
								<a class="fmn-button m-invert m-twoLine" href="#"><span>Coke</span><span class="e-subCaption">€4.45</span></a>
							</div>
							<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span></a>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Drop-down</label>
						<div class="fmn-form-field mod-select">
							<select style="">
								<option>Mitte</option>
								<option selected>Prenzlauer Berg</option>
							</select>
						</div>
					</div>
					
					
					
					<div class="fmn-form-option">
						<label>Drop-down Prices</label>
						<div class="fmn-form-field mod-select">
							<select style="">
								<option>White Bread +€2.45</option>
								<option selected>Black Bread +€2.45</option>
								<option>Full-grain Bread +€2.45</option>
							</select>
						</div>
					</div>
					
					
					<div class="fmn-form-option">
						<label>Radios Two-cols</label>
						<div class="fmn-form-field">
							<div class="fmn-form-simpleArray m-twoCol">
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier"> <span>Double meat</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier" checked=""> <span>Double cheese</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier"> <span>Double tomatoes</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier" checked=""> <span>Double cucumbers</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier"> <span>Double snails</span></label>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="fmn-form-option">
						<label>Checks Two-cols</label>
						<div class="fmn-form-field">
							<div class="fmn-form-simpleArray m-twoCol">
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double meat</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double tomatoes</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double snails</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>
		
							</div>
						</div>
					</div>
					
					
					
					<div class="fmn-form-option">
						<label>Radios One-col</label>
						<div class="fmn-form-field">
							<div class="fmn-form-simpleArray">
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier"> <span>Double meat</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier" checked=""> <span>Double cheese</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier"> <span>Double tomatoes</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier" checked=""> <span>Double cucumbers</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="radio" name="modifier"> <span>Double snails</span></label>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="fmn-form-option">
						<label>Checks One-col</label>
						<div class="fmn-form-field">
							<div class="fmn-form-simpleArray">
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double meat</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double tomatoes</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double snails</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>
		
							</div>
						</div>
					</div>
					
					
					
					<div class="fmn-form-option">
						<label>Disabled</label>
						<div class="fmn-form-field">
							<div class="fmn-form-buttonArray">
								<a class="fmn-button m-invert m-twoLine m-disabled" href="#"><span>Cheese</span><span class="e-subCaption">€2.45</span></a>
								<a class="fmn-button m-invert m-twoLine m-disabled" href="#"><span>Ham</span><span class="e-subCaption">€1.45</span></a>
								<a class="fmn-button m-invert m-twoLine m-disabled" href="#"><span>Coke</span><span class="e-subCaption">€4.45</span></a>
							</div>
						</div>
					</div>
		
					<div class="fmn-form-option">
						<label>Mixed With and Without Price</label>
						<div class="fmn-form-field">
							<div class="fmn-form-buttonArray">
								<button class="fmn-button m-invert m-twoLine" href="#"><span>This</span><span class="e-subCaption">€12.45</span></button>
								<button class="fmn-button m-invert m-twoLine" href="#"><span>The Longer That</span><span class="e-subCaption">€18.45</span></button>
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Short</span><span class="e-subCaption">&nbsp;</span></button>
							</div>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Quantity</label>
						<div class="fmn-form-field fmn-form-numberBox">
							<a href="#"><span class="fmn-icon-minus"></span></a><input class="" type="text" value="297"><a href="#"><span class="fmn-icon-plus"></span></a>
						</div>
						<span class="fmn-form-option-help">3 available</span>
					</div>
		
				</div><!-- fmn-details-options -->
		
		
				<div class="fmn-message">
					<span class="e-icon fmn-icon-online-payment"></span> <span class="e-icontext">This item requires advance online payment.</span>
				</div>
		
				
				<div class="fmn-details-priceAndAddToBasket">
							
					<div class="fmn-details-price">
						<div class="fmn-details-price-main">Box of 4 rolls: <span class="fmn-common-price"><span class="e-original">€10.00</span> <span class="e-special">€8.00</span></span></div>
						<div class="fmn-details-price-save">You save: <span class="fmn-common-price m-secondary"><span class="e-special">€2.00</span></span></div>
					</div>
					
					
					
					<div class="fmn-details-addToBasket">
						
						<span class="fmn-labels">
							
							<span class="i-label">
								<span class="fmn-timer">
									<span class="e-icon fmn-icon-timer"></span>
									<span class="e-label">03:27:11</span>
								</span>
							</span>
		
					 	</span>
					 	
						<button type="submit" class="m-large">Add to Basket</button>
					</div>
				</div><!-- fmn-details-priceAndAddToBasket -->
				
				<div class="fmn-details-priceAndAddToBasket">
							
					<div class="fmn-details-price">
						<div class="fmn-details-price-single">Box of 4 rolls: <span class="fmn-common-price m-secondary"><span class="e-original">€10.00</span> <span class="e-special">€8.00</span></span></div>
						<div class="fmn-details-price-main">Total for 2: <span class="fmn-common-price"><span class="e-original">€20.00</span> <span class="e-special">€16.00</span></span></div>
						<div class="fmn-details-price-save">You save: <span class="fmn-common-price m-secondary"><span class="e-special">€4.00</span></span></div>
					</div>
					
					
					
					<div class="fmn-details-addToBasket">
						
						<span class="fmn-labels">
							
							<span class="i-label">
								<span class="fmn-timer">
									<span class="e-icon fmn-icon-timer"></span>
									<span class="e-label">03:27:11</span>
								</span>
							</span>
		
					 	</span>
					 	
						<button type="submit" class="m-large">Add to Basket</button>
					</div>
				</div><!-- fmn-details-priceAndAddToBasket -->
				
				
										
			</div>
		</div>
		


		

	</div><!-- fmn-overlay-box -->
	
	
	
</div><!-- fmn-overlay -->

