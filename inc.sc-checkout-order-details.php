<?
	$views = "available";
	
	$allowedViews = array(
    	"core" => array( 
	    	"visibleView" => "core"
    	),
    	"all" => array( 
	    	"visibleView" => "all"
    	)

	);

	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];
	
?>


	<div class="fmn-screen-content-wrap m-reducedWidth-oneThird m-checkout m-orderDetails fmn-orderDetails">

		<div class="fmn-form">
			
			<div class="debug-view debug-view-core">

				<div class="fmn-form-option">
					<label>Time for Delivery</label>
					<div class="fmn-form-field mod-select">
						<select style="">
							<option selected>As Soon As Possible</option>
							<option>15:30</option>
							<option>15:45</option>
							<option>16:00</option>
							<option>16:15</option>
							<option>16:30</option>	
						</select>
					</div>
				</div>
				
				<div class="fmn-form-option ">
						
						<div class="orderDetails-note">
							<p>
								<span class="n-icon fmn-icon-clock-1"></span>
								<span class="n-caption">
									Estimated hour: 10:30
								</span>
							</p>
						</div>
				</div>
				
				<div class="fmn-form-option">
					<label>Notes for This Order</label>
					<div class="fmn-form-field">
						<textarea style="min-height: 60px; height: 60px;" placeholder="e.g. if you would like items packaged separately"></textarea>
					</div>
				</div>			
				
			</div>


			<div class="debug-view debug-view-all">
					
				<div class="fmn-form-option">
					<label>Time for Delivery</label>
					<div class="fmn-form-field mod-select">
						<select style="">
							<option selected>As Soon As Possible</option>
							<option>15:30</option>
							<option>15:45</option>
							<option>16:00</option>
							<option>16:15</option>
							<option>16:30</option>	
						</select>
					</div>
				</div>
				
				<div class="fmn-form-option ">
						
						<div class="orderDetails-note">
							<p>
								<span class="n-icon fmn-icon-clock-1"></span>
								<span class="n-caption">
									Estimated hour: 10:30
								</span>
							</p>
						</div>
				</div>


				<div class="fmn-form-option ">
					<div class="orderDetails-note m-highlight">
						
						<p>
							<span class="n-message">Unfortunately all our locations are closed for the day. Please note the next available day below.</span>
						</p>
						<p>
							<span class="n-icon fmn-icon-calendar-check"></span>
							<span class="n-caption">
								Monday, September 17
							</span>
							<a class="n-link" href="">See Details</a>
						</p>
						
					</div>
				</div>
				
	<!--
				
				<div class="fmn-form-option">
					<label>Order Type</label>
					<div class="fmn-form-field">
						<div class="fmn-form-buttonArray">
							<a class="fmn-button m-invert selected" href="#"><span>Delivery</span></a>
							<a class="fmn-button m-invert" href="#"><span>Takeaway</span></a>
						</div>
					</div>
				</div>
	-->
	
				<div class="fmn-form-option">
					<label>Time for Delivery</label>
					<div class="fmn-form-field mod-select">
						<select style="">
							<option selected>As Soon As Possible</option>
							<option>15:30</option>
							<option>15:45</option>
							<option>16:00</option>
							<option>16:15</option>
							<option>16:30</option>	
						</select>
					</div>
				</div>
				
<!--
				<div class="fmn-form-option ">
						
						<div class="fmn-orderTypeLocation">
							<span class="n-time">
								<span class="n-icon fmn-icon-clock-1"></span>
								<span class="n-caption">
									Estimated hour: <b>10:30</b>
								</span>
							</span>
						</div>
				</div>
-->
				
				<div class="fmn-form-option ">
					<div class="orderDetails-note m-highlight">
						<p class="n-time">
							<span class="n-message">Please note the earliest possible hour for fulfilling your order.</span>
						</p>
						<p>
							<span class="n-icon fmn-icon-clock-1"></span>
							<span class="n-caption">
								Estimated hour: 10:30
							</span>
						</p>
					</div>
				</div>
				
				<div class="fmn-form-option">
					<label>Deliver To</label>
					<div class="fmn-form-field">
						<div class="fmn-form-buttonArray m-fullWidth m-2">
							<a class="fmn-button m-invert" href="#"><span>Building</span></a>
							<a class="fmn-button m-selected m-invert" href="#"><span>Door</span></a>
						</div>
					</div>
				</div>
				
				<div class="fmn-form-option ">
					<div class="orderDetails-note">
						
						<p>
							<span class="n-message">Please consider tipping our delivery driver as a token of appreciation for him coming to the door.</span>
						</p>
						
					</div>
				</div>

				
				<div class="fmn-form-option">
					<label>Notes for This Order</label>
					<div class="fmn-form-field">
						<textarea style="min-height: 60px; height: 60px;" placeholder="e.g. if you would like items packaged separately"></textarea>
					</div>
				</div>				
			</div>
			
		</div><!-- fmn-form -->

	</div><!-- fmn-screen-content-wrap -->
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Next</button> -->
		<a  href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Next: Contact Info</a>
		
	</div><!-- fmn-actions -->





<script>

	$(document).ready(function() {
		visibleView = "<?=$fmn_visibleView?>";
		$(".debug-view").css("display", "none");
		$(".debug-view-"+visibleView).css("display", "block");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+visibleView).addClass("selected");

	});
	
	function setView(myView) {
		$(".debug-view").css("display", "none");
		$(".debug-view-"+myView).css("display", "block");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+myView).addClass("selected");			
	}

</script>




<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('core');" class="debug-view-link debug-view-link-core">Core <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('all');" class="debug-view-link debug-view-link-all">All <span class="debug-visible">Current</span></a>
</div>