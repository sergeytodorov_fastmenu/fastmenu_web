<div class="fmn-screen-content-wrap m-reducedWidth-oneHalf">

	<div class="fmn-announcements">
		
		<div class="i-announcement-wrap">
			
			<div class="i-announcement">
				
				<img src="images/sample-category.jpg">
				
				<h2>Event Announcement Title</h2>

				<div class="n-dateTimeLocation">
					<span class="n-dateTime">on Wednesday, July 27, 2016 at 19:00</span> <span class="n-location">at Turquoise Flying Lotus</span>
				</div>

				<div class="n-actions">
					<a href="#" class="fmn-button m-invert">Make Reservation</a>
				</div>
				
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
				
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>

				<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>

			</div>
			
		</div>

	</div>

</div>
