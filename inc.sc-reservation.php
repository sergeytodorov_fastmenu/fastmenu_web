<link href='plugins/calendar/styles-bootstrap-datetimepicker-build.css' rel='stylesheet' type='text/css'>
<script src='plugins/calendar/moment.js'></script>
<script src='plugins/calendar/bootstrap-datetimepicker.js'></script>

	<div class="fmn-screen-messages">	
		<div class="fmn-message m-image m-970">
			<img src="images/sample-ad.jpg"/>
		</div>
	</div>

	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">

		<div class="fmn-screen-intro m-center">
			If you would like to make an advance order with your reservation, please submit a Pre-order instead. <a href="#">Go to Pre-order now</a>
		</div>

		<div class="fmn-form">
			
			
			<div class="fmn-screen-content-wrap-columns">
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					
					<div class="fmn-form-option">
						<label>
							Location for Reservation
						</label>
						<div class="fmn-form-field mod-select">
							<select>
								<option>Turquoise Flying Lotus (17 Street Name Address)</option>
								<option>Yellow Flying Lotus (17 Long Boulevard Name Goes Here)</option>
								<option>Blue Flying Lotus (17 Beep)</option>
							</select>
						</div>
					</div>
					
					
					<div class="fmn-form-option">
						<label>
							Location for Reservation
						</label>
						<div class="fmn-form-field">
							<input type="text" disabled value="Yellow Flying Lotus (17 Long Boulevard Name Goes Here)">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Location for Reservation
						</label>
						<div class="fmn-form-option-text">
							<span class="n-large">Yellow Flying Lotus</span> <span class="n-small">(17 Long Boulevard Name Goes Here)</span>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Date and Time for Reservation
							<span class="fmn-form-option-details">note that days and times that cannot be selected are outside the restaurant's working hours</span>
						</label>
		
						<div id="datetimepicker12"></div>
										    
					    <script type="text/javascript">
						    
					        $(function () {
					            $('#datetimepicker12').datetimepicker({
					                inline: true,
					                sideBySide: true,
					                icons: {
						                up: 'fmn-icon-arrow-up',
						                down: 'fmn-icon-arrow-down',
						                previous: 'fmn-icon-arrow-left',
						                next: 'fmn-icon-arrow-right'
					                },
					                format: 'MMMM YYYY DD, HH:mm'
					            });
					        });
					        
					    </script>
		
					</div>
					
					
				</div>

				<div class="fmn-screen-content-wrap-column m-oneHalf">
					
					<div class="fmn-form-option">
						<label>
							Name for Reservation
						</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Number of Persons
						</label>
						<div class="fmn-form-field fmn-form-numberBox">
							<a href="#"><span class="fmn-icon-minus"></span></a><input class="" type="text" value="297"><a href="#"><span class="fmn-icon-plus"></span></a>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Your Phone Number
							<span class="fmn-form-option-details">we may need to update you promptly on your reservation</span>
						</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
						<span class="fmn-form-option-help">please follow format XXX XXX XXXX</span>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Email Address
						</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Notes for the Reservation <sup>Optional</sup>
							<span class="fmn-form-option-details">e.g. do you want a window or terrace table</span>
						</label>
						<div class="fmn-form-field">
							<textarea></textarea>
						</div>
					</div>
					
				</div>
			</div>			
			
			
			
			
			
					
			
			
			
			
			
			
					
			
		</div><!-- fmn-form -->
	
	</div><!-- fmn-screen-content-wrap -->
	<div class="fmn-actions m-right">
		
		<button class="m-large" value="Checkout">Submit Reservation</button>
		
	</div><!-- fmn-actions -->
