<div class="fmn-orders">

	<div class="fmn-list m-row-clickable">
		
		<div class="i-list-line m-header">
			<span class="i-list-line-cell m-orderNo">order no.</span>
			<span class="i-list-line-cell m-dateTime">ordered on</span>
			<span class="i-list-line-cell m-details">details</span>
			<span class="i-list-line-cell m-status">status</span>
			<span class="i-list-line-cell m-orderTotal">total</span>
			<span class="i-list-line-cell m-actions"></span>
		</div>
		
		<div class="i-list-line m-row">
			<span class="i-list-line-cell m-orderNo">84903984</span>
			<span class="i-list-line-cell m-dateTime">01.12.2015 at 11:50</span>
			<span class="i-list-line-cell m-details">
				<span>Delivery to Home</span>
				<span class="e-detail">
					<span class="e-dataLabel">ordered for:</span> 01.12.2015 at 15:00<br>
					<span class="e-dataLabel">contents:</span> Veal Pepper Steak, Cod Fillet
				</span>
			</span>
			<span class="i-list-line-cell m-status"><span class="e-mobileLabel">status:</span>Scheduled</span></span>
			<span class="i-list-line-cell m-orderTotal"><span class="e-mobileLabel">total:</span><span class="fmn-common-price"><span class="e-normal">€18.45</span></span></span>
			<span class="i-list-line-cell m-actions">
				<a href="#" title="Order Details"><span class="fmn-icon-details"></span></a>
				<a href="#" title="Repeat Order"><span class="fmn-icon-repeat"></span></a>
			</span>
		</div>
		
		<div class="i-list-line m-row">
			<span class="i-list-line-cell m-orderNo">84903984</span>
			<span class="i-list-line-cell m-dateTime">01.12.2015 at 11:50</span>
			<span class="i-list-line-cell m-details">
				<span>Delivery to Office</span>
				<span class="e-detail">
					<span class="e-dataLabel">ordered for:</span> 01.12.2015 at 15:00<br>
					<span class="e-dataLabel">contents:</span> Veal Pepper Steak, Cod Fillet
				</span>
			</span>
			<span class="i-list-line-cell m-status"><span class="e-mobileLabel">status:</span>Scheduled</span>
			<span class="i-list-line-cell m-orderTotal"><span class="e-mobileLabel">total:</span><span class="fmn-common-price"><span class="e-normal">€18.45</span></span></span>
			<span class="i-list-line-cell m-actions">
				<a href="<?=$fmn_next_page_url?>" title="Order Details"><span class="fmn-icon-details"></span></a>
				<a href="#" title="Repeat Order"><span class="fmn-icon-repeat"></span></a>
			</span>
		</div>
	
	</div><!-- fmn-list -->
	
	<div class="fmn-list-paging">
		<div class="n-prev"><a href="#"><span class="e-icon fmn-icon-arrow-left"></span><span class="e-caption">Previous Page</span></a></div>
		<div class="n-current">
			<div class="fmn-form-field mod-select" style="width: 120px; display: inline-block;">
				<select>
					<option>Page 1</option>
					<option>Page 2</option>
					<option selected="">Page 3</option>
					<option>Page 4</option>
					<option>Page 5</option>
					<option>Page 6</option>
					<option>Page 7</option>
			</select>
			</div>
		</div>
		<div class="n-next"><a href="#"><span class="e-caption">Next Page</span><span class="e-icon fmn-icon-arrow-right"></span></a></div>		
	</div>
	
</div><!-- fmn-orders -->
