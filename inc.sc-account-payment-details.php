	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirdsWithSide">
		
		<div class="fmn-form">
			
			<div class="fmn-blocks m-twoCols">
				
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>Payment Defaults</h4>
						<div class="e-data-lines">
							<div class="i-data-line">
								<span class="e-label">Payment Method</span>
								<span class="e-value">Pay on Location</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">Invoice Type</span>
								<span class="e-value">None</span>
							</div>
						</div>
						<a href="dynamic.php?page=account-payment-defaults" class="fmn-button">Edit</a>
					</div>
				</div>

			</div><!-- fmn-blocks -->
			
			<div class="fmn-blocks m-twoCols">

				<div class="i-block-wrap">
					<div class="i-block">
						<h4>Individual Invoice Details</h4>
						<div class="e-data-lines">
							<div class="i-data-line">
								<span class="e-label">Individual Name</span>
								<span class="e-value">John Smith</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">Citizen ID (EGN)</span>
								<span class="e-value">8001011234</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">City</span>
								<span class="e-value">Sofia, 1000</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">Address</span>
								<span class="e-value">11, Ivan Vazov Str.</span>
							</div>
						</div>
						<a href="dynamic.php?page=account-company-invoice-details" class="fmn-button">Edit</a>
					</div>
				</div>
				
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>Company Invoice Details</h4>
						<div class="e-data-lines">
							<div class="i-data-line">
								<span class="e-label">Company Name</span>
								<span class="e-value">John Smith Co.</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">Company ID</span>
								<span class="e-value">1010201301</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">Company Accountable Person</span>
								<span class="e-value">John Smith</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">City</span>
								<span class="e-value">Sofia, 1000</span>
							</div>
							<div class="i-data-line">
								<span class="e-label">Company Address</span>
								<span class="e-value">11, Ivan Vazov Str.</span>
							</div>
						</div>
						<a href="dynamic.php?page=account-company-invoice-details" class="fmn-button">Edit</a>
					</div>
				</div>

				
			</div><!-- fmn-blocks -->

		</div><!-- fmn-form -->
		
	</div><!-- fmn-screen-content-wrap -->
