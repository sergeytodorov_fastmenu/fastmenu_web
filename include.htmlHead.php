<title>Fast Menu</title>

<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">

<link href='css/styles-static.css' rel='stylesheet' type='text/css'>
<link href='css/styles-main.css' rel='stylesheet' type='text/css'>

<link href='css/styles-addon.css' rel='stylesheet' type='text/css'>
<link href='css/styles-debug.css' rel='stylesheet' type='text/css'>
<link href='css/styles-brand-frame.css' rel='stylesheet' type='text/css'>

<meta charset="utf-8">

<!-- viewport -->
<meta name="viewport" content="initial-scale=1.0, width=device-width, height=device-height" />

<!-- load jQuery -->
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js'></script>

<!-- Font Awesome
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->

<link rel="stylesheet" href="icons/icons.css">



<script>

	$(document).ready(function() {
		console.log( $(this).scrollTop() );		
		if ($(this).scrollTop() < 50) {
			$(".fmn-header").removeClass("mod-scrolled");
		} else {
			$(".fmn-header").addClass("mod-scrolled");
		}
	});	
	
	$(window).scroll(function() {
		console.log( $(this).scrollTop() );
		if ($(this).scrollTop() < 50) {
			$(".fmn-header").removeClass("mod-scrolled");
		} else {
			$(".fmn-header").addClass("mod-scrolled");
		}
	});



/*
	var myIframeBehavior = window.parent.myIframeBehavior;

	$(document).ready(function() {

		var myScrollOffset = $(".fmn-brand-frame").height() + 50;
		console.log( myScrollOffset );
		console.log( $(this).scrollTop() );
		

		if (myIframeBehavior == 1) {
			window.parent.myCall();
		}

		if (myIframeBehavior != 1) {
			if ($(this).scrollTop() < myScrollOffset) {
				$(".fmn-fixed-header").css("display","none");
				$("#fmnid-toolbar").detach().appendTo("#fmnid-toolbar-not-scrolled");
				$("#fmnid-toolbar").removeClass("m-scrolled");
				$("#fmnid-app-location").detach().appendTo("#fmnid-app-location-not-scrolled");
				$("#fmnid-app-location").removeClass("m-scrolled");
			} else {
				$(".fmn-fixed-header").css("display","block");
				$("#fmnid-toolbar").detach().appendTo("#fmnid-toolbar-scrolled");
				$("#fmnid-toolbar").addClass("m-scrolled");
				$("#fmnid-app-location").detach().appendTo("#fmnid-app-location-scrolled");
				$("#fmnid-app-location").addClass("m-scrolled");
			}
		}
		
	});	
	
	$(window).scroll(function() {
		
		var myScrollOffset = $(".fmn-brand-frame").height() + 50;
		console.log( myScrollOffset );
		console.log( $(this).scrollTop() );
		
		if (myIframeBehavior != 1) {
			if ($(this).scrollTop() < myScrollOffset) {
				$(".fmn-fixed-header").css("display","none");
				$("#fmnid-toolbar").detach().appendTo("#fmnid-toolbar-not-scrolled");
				$("#fmnid-toolbar").removeClass("m-scrolled");
				$("#fmnid-app-location").detach().appendTo("#fmnid-app-location-not-scrolled");
				$("#fmnid-app-location").removeClass("m-scrolled");
			} else {
				$(".fmn-fixed-header").css("display","block");
				$("#fmnid-toolbar").detach().appendTo("#fmnid-toolbar-scrolled");
				$("#fmnid-toolbar").addClass("m-scrolled");
				$("#fmnid-app-location").detach().appendTo("#fmnid-app-location-scrolled");
				$("#fmnid-app-location").addClass("m-scrolled");
			}
		}
	});

	
	function mySetTop2(mySetTop2) {

		if (mySetTop2 <= 0) {
			frameSetTop2 = 0;
		} else {
			frameSetTop2 = mySetTop2;
		}

		$(".m-smart-scroll").css("top", frameSetTop2);

		if (frameSetTop2 < 50 ) {
			
			$(".fmn-fixed-header").css("display","none");
			
			$("#fmnid-toolbar").detach().appendTo("#fmnid-toolbar-not-scrolled");
			$("#fmnid-toolbar").removeClass("m-scrolled");
			
			$("#fmnid-app-location").detach().appendTo("#fmnid-app-location-not-scrolled");
			$("#fmnid-app-location").removeClass("m-scrolled");
			
		} else {
			
			$(".fmn-fixed-header").css("display","block");
			
			$("#fmnid-toolbar").detach().appendTo("#fmnid-toolbar-scrolled");
			$("#fmnid-toolbar").addClass("m-scrolled");
			
			$("#fmnid-app-location").detach().appendTo("#fmnid-app-location-scrolled");
			$("#fmnid-app-location").addClass("m-scrolled");
			
		}

	}
*/

/*
	$(window).on('resize load', function () { 
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
	});
*/





</script>