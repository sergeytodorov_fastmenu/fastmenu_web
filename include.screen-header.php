
		<? if ($fmn_page_type == "checkout") { ?>

			<div class="fmn-steps">
				
				<a href="dynamic.php?page=checkout-complete-address&view=guest" class="i-step num-1">
					<span class="step-graphic">
						<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="3.5 13.5 3.5 23.5 9.5 23.5 9.5 16.5 14.5 16.5 14.5 23.5 20.5 23.5 20.5 14"></polyline>
					            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="0.5 13 12 1.5 23.5 13"></polyline>
					            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="16 2.5 19.5 2.5 19.5 6"></polyline>
						    </g>
						</svg>
						<? include("icon.steps-checkmark.php"); ?>
					</span>
					<span class="step-label">Address</span>
				</a>
				
				<span class="i-separator num-1"><? include("icon.steps-arrow.php"); ?></span>
				
				<a href="dynamic.php?page=checkout-order-details&view=core" class="i-step num-2">
					<span class="step-graphic">
						<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					            <circle id="Oval" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" cx="12" cy="12" r="11.5"></circle>
					            <path d="M18,18 L13.057,13.059" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <circle id="Oval" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" cx="12" cy="12" r="1.5"></circle>
					            <path d="M11.8,5 L11.8,10.583" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						    </g>
						</svg>
						<? include("icon.steps-checkmark.php"); ?>
					</span>
					<span class="step-label">Order Details</span>
				</a>
				
				<span class="i-separator num-2"><? include("icon.steps-arrow.php"); ?></span>
				
				<a href="dynamic.php?page=checkout-contact-info" class="i-step num-3">
					<span class="step-graphic">
						<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="3.5" y="0.5" width="17" height="23"></rect>
					            <path d="M3.5,0.5 L1.5,0.5 C0.95,0.5 0.5,0.95 0.5,1.5 L0.5,22.5 C0.5,23.05 0.95,23.5 1.5,23.5 L3.5,23.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <path d="M13,12 L13,13.5 L15.538,14.226 C16.067,14.377 16.5,14.95 16.5,15.5 L16.5,18.5 L6.5,18.5 L6.5,15.5 C6.5,14.95 6.933,14.377 7.461,14.226 L10,13.5 L10,12" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
					            <ellipse id="Oval" stroke="#000000" stroke-linejoin="round" cx="11.5" cy="9.271" rx="3" ry="3.272"></ellipse>
					            <path d="M14.469,8.676 C13.969,9.176 12.539,9.174 12,8.171 C11,9.171 9.375,9.171 8.566,8.6" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
					            <path d="M20.5,2 L21.5,2 C22.6,2 23.5,2.9 23.5,4 L23.5,7 L20.5,7" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
					            <polyline id="Shape" stroke="#000000" stroke-linejoin="round" points="20.5 7 23.5 7 23.5 12 20.5 12"></polyline>
					            <polyline id="Shape" stroke="#000000" stroke-linejoin="round" points="20.5 12 23.5 12 23.5 17 20.5 17"></polyline>
					            <path d="M20.5,17 L23.5,17 L23.5,20 C23.5,21.1 22.6,22 21.5,22 L20.5,22" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
						    </g>
						</svg>
						<? include("icon.steps-checkmark.php"); ?>
					</span>
					<span class="step-label">Contact Info</span>
				</a>
				
				<span class="i-separator num-3"><? include("icon.steps-arrow.php"); ?></span>
				
				<a href="dynamic.php?page=checkout-payment-details&view=core" class="i-step num-4">
					<span class="step-graphic">
						<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					            <path d="M1.297,1.453 L14.59,4.772 C16.191,5.172 17.5,6.849 17.5,8.5 L17.5,20.5 C17.5,22.15 16.186,23.19 14.58,22.812 L3.42,20.188 C1.814,19.809 0.5,18.15 0.5,16.5 L0.5,3.5 C0.5,1.849 1.85,0.5 3.5,0.5 L20.5,0.5 C22.15,0.5 23.5,1.849 23.5,3.5 L23.5,14.5 C23.5,16.15 22.15,17.5 20.5,17.5 L17.5,17.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <path d="M9.5,3.5 L19.5,3.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <circle id="Oval" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" cx="13" cy="13.5" r="2"></circle>
					            <path d="M17.5,8.5 L19.5,8.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						    </g>
						</svg>
						<? include("icon.steps-checkmark.php"); ?>
					</span>
					<span class="step-label">Payment</span>
				</a>
				
				<span class="i-separator num-4"><? include("icon.steps-arrow.php"); ?></span>
				
				<a href="dynamic.php?page=checkout-payment-details&view=core" class="i-step num-5">
					<span class="step-graphic">
						<svg class="graphic-icon" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="7 5 4 8 2.5 6.5"></polyline>
					            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="7 10.5 4 13.5 2.5 12"></polyline>
					            <polygon id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="9.5 21.5 0.5 21.5 0.5 2.5 14.5 2.5 14.5 16.5"></polygon>
					            <polyline id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" points="9.5 21.5 9.5 16.5 14.5 16.5"></polyline>
					            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="17.499" y="8.5" width="4.001" height="10.5"></rect>
					            <path d="M21.498,1.501 C22.603,1.501 23.498,2.396 23.498,3.5 L23.5,11.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <path d="M19.5,21 L19.5,23.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="17.499" y="0.5" width="3.999" height="8"></rect>
					            <rect id="Rectangle-path" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" x="18.5" y="19" width="2" height="2"></rect>
					            <path d="M8.5,7.5 L12,7.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
					            <path d="M8.5,12.5 L12,12.5" id="Shape" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path>
						    </g>
						</svg>
						<? include("icon.steps-checkmark.php"); ?>
					</span>
					<span class="step-label">Review & Confirm<span>
				</a>



				<script>
					
					function makeStepCurrent(step) {
				        $( String('.i-step.num-' + step) ).addClass('mod-current');
				        $( String('.i-step.num-' + step) ).attr('href', 'javascript:void(0);');
					}

					function makeStepCompleted(step) {
				        $( String('.i-step.num-' + step) ).addClass('mod-completed');
				        $( String('.i-separator.num-' + step) ).addClass('mod-completed');
					}

					function makeStepUpcoming(step) {
				        $( String('.i-step.num-' + step) ).attr('href', 'javascript:void(0);');
					}
					
				</script>


				<? if ($fmn_checkout_step == 1) { ?>

					<script>
						$(document).ready(function() {
							makeStepCurrent('1');
							makeStepUpcoming('2');
							makeStepUpcoming('3');
							makeStepUpcoming('4');
							makeStepUpcoming('5');
						});
					</script>

		 		<? } else if ($fmn_checkout_step == 2) { ?>
		 		
		 			<script>
						$(document).ready(function() {
							makeStepCompleted('1');
							makeStepCurrent('2');
							makeStepUpcoming('3');
							makeStepUpcoming('4');
							makeStepUpcoming('5');
						});
					</script>

		 		<? } else if ($fmn_checkout_step == 3) { ?>

		 			<script>
						$(document).ready(function() {
							makeStepCompleted('1');
							makeStepCompleted('2');
							makeStepCurrent('3');
							makeStepUpcoming('4');
							makeStepUpcoming('5');
						});
					</script>

		 		<? } else if ($fmn_checkout_step == 4) { ?>

		 			<script>
						$(document).ready(function() {
							makeStepCompleted('1');
							makeStepCompleted('2');
							makeStepCompleted('3');
							makeStepCurrent('4');
							makeStepUpcoming('5');
						});
					</script>

		 		<? } else if ($fmn_checkout_step == 5) { ?>

		 			<script>
						$(document).ready(function() {
							makeStepCompleted('1');
							makeStepCompleted('2');
							makeStepCompleted('3');
							makeStepCompleted('4');
							makeStepCurrent('5');
						});
					</script>

		 		<? } else if ($fmn_checkout_step == 6) { ?>

		 			<script>
						$(document).ready(function() {
							makeStepCompleted('1');
							makeStepCompleted('2');
							makeStepCompleted('3');
							makeStepCompleted('4');
							makeStepCompleted('5');
						});
					</script>

					<div class="steps-thankYou">
						<h1>Thank you!</h1>
					</div>
		 		
		 		<? } ?>


				<div class="steps-mobileTitle">
					
					<div class="steps-back">
						<a href="<?=$fmn_back_link_url?>"><span class="e-icon fmn-icon-arrow-left"></span></a>
					</div>
	
					<? if ($fmn_checkout_step == 1) { ?>
						<h1><span class="step-number">1 of 5.</span> Address</h1>
			 		<? } else if ($fmn_checkout_step == 2) { ?>
						<h1><span class="step-number">2 of 5.</span> Order Details</h1>
			 		<? } else if ($fmn_checkout_step == 3) { ?>
						<h1><span class="step-number">3 of 5.</span> Contact Info</h1>
			 		<? } else if ($fmn_checkout_step == 4) { ?>
						<h1><span class="step-number">4 of 5.</span> Payment Details</h1>
			 		<? } else if ($fmn_checkout_step == 5) { ?>
						<h1><span class="step-number">5 of 5.</span> Review & Confirm</h1>
			 		<? } else if ($fmn_checkout_step == 6) { ?>
						<h1>Thank you!</h1>
			 		<? } ?>
	
				</div>

			</div>
			

		<? } else { ?>

			<div id="fmnid-app-location-not-scrolled"></div>
			
			<? if ($fmn_back_link == "no") { ?>
			
				<div class="fmn-app-location m-no-back" id="fmnid-app-location">
					<h1 class="fmn-title mainTitle"><?=$fmn_screen_title?></h1>
				</div>

			<? } else if ($fmn_back_link == "menu") { ?>
			
			<div class="fmn-app-location" id="fmnid-app-location">
				<div class="fmn-backToMenu">
					<a href="<?=$fmn_back_link_url?>"><span class="backToMenu-back">Back To</span> Menu</a>
				</div>
				<h1 class="fmn-title mainTitle"><?=$fmn_screen_title?></h1>
			</div>

			
			
			<? } else { ?>
			
				<div class="fmn-app-location" id="fmnid-app-location">
					<div class="fmn-back">
						<a href="<?=$fmn_back_link_url?>"><span class="e-icon fmn-icon-arrow-left"></span></a>
					</div>
					<h1 class="fmn-title mainTitle"><?=$fmn_screen_title?></h1>
				</div>
				
			<? } ?>

		<? } ?>