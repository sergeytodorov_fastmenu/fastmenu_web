<!DOCTYPE html>

<html>

	<head>

		<title>FastMenu</title>

		<meta charset="utf-8">
		
		<!-- viewport -->
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
		
		<!-- load jQuery -->
		<script src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js'></script>
				
	</head>

	<body style="background-color: silver;">
		
		
		<br><br><br><br><br>

		<iframe id="myFrame" src="http://lab.uiux.eu/fastmenu/webdev/dynamic.php?page=overlay" style="border: 0; width: 1200px; height: 2500px; margin: 0 auto; display: block;"></iframe>
		
		<script>
			
			var myIframeBehavior = 1;
			var pageLoaded = 0;

			$(window).on('load', function ()  {
				pageLoaded = 1;
 				var myParentFramePosition = $("#myFrame").position();
 				var myParentFrameTop = myParentFramePosition.top;				
				var myParentScroll = $(this).scrollTop();
				var mySetTop2 = myParentScroll - myParentFrameTop;
				$('#myFrame')[0].contentWindow.mySetTop2(mySetTop2);
			});
			

			$(window).scroll(function() {
				if ( pageLoaded == 0 ) {
				} else if ( pageLoaded == 1 ) {
	 				var myParentFramePosition = $("#myFrame").position();
	 				var myParentFrameTop = myParentFramePosition.top;				
					var myParentScroll = $(this).scrollTop();
					var mySetTop2 = myParentScroll - myParentFrameTop;
					$('#myFrame')[0].contentWindow.mySetTop2(mySetTop2);					
				} 
			});

			
			function myCall() {
 				var myParentFramePosition = $("#myFrame").position();
 				var myParentFrameTop = myParentFramePosition.top;				
				var myParentScroll = $(this).scrollTop();
				var mySetTop2 = myParentScroll - myParentFrameTop;
				$('#myFrame')[0].contentWindow.mySetTop2(mySetTop2);
				
			}

		
		</script>

		<br><br><br><br><br>
				
	</body>

</html>
