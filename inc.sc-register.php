<div class="fmn-screen-content-wrap m-reducedWidth-oneThird">

	<div class="fmn-form">
				
		<div class="fmn-form-option">
			<label>User Name</label>
			<div class="fmn-form-field">
				<input type="text">
			</div>
		</div>
		
		<div class="fmn-form-option">
			<label>Email Address</label>
			<div class="fmn-form-field">
				<input type="text">
			</div>
		</div>
		
		<div class="fmn-form-option">
			<label>Password</label>
			<div class="fmn-form-field">
				<input type="password">
			</div>
		</div>

		<div class="fmn-form-option">
			<label>Confirm Password</label>
			<div class="fmn-form-field">
				<input type="password">
			</div>
		</div>

		<div class="fmn-form-option-columns">
			<div class="fmn-form-option-column fmn-oneHalf">
				<div class="fmn-form-option">
					<label>First Name</label>
					<div class="fmn-form-field">
						<input type="text">	
					</div>
				</div>
			</div>
			<div class="fmn-form-option-column fmn-oneHalf">
				<div class="fmn-form-option">
					<label>Last Name</label>
					<div class="fmn-form-field">
						<input type="text">	
					</div>
				</div>
			</div>
		</div>

		<div class="fmn-form-option">
			<label>Phone Number</label>
			<div class="fmn-form-field">
				<input type="text">
			</div>
		</div>
		
		<div class="fmn-form-option">
			<div class="i-basic-value">
				<label><input type="checkbox"> I agree with the <a href="#">terms and conditions</a></label>
			</div>
		</div>
		
		<div class="fmn-form-option">
			<div class="i-basic-value">
				<label><input type="checkbox"> Subscribe me for promos and special offers</label>
			</div>
		</div>

		<div class="fmn-form-actions">
			<button class="m-large">Register</button>
		</div>		

	</div><!-- fmn-form -->
	
</div>