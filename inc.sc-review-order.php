<?
	$views = "available";
	
	$allowedViews = array(
    	"delivery" => array( 
	    	"visibleView" => "delivery"
    	),
    	"takeAway" => array( 
	    	"visibleView" => "takeAway"
    	),
    	"delayedDelivery" => array( 
	    	"visibleView" => "delayedDelivery"
    	),
    	"all" => array( 
	    	"visibleView" => "all"
    	)
	);

	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];
	
?>

<script>

	$(document).ready(function() {
		visibleView = "<?=$fmn_visibleView?>";
		defaultView = "delivery";
		console.log(visibleView);
		console.log(defaultView);
		if (visibleView == "") {
			console.log("here!");	
			$(".debug-view").addClass("debug-view-hidden");
			$(".debug-view-"+defaultView).removeClass("debug-view-hidden");	
			$(".debug-view-link").removeClass("selected");
			$(".debug-view-link-"+defaultView).addClass("selected");
		} else {
			$(".debug-view").addClass("debug-view-hidden");
			$(".debug-view-"+visibleView).removeClass("debug-view-hidden");	
			$(".debug-view-link").removeClass("selected");
			$(".debug-view-link-"+visibleView).addClass("selected");

		}
	});
	
	function setView(myView) {
		$(".debug-view").addClass("debug-view-hidden");
		$(".debug-view-"+myView).removeClass("debug-view-hidden");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+myView).addClass("selected");			
	}

</script>

<style>
	.debug-view-hidden {
		display: none !important;
	}
</style>

<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('delivery');" class="debug-view-link debug-view-link-delivery">Delivery <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('delayedDelivery');" class="debug-view-link debug-view-link-delayedDelivery">Delayed Delivery <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('takeAway');" class="debug-view-link debug-view-link-takeAway">Take Away <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('all');" class="debug-view-link debug-view-link-all">All <span class="debug-visible">Current</span></a>
</div>













	<div class="fmn-screen-content-wrap m-fullWidth m-checkout m-reviewOrder">
		
		<div class="fmn-screen-content-wrap-columns">

			<div class="fmn-screen-content-wrap-column m-oneHalf">
				
				<div class="fmn-summary">

					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delivery debug-view-all">
						<h3 class="fmn-section-title"><span>Delivery Address</span><a href="dynamic.php?page=checkout-complete-address&view=guest" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							Sofia, Bulgaria<br>
							Liulin, bl. 700<br>
							entr. A, fl. 3, apt. 30<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Name:</span> John Smith<br>
							<span class="data-label">Phone:</span> 0888 876543<br>
							<span class="data-label">Email:</span> john@johnsmith.com<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Address Notes:</span> Ring the third bell<br>
						</p>
					</div><!-- fmn-summary-section -->

					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delayedDelivery debug-view-all">
						<h3 class="fmn-section-title"><span>Delivery Address</span><a href="dynamic.php?page=checkout-complete-address&view=registered" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							Sofia, Bulgaria<br>
							11, Ivan Vazov Str.<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Name:</span> John Smith<br>
							<span class="data-label">Phone:</span> 0888 876543<br>
							<span class="data-label">Email:</span> john@johnsmith.com<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Address Notes:</span> Ring the third bell<br>
						</p>
					</div><!-- fmn-summary-section -->
					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-takeAway debug-view-all">
						<h3 class="fmn-section-title"><span>Take Away</span><a href="dynamic.php?page=otlTakeAway" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="data-label">From:</span> Flying Lotus<br>
							<span class="data-note">
								7, Main Street, Taito, Tokyo, Japan<br>
								accepts orders between 09:30 — 20:00<br>
							</span>
						</p>
					</div><!-- fmn-summary-section -->
					
					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delivery debug-view-takeAway debug-view-all">
						<h3 class="fmn-section-title"><span>Order Details</span><a href="dynamic.php?page=checkout-order-details&view=core" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="data-label">Date:</span> Monday, September 19<br>						
							<span class="data-label">Estimated hour:</span> 10:30<br>
							<span class="data-label">Order notes:</span> Please deliver this in a gold plated freezer box.
						</p>
					</div><!-- fmn-summary-section -->
					
					
<!--
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-closed debug-view-all">
						<h3 class="fmn-section-title"><span>Order Details</span><a href="#" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="e-highlight">
								Unfortunately all our locations are closed for the day. Please note the next available day below.<br>
								Date: Monday, September 19<br>
							</span>
							Estimated hour: 10:30<br>
							Order notes: Please deliver this in a gold plated freezer box.
						</p>
					</div><!-- fmn-summary-section -->
-->
					
					<div class="fmn-section fmn-summary-section orderTypeAndDetails debug-view debug-view-delayedDelivery debug-view-all">
						<h3 class="fmn-section-title"><span>Order Details</span><a href="dynamic.php?page=checkout-order-details&view=core" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="data-label">Date:</span> Monday, September 19<br>
						
						<p class="fmn-section-data mod-highlight">
							Please note the earliest possible hour for fulfilling your order.<br>
							<span class="data-label">Estimated hour:</span> 10:30<br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Order notes:</span> Please deliver this in a gold plated freezer box.
						</p>
					</div><!-- fmn-summary-section -->
					



					<div class="fmn-section fmn-summary-section contactInfo debug-view debug-view-takeAway debug-view-all">
						<h3 class="fmn-section-title"><span>Contact Info</span><a href="dynamic.php?page=checkout-contact-info" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="data-label">Name:</span> John Smith<br>
							<span class="data-label">Phone:</span> 0888 876543<br>
							<span class="data-label">Email:</span> john@johnsmith.com<br>
						</p>
					</div><!-- fmn-summary-section -->





					
					<div class="fmn-section fmn-summary-section paymentDetails">
						<h3 class="fmn-section-title"><span>Payment Details</span><a href="dynamic.php?page=checkout-payment-details" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						<p class="fmn-section-data">
							<span class="data-label">Payment Method:</span> Credit Card<br>
							<span class="data-note">(You’ll be prompted for your card details after order is submitted.)</span><br>
						</p>
						<p class="fmn-section-data">
							<span class="data-label">Invoice Details:</span> John Smith Co., 1010201301, John Smith, Sofia 1000, 11, Ivan Vazov Str.
						</p>
					</div><!-- fmn-summary-section -->
					
										
					
				</div><!--fmn-summary -->

			</div><!-- fmn-screen-content-wrap-column -->



			<div class="fmn-screen-content-wrap-column m-oneHalf">

				<div class="fmn-summary">
					
					<div class="fmn-section fmn-summary-section">
						
						<h3 class="fmn-section-title mod-huge"><span>Basket</span><a href="dynamic.php?page=basket" class="reviewOrder-changeLink fmn-button m-smaller m-invert">Edit</a></h3>
						
						

						<div class="fmn-basket m-readOnly">
			
							<div class="fmn-list">
								
								<div class="i-list-line m-header">
									<span class="i-list-line-cell m-label"></span>
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln1"><input id="cx-ln1" type="checkbox"></label><label for="cx-ln1" class="e-mobileLabel">Select All</label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo">photo</span>
										<span class="i-list-line-cell m-name">name and details</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price">price</span>
										<span class="i-list-line-cell m-qty">qty</span>
										<span class="i-list-line-cell m-lineTotal">total</span>
									</span>
									<span class="i-list-line-cell m-actions"></span>
								</div>
								
								<div class="i-list-line m-row">
<!--
									<span class="i-list-line-cell m-label">
										<span class="i-list-line-cell-label m-justAdded">Just Added!</span>
									</span>
-->
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln2"><input id="cx-ln2" type="checkbox"></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo"><img src="images/sample-dish-green.jpg"></span>
										<span class="i-list-line-cell m-name">
											<span>Green Salad with a Convoluted Long Name</span>
											<span class="e-detail">(no onions, but otherwise super long details)</span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€18.45</span> <span class="e-special">€11.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											2
										</span>
										<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span>€23.90</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
								<div class="i-list-line m-row m-noLongerAvailable debug-view debug-view-all">
									<span class="fmn-labels">
									 	<span class="i-label">
										 	<span class="fmn-noLongerAvailable">
											 	<span class="e-label">This item is no longer available.</span>
										 	</span>
									 	</span>
								 	</span>
<!--
									<span class="i-list-line-cell m-label">
										<span class="i-list-line-cell-label m-noLongerAvailable">This item is no longer available.</span>
									</span>
-->
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln3"><input id="cx-ln3" type="checkbox"></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-name">
											<span>Pizza Margheritta</span>
											<span class="e-detail">(small + garlic sauce)</span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€18.45</span> <span class="e-special">€11.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											2
										</span>
										<span class="i-list-line-cell m-lineTotal">—</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
								<div class="i-list-line m-row m-adjustedQuantity debug-view debug-view-all">
									<span class="fmn-labels">
									 	<span class="i-label">
										 	<span class="fmn-adjustedQuantity">
											 	<span class="e-label">Item Quantity Adjusted</span>
										 	</span>
									 	</span>
								 	</span>
<!--
									<span class="i-list-line-cell m-label">
										<span class="i-list-line-cell-label m-adjustedQuantity">Item Quantity Adjusted</span>
									</span>
-->
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln4"><input id="cx-ln4" type="checkbox" checked=""></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo"><img src="images/sample-category-tile-veal.jpg"></span>
										<span class="i-list-line-cell m-name">
											<span>Veal Pepper Steak</span>
											<span class="e-detail">(medium rare)</span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											1
										</span>
										<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span>€23.90</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
								<div class="i-list-line m-row">
									<span class="i-list-line-cell m-checkbox"><label for="cx-ln5"><input id="cx-ln5" type="checkbox"></label></span>
									<span class="i-list-line-wrap m-photoName">
										<span class="i-list-line-cell m-photo"><img src="images/sample-category-tile-fish.jpg"></span>
										<span class="i-list-line-cell m-name">
											<span>Cod Fillet</span>
											<span class="e-detail"></span>
										</span>
									</span>
									<span class="i-list-line-wrap m-priceQtyTotal">
										<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
										<span class="i-list-line-cell m-qty">
											2
										</span>
										<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span>€23.90</span>
									</span>
									<span class="i-list-line-cell m-actions">
										<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
									</span>
								</div>
								
							</div><!-- fmn-list -->
							
	
	
	
							<div class="fmn-basket-footer m-readOnly">
								
								<div class="fmn-basket-footer-secondaryActions">
									<a href="#" class="tap fmn-button m-small m-invert m-destructive">Delete Selected</a>
								</div>
								
								<div class="fmn-basket-footer-data">
									
									<div class="i-basket-footer-wrap m-basket">
										<div class="i-basket-footer-line m-basket">
											<span class="i-basket-footer-line-cell m-label">Basket</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€40.00</span></span></span>
										</div>
										<div class="i-basket-footer-line m-discount debug-view debug-view-all">
											<span class="i-basket-footer-line-cell m-label">
												XX% discount for XXXXXX orders above €XX.XX*<br>
												<span class="e-note">
													<span class="e-special">You are just €XX.XX away from a XX% discount.</span> <a href="#">See details</a>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€4.00</span></span></span>
										</div>
<!--
										<div class="i-basket-footer-line subtotal">
											<span class="i-basket-footer-line-cell m-label">Sub-total</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€36.00</span></span></span>
										</div>
-->
									</div>
									<div class="i-basket-footer-wrap m-delivery debug-view debug-view-all">
										<div class="i-basket-footer-line m-delivery">
											<span class="i-basket-footer-line-cell m-label">
												Delivery to XXXXXX for orders above €XX.XX<br>
												<span class="e-note">
													<span class="e-special">Your sub-total is just €XX.XX away from a special €XX.XX delivery fee.</span><br>
													<a href="#">Specify Another Location</a>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">€2.00</span></span></span>
										</div>
									</div>
									<div class="i-basket-footer-wrap m-delivery">
										<div class="i-basket-footer-line m-delivery">
											<span class="i-basket-footer-line-cell m-label">
												Доставка до Център за поръчки над 20 лв<br>
												<span class="e-note">
													<span><a href="dynamic.php?page=checkout-payment-details">Изберете плащане с карта или онлайн</a> за по-ниска цена на достака</span><br>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">€2.00</span></span></span>
										</div>
									</div>

									<div class="i-basket-footer-wrap m-promoCode debug-view debug-view-all">
										<div class="i-basket-footer-line m-promoCode">
											<span class="i-basket-footer-line-cell m-label">Promo Code</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€4.00</span></span></span>
										</div>
									</div>

									<div class="i-basket-footer-wrap m-total">
										<div class="i-basket-footer-line m-total">
											<span class="i-basket-footer-line-cell m-label">
												Total
												<span class="e-note">
													<span class="e-special">You save €38.00</span>
												</span>
											</span>
											<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€38.00</span></span></span>
										</div>
									</div>
									
									<div class="i-basket-footer-wrap m-note">
										<div class="i-basket-footer-line m-note">
											<span class="i-basket-footer-line-span">
												* Items which are already discounted are not further discounted by order total promos
											</span>
										</div>
										
									</div>
								
								</div>
								
							</div><!-- fmn-basket-footer -->
																		
						</div><!-- fmn-basket -->

					</div><!-- fmn-summary-section -->

				</div><!-- fmn-summary -->

			</div><!-- fmn-screen-content-wrap-column --> 
			







		</div><!-- fmn-screen-content-wrap-columns -->
		
	</div><!-- fmn-screen-content-wrap -->




	<div class="fmn-actions m-right">

		<div class="fmn-agree">
			<div class="fmn-form-option">
				<span class="fm-checkbox">
					<label for="check1">
						<input type="checkbox" name="1" id="check1" checked=""/>
						<span class="fm-checkbox-icon mod-empty"></span>
						<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
						<span class="fm-checkbox-label">I agree with the online order <a href="#" class="n-termsLink">Terms & Conditions</a></span>
					</label>
				</span>
<!--
				<div class="i-basic-value">
					<label><input type="checkbox" checked=""> I agree with the online order <a href="#" class="n-termsLink">Terms & Conditions</a></label>
				</div>
-->
			</div>
		</div>		

<!-- 		<button class="m-large" value="Checkout">Submit Order</button> -->
		<div class="n-actions-alert-wrap debug-view debug-view-all">
			<div class="n-actions-alert">Your basket is less than the €40.00 minimum for delivery orders</div>
		</div>

		<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large debug-view debug-view-all">Confirm & Proceed to Payment</a>

		<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large m-disabled debug-view debug-view-all">Submit Order</a>

		<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Submit Order</a>
		
	</div><!-- fmn-actions -->
