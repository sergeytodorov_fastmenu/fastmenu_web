
	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds m-deliveryAddress">
		
		<div class="fmn-form">
			
			<div class="fmn-blocks m-twoCols">
				
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>11, Ivan Vazov Str. (main)</h4>
						<p>
							<span class="c-nobr">Center, Sofia 1000,</span>
							<span class="c-nobr">John Smith,</span>
							<span class="c-nobr">0888 876 543,</span>
							<span class="c-nobr">john@johnsmith.com</span>
						</p>
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Select</a><a href="dynamic.php?page=checkout-specify-address">Customize</a>
					</div>
				</div>
				
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>11, Ivan Vazov Str.</h4>
						<p>
							<span class="c-nobr">Center, Sofia 1000,</span>
							<span class="c-nobr">John Smith,</span>
							<span class="c-nobr">0888 876 543,</span>
							<span class="c-nobr">john@johnsmith.com</span>
						</p>
<!-- 						<button type="submit" href="#">Select</button><a href="<?=$fmn_next_page_url?>">Customize</a> -->
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Select</a><a href="dynamic.php?page=checkout-specify-address">Customize</a>
					</div>
				</div>
				
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>11, Ivan Vazov Str.</h4>
						<p>
							<span class="c-nobr">Center, Sofia 1000,</span>
							<span class="c-nobr">John Smith,</span>
							<span class="c-nobr">0888 876 543,</span>
							<span class="c-nobr">john@johnsmith.com</span>
						</p>
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Select</a><a href="dynamic.php?page=checkout-specify-address">Customize</a>
					</div>
				</div>

				<div class="i-block-wrap m-onlyButton">
					<div class="i-block">
						<a class="fmn-button" href="dynamic.php?page=checkout-specify-address">Specify Another Address</a>
					</div>
				</div>

				
			</div><!-- fmn-blocks -->

		</div><!-- fmn-form -->
		
	</div><!-- fmn-screen-content-wrap -->
