	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirdsWithSide">
		
		<div class="fmn-form">
			
			<div class="fmn-form-option vertical">
				<label>Default Payment Method</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<button class="fmn-button m-invert m-selected" href="#"><span>Pay on Location</span></button>
						<button class="fmn-button m-invert" href="#"><span>ePay</span></button>
						<button class="fmn-button m-invert" href="#"><span>Credit Card</span></button>
					</div>
				</div>
			</div>
			
			<div class="fmn-form-option vertical">
				<label>Default Invoice type</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<button class="fmn-button m-invert m-selected" href="#"><span>None</span></button>
						<button class="fmn-button m-invert" href="#"><span>Individual</span></button>
						<button class="fmn-button m-invert" href="#"><span>Company</span></button>
					</div>
				</div>
			</div>
			
		</div><!-- fmn-form -->
		
	</div><!-- fmn-screen-content-wrap -->
	
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Save</button> -->
		<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Save Defaults</a>
		
	</div><!-- fmn-actions -->