 

<!-- 	<div class="fmn-screen-content-wrap m-fullWidth m-specifyAddress"> -->
	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds m-specifyAddress">

		<div class="fmn-content-section">
			<p>Please complete the address you provided earlier.</p>
		</div>



		<div class="fmn-locationAddress mod-account">

			<div class="locationAddress-addressForm">
				


				<div class="fmn-form">

					<? /* uncomment 						
					<div class="fmn-form-error">
						This form contains errors. Please revise.
					</div>
					*/ ?>
					
					<div class="fmn-form-required">
						<span class="required-text">Required fields</span><span class="required-asterisk">*</span>
					</div>

<!--
					<div class="addressForm-columns">
						<div class="columns-col m-twoThirds">
							<div class="fmn-form-option mod-required">
								<label>Street / Residential Complex</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
						<div class="columns-col m-oneThird">
							<div class="fmn-form-option mod-required">
								<label>Number</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
					</div>
-->

<!--
					<div class="fmn-form-option">
						<label>Country</label>
						<div class="fmn-form-field mod-select"><select><option>Bulgaria</option></select></div>
					</div>
-->


					<div class="addressForm-columns m-twoCols">
						<div class="columns-col m-twoThirds">
							<div class="fmn-form-option">
								<label>City</label>
								<div class="fmn-form-field mod-select"><select disabled=""><option>Plovdiv</option></select></div>
							</div>
						</div>
						<div class="columns-col m-oneThird">
							<div class="fmn-form-option">
								<label>Postal Code</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
					</div>
								
<!--
					<div class="fmn-form-option m-vertical">
						<label>Specify address as</label>
						<div class="fmn-form-field">
							<div class="fmn-form-buttonArray">
								<button class="fmn-button m-invert m-selected" href="#"><span>Street and Number</span></button>
								<button class="fmn-button m-invert" href="#"><span>Residential Complex and Block</span></button>
							</div>
						</div>
					</div>


					<div class="fmn-form-option">
						<label>Specify address as:</label>
						<div class="i-basic-value">
							<label style="margin-bottom: 0;" onclick="fmnSetAddressType('street');"><input name="addressType" type="radio" checked=""> Street and Number</label>
						</div>
						<div class="i-basic-value">
							<label onclick="fmnSetAddressType('complex');"><input name="addressType" type="radio"> Residential Complex and Block</label>
						</div>
					</div>
-->

					
					<div id="fmn-addressForm-fields-street" style="display: none;">
				
						<div class="addressForm-columns m-twoCols">
							<div class="columns-col m-twoThirds">
								<div class="fmn-form-option mod-required">
									<label>Street</label>
									<div class="fmn-form-field"><input type="text" ></div>
								</div>
							</div>
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option mod-required">
									<label>Number</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
						</div>

						<div class="addressForm-columns m-threeCols">
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option">
									<label>Еntrance</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option">
									<label>Floor</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option">
									<label>Apartment</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
						</div>
						
					</div>
					
					<div id="fmn-addressForm-fields-complex">
				

						<div class="addressForm-columns m-twoCols">
							<div class="columns-col m-twoThirds">
								<div class="fmn-form-option">
									<label>Residential Complex</label>
									<div class="fmn-form-field"><input type="text" value="Trakia" disabled=""></div>
								</div>
							</div>
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option">
									<label>Block</label>
									<div class="fmn-form-field"><input type="text" value="17" disabled=""></div>
								</div>
							</div>
						</div>

						<div class="addressForm-columns m-threeCols">
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option mod-required">
									<label>Еntrance</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option mod-required">
									<label>Floor</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
							<div class="columns-col m-oneThird">
								<div class="fmn-form-option mod-required">
									<label>Apartment</label>
									<div class="fmn-form-field"><input type="text"></div>
								</div>
							</div>
						</div>
						
					</div>


<!--
					<div class="addressForm-columns">
						<div class="columns-col m-oneQuarter">
							<div class="fmn-form-option">
								<label>Block</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
						<div class="columns-col m-oneQuarter">
							<div class="fmn-form-option">
								<label>Еntrance</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
						<div class="columns-col m-oneQuarter">
							<div class="fmn-form-option">
								<label>Floor</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
						<div class="columns-col m-oneQuarter">
							<div class="fmn-form-option">
								<label>Apartment</label>
								<div class="fmn-form-field"><input type="text"></div>
							</div>
						</div>
					</div>

					
					<div class="fmn-form-option">
						<label>Your Full Name</label>
						<div class="fmn-form-field"><input type="text"></div>
					</div>

					<div class="fmn-form-option m-error mod-required">
						<label>
							Email Address
							<span class="fmn-form-option-error">Please enter a valid email address.</span>
						</label>
						<div class="fmn-form-field"><input type="text" class="error" value="ben@microsoft"></div>
					</div>

					<div class="fmn-form-option mod-required">
						<label>Phone Number</label>
						<div class="fmn-form-field"><input type="text"></div>
					</div>

-->
					
					<div class="fmn-form-option">
						<label>Notes for This Address</label>
						<div class="fmn-form-field"><input type="text" placeholder="e.g. if we need to call for access"></div>
					</div>

					<div class="fmn-form-option" style="margin-bottom: 0;">
						<div class="i-basic-value">
							<label><input type="checkbox" checked=""> Save this address for future orders</label>
						</div>
					</div>
					
<!--
					<div class="fmn-form-option">
						<label>Address Label</label>
						<div class="fmn-form-field"><input type="text" placeholder="e.g. My Office"></div>
					</div>
-->

					<div class="fmn-form-option" style="padding-top: 0;">
						<div class="i-basic-value">
							<label><input type="checkbox"> Make it the main address for my profile</label>
						</div>
					</div>

				</div>
			
			</div>
			
			
			<div class="locationAddress-findAddress">

<!--
				<div class="fmn-form-option">
					<label>Find an Address</label>
					<div class="fmn-form-option-field">
						<input type="text" placeholder="e.g. ulitsa Ivan Vazov 7, Plovdiv">
						<a href="#" class="field-icon"><span class="e-icon fmn-icon-search"></span></a>
					</div>
				</div>
-->

<!--
				<div class="findAddress-instruction">Please enter your street address above.</div>

				<div class="findAddress-instruction">Thanks! Now complete and confirm your full address details.</div>
				
				<div class="findAddress-tip">Tip: You can also drag the red indicator on the map to visually pin-point your street address.</div>
-->
				<div class="findAddress-instruction">Please review your address location and apply corrections if needed</div>


				<div class="findAddress-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d11831.96115109383!2d24.73210195!3d42.15049465!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbg!4v1518102743587" width="100%" height="240px" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			
			</div>

			
			
		</div>






		<? /* previous version
		
		<div class="fmn-form">
			
			<div class="fmn-form-error">
				This form contains errors. Please revise.
			</div>
										
			<div class="fmn-screen-content-wrap-columns">
				<div class="fmn-screen-content-wrap-column m-oneThird">
					
					
					<div class="fmn-form-option">
						<div class="fmn-orderTypeLocation">
							
							<span class="n-location">
								<span class="n-icon fmn-icon-store"></span>
								<span class="n-caption">
									<b>Delivery</b> to <b>Sofia</b> 
								</span>
								<a class="n-link" href="">Change</a>
							</span>
							
						</div>
					</div>

					<div class="fmn-form-option">
						<label>Country</label>
						<div class="fmn-form-field">
							<input type="text" value="Bulgaria" disabled="">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>City</label>
						<div class="fmn-form-field">
							<input type="text" value="Sofia" disabled="">
						</div>
					</div>

					
					<div class="fmn-form-option">
						<label>Neighborhood</label>
						<div class="fmn-form-field mod-select">
							<select style="">
								<option>Mitte</option>
								<option selected>Prenzlauer Berg</option>
							</select>
						</div>
					</div>
					
					<div class="fmn-form-option-columns">
						<div class="fmn-form-option-column fmn-twoThirds">
							<div class="fmn-form-option">
								<label>
									Address
									<span class="fmn-form-option-details">str, №, ent, fl, apt</span>
								</label>
								<div class="fmn-form-field">
									<textarea></textarea>
								</div>
							</div>
						</div>
						<div class="fmn-form-option-column fmn-oneThird">
							<div class="fmn-form-option">
								<label>Postal Code</label>
								<div class="fmn-form-field">
									<input type="text">	
								</div>
							</div>
						</div>
					</div>
					
					
					
				</div>
				
				<div class="fmn-screen-content-wrap-column m-oneThird">
					<div class="fmn-form-option">
						<label>
							Contact Name
							<span class="fmn-form-option-details">We'll need to know who we're delivering to</span>
						</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					<div class="fmn-form-option">
						<label>Phone Number</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
						<span class="fmn-form-option-help">please follow format XXX XXX XXXX</span>
					</div>
					<div class="fmn-form-option">
						<label>Email Address</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
				</div>
				
				<div class="fmn-screen-content-wrap-column m-oneThird">
					<div class="fmn-form-option">
						<label>
							Notes for This Address
						</label>
						<div class="fmn-form-field">
							<textarea></textarea>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<div class="i-basic-value">
							<label><input type="checkbox"> Save this address for future orders</label>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>
							Address Label
							<span class="fmn-form-option-details">e.g. My Office</span>
						</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>

					<div class="fmn-form-option">
						<div class="i-basic-value">
							<label><input type="checkbox"> Make it the main address for my profile</label>
						</div>
					</div>

				</div>
				
			</div>
			
		</div><!-- fmn-form -->

		*/ ?>
	
	</div><!-- fmn-screen-content-wrap -->
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Next</button> -->
		<a  href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Next: Order Details</a>
	</div><!-- fmn-actions -->
