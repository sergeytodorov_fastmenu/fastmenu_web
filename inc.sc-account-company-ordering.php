	<div class="fmn-screen-content-wrap m-reducedWidth-oneThirdWithSide">

		<div class="debug-section">
			if associated<br>
			<span class="debug-section-note">note: if Disconnect is selected, show confirmation dialog</span>
		</div>
		
		<div class="fmn-content-section">
			<p>Your account is currently associated with <b>Company X</b>.</p>
			<p>If you would like to associate with a different company, you first need to disconnect from your current one.</p>
			<button class="">Disconnect from Company X</button>
		</div>
		
		<div class="debug-section">
			if not associated<br>
			<span class="debug-section-note">note: when Enter Company Code is selected, use the already available screen</span>
		</div>

		<div class="fmn-content-section">
			<p>Your account is not associated with any company at this moment. You can enter your company code to connect.</p>
			<button class="">Enter Company Code</button>
		</div>


	</div><!-- screenWrap -->
	
	<div class="fmn-actions m-right">
		
		
		
	</div><!-- fmn-actions -->
