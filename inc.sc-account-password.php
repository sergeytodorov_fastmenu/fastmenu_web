
	<div class="fmn-screen-content-wrap m-reducedWidth-oneThirdWithSide">
		
		<div class="fmn-form">
					
			<div class="fmn-form-option">
				<label>Current Password</label>
				<div class="fmn-form-field">
					<input type="password">
				</div>
			</div>
			<div class="fmn-form-option">
				<label>New Password</label>
				<div class="fmn-form-field">
					<input type="password">
				</div>
			</div>
			<div class="fmn-form-option">
				<label>Confirm New Password</label>
				<div class="fmn-form-field">
					<input type="password">
				</div>
			</div>								

		</div><!-- fmn-form -->
		
		
	</div><!-- screenWrap -->
	
	<div class="fmn-actions m-right">
		
		<button class="m-large" value="Checkout">Save</button>
		
	</div><!-- fmn-actions -->