<?
	$views = "available";
	
	$allowedViews = array(
    	"guest" => array( 
	    	"visibleView" => "guest"
    	),
    	"registered" => array( 
	    	"visibleView" => "registered"
    	)

	);

	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];
	
?>

<script>

	$(document).ready(function() {
		visibleView = "<?=$fmn_visibleView?>";
		$(".debug-view").css("display", "none");
		$(".debug-view-"+visibleView).css("display", "block");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+visibleView).addClass("selected");

	});
	
	function setView(myView) {
		$(".debug-view").css("display", "none");
		$(".debug-view-"+myView).css("display", "block");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+myView).addClass("selected");			
	}

</script>




<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('guest');" class="debug-view-link debug-view-link-0 debug-view-link-guest">Guest <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('registered');" class="debug-view-link debug-view-link-1 debug-view-link-registered">Registered <span class="debug-visible">Current</span></a>
</div>






<!-- 	<div class="fmn-screen-content-wrap m-fullWidth m-specifyAddress"> -->
	<div class="fmn-screen-content-wrap m-reducedWidth-oneThird m-checkout m-completeAddress">

		<div class="fmn-content-section m-center">
			<p>Please complete the delivery address you provided earlier.</p>
		</div>

		<div class="fmn-locationAddress mod-checkout">

			<div class="locationAddress-addressForm">
				


				<div class="fmn-form">

					<? /* uncomment 						
					<div class="fmn-form-error">
						This form contains errors. Please revise.
					</div>
					*/ ?>
										
					<div class="debug-view debug-view-guest">
						
						<div class="fmn-form-required">
							<span class="required-text">Required fields</span><span class="required-asterisk">*</span>
						</div>
	
						<div class="addressForm-readOnly">
							<div class="readOnly-data">
								<a href="dynamic.php?page=otlDeliveryNotLoggedIn" class="data-edit fmn-button m-smaller">Change</a>
								Sofia, Bulgaria, Residential Complex Liulin, Block 700, Entrance A, 3rd Floor, Apartment 30
							</div>
						</div>
						
						<div class="fmn-form-option mod-required">
							<label>Your Full Name</label>
							<div class="fmn-form-field"><input type="text"></div>
						</div>
						
						<div class="fmn-form-option mod-required">
							<label>
								Email Address
	<!-- 							<span class="fmn-form-option-error">Please enter a valid email address.</span> -->
							</label>
							<div class="fmn-form-field"><input type="text" class="error" value="ben@microsoft"></div>
						</div>
	
						<div class="fmn-form-option mod-required">
							<label>Phone Number</label>
							<div class="fmn-form-field"><input type="text"></div>
						</div>
						
						<div class="fmn-form-option">
							<label>Notes for This Address</label>
							<div class="fmn-form-field"><input type="text" placeholder="e.g. if we need to call for access"></div>
						</div>

					</div>
					
					<div class="debug-view debug-view-registered">

						<div class="addressForm-readOnly">
							<div class="readOnly-data">
								<a href="dynamic.php?page=otlDeliveryLoggedIn" class="data-edit fmn-button m-smaller">Change</a>
								Sofia, Bulgaria, 11, Ivan Vazov Str.
							</div>
	
						</div>
	
						<div class="addressForm-readOnly">
							<div class="readOnly-data">
								<a href="#" class="data-edit fmn-button m-smaller">Edit</a>
								John Smith<br>
								johnsmith@smith.com<br>
								0888123456<br>
								
							</div>		
						</div>
						
						<div class="fmn-form-option">
							<label>Notes for This Address</label>
							<div class="fmn-form-field"><input type="text" placeholder="e.g. if we need to call for access"></div>
						</div>
	
						<div class="fmn-form-option" style="margin-bottom: 0;">
							<div class="i-basic-value">
								
								<span class="fm-checkbox">
									<label for="check1">
										<input type="checkbox" name="1" id="check1" checked=""/>
										<span class="fm-checkbox-icon mod-empty"></span>
										<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
										<span class="fm-checkbox-label">Save this address for future orders</span>
									</label>
								</span>
								
<!-- 								<label><input type="checkbox" checked=""> Save this address for future orders</label> -->
							</div>
						</div>
	
						<div class="fmn-form-option" style="padding-top: 0;">
							<div class="i-basic-value">
								
								<span class="fm-checkbox">
									<label for="check2">
										<input type="checkbox" name="1" id="check2" checked=""/>
										<span class="fm-checkbox-icon mod-empty"></span>
										<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
										<span class="fm-checkbox-label">Make it the main address for my profile</span>
									</label>
								</span>
								
							</div>
						</div>
					
					</div>

				</div>
			
			</div>
			
			<? /*
			<div class="locationAddress-findAddress">

<!--
				<div class="fmn-form-option">
					<label>Find an Address</label>
					<div class="fmn-form-option-field">
						<input type="text" placeholder="e.g. ulitsa Ivan Vazov 7, Plovdiv">
						<a href="#" class="field-icon"><span class="e-icon fmn-icon-search"></span></a>
					</div>
				</div>
-->

<!--
				<div class="findAddress-instruction">Please enter your street address above.</div>

				<div class="findAddress-instruction">Thanks! Now complete and confirm your full address details.</div>
				
				<div class="findAddress-tip">Tip: You can also drag the red indicator on the map to visually pin-point your street address.</div>
-->
				<div class="findAddress-instruction">Please review your address location and apply corrections if needed</div>


				<div class="findAddress-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d11831.96115109383!2d24.73210195!3d42.15049465!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbg!4v1518102743587" width="100%" height="240px" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			
			</div>
			*/ ?>

			
			
		</div>



	
	</div><!-- fmn-screen-content-wrap -->
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Next</button> -->
		<a  href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Next: Order Details</a>
	</div><!-- fmn-actions -->











