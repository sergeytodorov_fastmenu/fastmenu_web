	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirdsWithSide">
		
			
			<div class="fmn-listactions">
				<a href="<?=$fmn_next_page_url?>" class="fmn-button">Add New Address</a>
			</div>
			
			<div class="fmn-blocks m-twoCols">
									
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>11, Ivan Vazov Str. (main)</h4>
						<p>
							<span class="c-nobr">Center, Sofia 1000,</span>
							<span class="c-nobr">John Smith,</span>
							<span class="c-nobr">0888 876 543,</span>
							<span class="c-nobr">john@johnsmith.com</span>
						</p>
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Edit</button><a href="#" class="destructive">Delete</a>
					</div>
				</div>
				
				<div class="i-block-wrap">
					<div class="i-block mod-invalid">
						<h4>7, Nikolai Kopernik Str.</h4>
						<p>
							<span class="c-nobr"></span>
							<span class="c-nobr">Geo Milev, Sofia 1000,</span>
							<span class="c-nobr">John Smith,</span>
							<span class="c-nobr">0888 876 543,</span>
							<span class="c-nobr">john@johnsmith.com</span>
							<span class="block-invalidMessage">
								<span>This address is invalid or incomplete.</span>
							</span>
						</p>
						
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Edit</button><a href="#" class="m-destructive">Delete</a>
					</div>
				</div>
				
				<div class="i-block-wrap">
					<div class="i-block">
						<h4>11, Ivan Vazov Str.</h4>
						<p>
							<span class="c-nobr">Center, Sofia 1000,</span>
							<span class="c-nobr">Jane Smith,</span>
							<span class="c-nobr">0888 876 543,</span>
							<span class="c-nobr">jane@johnsmith.com</span>
							<span class="c-nobr">note: Call mobile so we can provide access to lobby</span>
						</p>
<!-- 						<button type="submit" href="#">Edit</button><a href="#" class="destructive">Delete</a> -->
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Edit</button><a href="#" class="destructive">Delete</a>

					</div>
				</div>

<? /*				<div class="i-block-wrap onlyButton">
					<div class="i-block">
<!-- 						<button type="submit" href="#">Add Another Address</button> -->
						<a href="<?=$fmn_next_page_url?>" class="fmn-button">Add Another Address</a>
					</div>
				</div> */ ?>

				
			</div><!-- fmn-blocks -->

		
	</div><!-- fmn-screen-content-wrap -->
	
