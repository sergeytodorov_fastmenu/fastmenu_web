<div class="fmn-screen-messages">	
	<div class="fmn-message m-image m-970">
		<img src="images/sample-ad.jpg"/>
	</div>
</div>

<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">

	
	<div class="fmn-reserve-or-preorder">
	
		<div class="fmn-blocks m-straightLinks m-withArrow m-iconTop m-twoCols">

			<div class="i-block-wrap">
				<a href="?page=reservation" class="i-block">
					<span class="e-icon fmn-icon-pre-order"></span>
					<h4>Make a <span class="c-nobr">Reservation</span></h4>
					<span class="e-rightArrow"><span class="e-icon fmn-icon-arrow-right"></span></span>
				</a>
			</div>
			
			<div class="i-block-wrap">
				<a href="?page=menus" class="i-block">
					<span class="e-icon fmn-icon-menu"></span><span class="e-icon fmn-icon-pre-order"></span>
					<h4>Start a Pre-order <span class="c-nobr">with Reservation</span></h4>
					<span class="e-rightArrow"><span class="e-icon fmn-icon-arrow-right"></span></span>
				</a>
			</div>
			
		</div><!-- fmn-blocks -->

	</div>

</div>