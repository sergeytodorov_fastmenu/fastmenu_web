<div class="fmn-overlay mod-new">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">

		<div class="overlay-header">
			<h3 class="header-title">Privacy and Terms</h3>
			<a href="#" class="header-close"><span class="fmn-icon-delete"></span></a>
		</div>
		<div class="overlay-content">

		
				
			<div class="fmn-overlay-message">
				
				<div style="padding: 20px;">
				<p>In accordance with European GDPR we have updated our:</p>
					<p style="text-align: center; font-size: 18px; margin-bottom: 5px;"><a href="#">Terms of Use</a></p>
					<p style="text-align: center; font-size: 18px;"><a href="#">Privacy Policy</a></p>
				<p>To continue, please read and accept the new things there.</p>
				<p>We use cookies to improve your shopping experience. By staying on the site you accept our cookie policy.</p>
				</div>
				
				<div class="fm-checkboxes" style="text-align: center;">
					<span class="fm-checkbox mod-moreSpace" style="display: inline-block; max-width: 300px; text-align: left;">
						<label for="check1">
							<input type="checkbox" name="1" id="check1"/>
							<span class="fm-checkbox-icon mod-empty"></span>
							<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
							<span class="fm-checkbox-label">I accept the Terms of Use and Privacy Policy and I agree to cookies</span>
						</label>
					</span>
				</div>
<!--
				<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
				
				
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
	
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
				<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
				
				
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
	
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. End.</p>
-->
				
				
			</div>

			

		</div><!-- overlay-content -->
		<div class="overlay-footer">
			<div class="fmn-actions m-right mod-overlay">
				<input type="submit" class="m-large" value="Continue">
			</div><!-- fmn-actions -->
		</div><!-- overlay-footer -->

	</div><!-- fmn-overlay-box -->
	
</div><!-- fmn-overlay -->