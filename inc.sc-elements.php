<? 
	$messages = "yes";
?>

<div class="fmn-screen-content">
	
	<div class="fmn-screen-intro">
		page intros and disclaimers go here
	</div>
	
	<div class="fmn-screen-content-wrap m-fullWidth">
		<div class="debug-spacer">this outlines full width container</div>
	</div>
	
	<div class="fmn-screen-content-wrap m-reducedWidth-oneThird">
		<div class="debug-spacer">this outlines 1/3 container</div>


		<style>
			
			.demo-fields input[type="text"],
			.demo-fields select,
			.demo-fields textarea {
				margin: 5px 0;
			}
			
			.demo-buttons {
				margin: 20px 0;
			}
			
			.demo-fields label,
			.demo-buttons label {
				display: block;
				font-size: 18px;
				margin-top: 20px;
				margin-bottom: 10px;
				margin-left: -20px;
				color: black;
				background-color: #ecefc7;
				padding: 5px 10px;
			}
			
		</style>
		
		<div class="demo-fields">

			<label>input fields</label>

			<input type="text" value="m-small" class="m-small">
			<input type="text" value="normal" class="">
			<input type="text" value="m-large" class="m-large">
			<input type="text" value="m-huge" class="m-huge">
			<input type="text" value="error" class="error">
			<input type="text" value="disabled" disabled="">
			

			<label>selects</label>
			
			<select class="m-small"><option>small</option><option>1</option><option>2</option><option>3</option></select>
			<select class=""><option>normal</option><option>1</option><option>2</option><option>3</option></select>
			<select class="m-large"><option>large</option><option>1</option><option>2</option><option>3</option></select>
			<select class="m-huge"><option>huge</option><option>1</option><option>2</option><option>3</option></select>
			<select class="error"><option>error</option><option>1</option><option>2</option><option>3</option></select>
			<select class="disabled" disabled=""><option>disabled</option><option>1</option><option>2</option><option>3</option></select>
			
			
			<label>textarea</label>
			
			<textarea>textarea</textarea>
			
		</div>
		
		<div class="demo-buttons">

			<label>input button</label>

			<input type="fmn-button" value="m-small" class="m-small">
			<input type="fmn-button" value="normal" class="">
			<input type="fmn-button" value="m-large" class="m-large">
			<input type="fmn-button" value="m-huge" class="m-huge">
			<input type="fmn-button" value="disabled" class="disabled">
			

			<label>input button m-inverted</label>
			
			<input type="fmn-button" value="m-small" class="m-invert m-small">
			<input type="fmn-button" value="normal" class="m-invert ">
			<input type="fmn-button" value="m-large" class="m-invert m-large">
			<input type="fmn-button" value="m-huge" class="m-invert m-huge">
			<input type="fmn-button" value="disabled" class="m-invert m-disabled">
			

			<label>input submit</label>

			<input type="submit" value="m-small" class="m-small">
			<input type="submit" value="normal" class="">
			<input type="submit" value="m-large" class="m-large">
			<input type="submit" value="m-huge" class="m-huge">
			<input type="submit" value="disabled" class="disabled">
			

			<label>input submit m-inverted</label>
			
			<input type="submit" value="m-small" class="m-invert m-small">
			<input type="submit" value="normal" class="m-invert ">
			<input type="submit" value="m-large" class="m-invert m-large">
			<input type="submit" value="m-huge" class="m-invert m-huge">
			<input type="submit" value="disabled" class="m-invert m-disabled">
			
			
			<label>button tag</label>
			
			<button type="fmn-button" class="m-small">small</button>
			<button type="fmn-button" class="">normal</button>
			<button type="fmn-button" class="m-large">large</button>
			<button type="fmn-button" class="m-huge">huge</button>
			<button type="fmn-button" class="m-disabled">disabled</button>
			

			<label>button tag m-inverted</label>
			
			<button type="fmn-button" class="m-invert m-small">small</button>
			<button type="fmn-button" class="m-invert ">normal</button>
			<button type="fmn-button" class="m-invert m-large">large</button>
			<button type="fmn-button" class="m-invert m-huge">huge</button>
			<button type="fmn-button" class="m-invert m-disabled">disabled</button>
			
			
			<label>a.button:link</label>
			
			<a href="#" class="fmn-button m-small">small</a>
			<a href="#" class="fmn-button ">normal</a>
			<a href="#" class="fmn-button m-large">large</a>
			<a href="#" class="fmn-button m-huge">huge</a>
			<a href="#" class="fmn-button m-disabled">disabled</a>
			
			
			<label>a.button:link m-inverted</label>
			
			<a href="#" class="fmn-button m-invert m-small">small</a>
			<a href="#" class="fmn-button m-invert ">normal</a>
			<a href="#" class="fmn-button m-invert m-large">large</a>
			<a href="#" class="fmn-button m-invert m-huge">huge</a>
			<a href="#" class="fmn-button m-invert m-disabled">disabled</a>
			
		</div>


	</div>

	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">
		<div class="debug-spacer">this outlines 2/3 container</div>
	</div>
	
</div><!-- fmn-screen-content -->
