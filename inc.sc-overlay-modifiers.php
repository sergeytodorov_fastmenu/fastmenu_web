<div class="fmn-overlay " style="">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">

		<a href="#" class="fmn-overlay-close"><span class="fmn-icon-delete"></span></a>
		

		<div class="debug-view-6">

			<div class="fmn-overlay-title">
				<h3>Caprese Salad with Long Title to Illustrate How Things Work</h3>
			</div>
			

			<div class="fmn-form fmn-details-overlay">
				
	
				
				
				<div class="fmn-details-options">
			
					<div class="fmn-form-option">
						<label>Size</label>
						<div class="fmn-form-field">
							<div class="fmn-form-buttonArray">
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Small</span><span class="e-subCaption">€12.45</span></button>
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Medium</span><span class="e-subCaption">€18.45</span></button>
								<button class="fmn-button m-invert m-twoLine" href="#"><span>Large</span><span class="e-subCaption">€24.45</span></button>
							</div>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Checks Two-cols</label>
						<div class="fmn-form-field">
							<div class="fmn-form-simpleArray m-twoCol">
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double meat</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double tomatoes</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<div class="i-arrayValue">
									<label><input type="checkbox" name="modifier"> <span>Double snails</span><span class="e-value-detail">+€2.45</span></label>
								</div>
								<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>
		
							</div>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Quantity</label>
						<div class="fmn-form-field fmn-form-numberBox">
							<a href="#"><span class="fmn-icon-minus"></span></a><input class="" type="text" value="2"><a href="#"><span class="fmn-icon-plus"></span></a>
						</div>
						<span class="fmn-form-option-help">3 available</span>
					</div>
		
				</div><!-- fmn-details-options -->
		
		
				<div class="fmn-message">
					<span class="e-icon fmn-icon-online-payment"></span> <span class="e-icontext">This item requires advance online payment.</span>
				</div>
		
				
				<div class="fmn-details-priceAndAddToBasket">
							
					<div class="fmn-details-price">
						<div class="fmn-details-price-single">Box of 4 rolls: <span class="fmn-common-price m-secondary"><span class="e-original">€10.00</span> <span class="e-special">€8.00</span></span></div>
						<div class="fmn-details-price-main">Total for 2: <span class="fmn-common-price"><span class="e-original">€20.00</span> <span class="e-special">€16.00</span></span></div>
						<div class="fmn-details-price-save">You save: <span class="fmn-common-price m-secondary"><span class="e-special">€4.00</span></span></div>
					</div>
					
					
					
					<div class="fmn-details-addToBasket">
											 	
						<button type="submit" class="m-large">Add to Basket</button>
					</div>
				</div><!-- fmn-details-priceAndAddToBasket -->
				
				
				
										
			</div>
		</div>
		
		
		

	</div><!-- fmn-overlay-box -->
	
	
	
	



	
</div><!-- fmn-overlay -->

