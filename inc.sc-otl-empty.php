<div class="fmn-overlay fmn-otl mod-new">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">
		
		<div class="overlay-header">
			<h3 class="header-title">Order Type & Location</h3>
			<a href="#" class="header-close"><span class="fmn-icon-delete"></span></a>
		</div>
		<div class="overlay-content">



			<div class="fmn-form">
				
				<div class="otl-fixWidth fmn-form-option">
					<label>Order Type</label>
					<div class="fmn-form-buttonArray m-fullWidth m-2">
						<a class="fmn-button m-invert" href="dynamic.php?page=otlDeliveryNotLoggedIn"><span>Delivery</span></a>
						<a class="fmn-button m-invert" href="dynamic.php?page=otlTakeAway"><span>Take-away</span></a>
					</div>
				</div>
				
			</div>
			<div class="otl-intro">
				Please select order type above to begin.
			</div>
				


		</div><!-- overlay-content -->
		
	</div><!-- fmn-overlay-box -->

</div><!-- fmn-overlay -->