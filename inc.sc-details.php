<link rel="stylesheet" href="plugins/lightbox/lightbox.css" type="text/css" charset="utf-8" />
<script src='plugins/lightbox/lightbox-2.6.min.js'></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- <script src="plugins/select2/select2.min.js"></script> -->
<script>
	

// 	$(window).on('resize load', function () { 
	$(window).on('load', function () { 
	    $('.js-example-basic-singleA').select2({
		  placeholder: 'Натисни за да добавиш една или повече'
		});
	    $('.js-example-basic-singleB').select2({
		  placeholder: 'Натисни за да добавиш един или повече'
		});
	});
	
	$(window).on('resize load scroll', function () { 
		var fmnWh = $(window).height();
		var fmnWs = $(this).scrollTop();
		var fmnDfh = $(".fmn-details-priceAndAddToBasket-wrap").height();
		var fmnDfo = $(".fmn-details-priceAndAddToBasket-wrap").offset().top;
		var fmnWbe = fmnWh + fmnWs;
		var fmnDfbe = fmnDfo + fmnDfh;
		if (fmnWbe < fmnDfbe ) {
			$(".fmn-details-priceAndAddToBasket").addClass("mod-fixPosition");
		} else {
			$(".fmn-details-priceAndAddToBasket").removeClass("mod-fixPosition");
		}
	});

</script>


<div class="fmn-details">
	
	<div class="n-details-left">
		<div class="fmn-details-photo">
<!-- 			<a class="e-photo" href="images/sample-dish-greek-large.jpg" style="background-image: url(images/sample-dish-caprese.jpg);" data-lightbox="gallery"> -->

			<a class="e-photo" href="images/sample-dish-pizzaBox.jpg" style="background-image: url(images/sample-dish-pizzaBox.jpg);" data-lightbox="gallery">

				<span class="e-zoom"><span class="e-icon fmn-icon-search"></span></span>
			</a>
		</div><!-- fmn-details-photo -->
<!-- 		<div class="e-productCode">#ab818973</div> -->


		

	</div>
	
	
	
	<div class="n-details-right">
	
		<div class="fmn-details-descriptionIngredientsAllergens">

			<div class="fmn-description">
				Пица Маргарита Фунги (с гъби) приготвена с тесто*, доматен сос, моцарела*, гъби манатарки и печурки, зехтин и риган или босилек.
<!-- 				A simple Italian salad with tomatoes, mozzarella*, basil, oregano, olive oil* and salt. It features the colors of the Italian flag: green, white, and red. Usually served as a starter, not a side dish. -->
			</div>
	
			<div class="fmn-allergens">
				<span class="allergens-title">Алергени:</span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/milk.png"><span class="e-caption">Мляко</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/gluten.png"><span class="e-caption">Глутен</span></span>
			</div>										
			
		</div>
		

<!--
		<div class="fmn-details-descriptionIngredientsAllergens">

			<div class="fmn-description">
				A simple Italian salad. It features the colors of the Italian flag: green, white, and red. Usually served as a starter, not a side dish.<br>
			</div>
			
			<div class="fmn-ingredients">
				Ingredients: tomatoes, mozzarella*, basil, oregano, olive oil* and salt<br>
			</div>
	
			<div class="fmn-allergens">
				<span class="allergens-title">Allergens:</span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/milk.png"><span class="e-caption">Dairy</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/sulphites.png"><span class="e-caption">Sulphites</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/fish.png"><span class="e-caption">Fish</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/soya.png"><span class="e-caption">Soya</span></span>
			</div>										
			
		</div>
-->
		
		<div class="fmn-details-options">

			<div class="fmn-form-option m-horizontal">
				<label>Размер</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<button class="fmn-button m-invert m-twoLine m-selected" href="#"><span>Мини (24 см)</span><span class="e-subCaption">7.45 лв</span></button>
						<button class="fmn-button m-invert m-twoLine" href="#"><span>Средна (30 см)</span><span class="e-subCaption">11.25 лв</span></button>
						<button class="fmn-button m-invert m-twoLine" href="#"><span>Гранде (38 см)</span><span class="e-subCaption">16.85 лв</span></button>
					</div>
				</div>
			</div>

			<div class="fmn-form-option m-horizontal">
				<label>Безплатна подправка</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert" href="#"><span>Босилек</span></a>
						<a class="fmn-button m-invert" href="#"><span>Риган</span></a>
					</div>
				</div>
			</div>
			
			<div class="fmn-form-option m-horizontal">
				<label>Добави екстри</label>
				<div class="fmn-form-field">
					<select class="js-example-basic-singleA"  name="states[]" multiple="multiple">
						<option>Замяна на тестото с лимец +1.35 лв</option>
						<option>Замяна на доматен сос със сметана +0.50 лв</option>
						<option>Прошуто крудо +3.75 лв</option>
						<option>Манатарки +2.00 лв</option>
						<option>Шунка френска +1.50 лв</option>
						<option>Песто Дженовезе 🌿 +1.50 лв</option>
						<option>Бекон +1.50 лв</option>
						<option>Пеперони салам 🌶 +1.50 лв</option>
						<option>Луканка +1.50 лв</option>
						<option>Лют зехтин Costa d`Oro 🌶 +1.50 лв</option>
						<option>Чоризо +1.50 лв</option>
						<option>Люти чушки Халапеньо 🌶 +1.50 лв</option>
						<option>Мляно св. & телешко +1.50 лв</option>
						<option>Люти чушки 🌶 +0.94 лв</option>
						<option>Пилешко месо +1.50 лв</option>
						<option>Ананас +1.50 лв</option>
						<option>Филе риба тон (1/2 кутия) +3.75 лв</option>
						<option>Чушки зелени пресни +0.94 лв</option>
						<option>Аншоа 🐟 +3.75 лв</option>
						<option>Домати пресни +0.94 лв</option>
						<option>Моцарела +1.50 лв</option>
						<option>Лук печен +0.94 лв</option>
						<option>Чедър сирене +1.50 лв</option>
						<option>Тиквички +0.94 лв</option>
						<option>Ементал +1.50 лв</option>
						<option>Царевица Bonduelle +0.94 лв</option>
						<option>Пармезан +1.50 лв</option>
						<option>Гъби печурки +0.94 лв</option>
						<option>Синьо сирене +1.50 лв</option>
						<option>Маслини черни +0.94 лв</option>
						<option>Топено сирене LB +1.50 лв</option>
						<option>Кисели краставички +0.94 лв</option>
					</select>
				</div>
			</div>
			
			<div class="fmn-form-option m-horizontal">
				<label>Сос Heinz</label>
				<div class="fmn-form-field">
					<select class="js-example-basic-singleB"  name="states[]" multiple="multiple">
						<option>Доматен сос Heinz (50 гр) +1.25 лв</option>
						<option>Бургер сос Heinz (50 гр) +1.49 лв</option>
						<option>Лют чили сос Heinz (50 гр) +1.25 лв</option>
						<option>Майонеза Heinz (50 гр) +1.49 лв</option>
						<option>Медена горчица Heinz (50 гр) +1.25 лв</option>
						<option>Чеснов сос Heinz (50 гр) +1.25 лв</option>
						<option>Млечен сос Heinz (50 гр) +1.49 лв</option>
					</select>
				</div>
			</div>
			
<!--
			<div class="fmn-form-option m-horizontal">
				<label>Сос Heinz (50 гр)</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray m-twoCol mod-boxed">
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Доматен сос</span><span class="e-value-detail">+1.25 лв</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Бургер сос</span><span class="e-value-detail">+1.49 лв</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Лют чили сос</span><span class="e-value-detail">+1.25 лв</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Майонеза</span><span class="e-value-detail">+1.49 лв</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Медена горчица</span><span class="e-value-detail">+1.25 лв</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Чеснов сос</span><span class="e-value-detail">+1.25 лв</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Млечен сос</span><span class="e-value-detail">+1.49 лв</span></label>
						</div>
						<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Изчисти</span></a>

					</div>
				</div>
			</div>
-->

			
<!--
			<div class="fmn-form-option m-horizontal">
				<label>Checks Two-cols</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray m-twoCol">
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double meat</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double tomatoes</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double snails</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>

					</div>
				</div>
			</div>
-->
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Количество</label>
				<div class="fmn-form-field fmn-form-numberBox">
					<a href="#"><span class="fmn-icon-minus"></span></a><input class="" type="text" value="2"><a href="#"><span class="fmn-icon-plus"></span></a>
				</div>
<!-- 				<span class="fmn-form-option-help">3 available</span> -->
			</div>

		</div><!-- fmn-details-options -->





<!--
		<div class="fmn-message">
			<span class="e-icon fmn-icon-online-payment"></span> <span class="e-icontext">This item requires advance online payment.</span>
		</div>
-->


		<div class="fmn-details-priceAndAddToBasket-wrap mod-noTemplate">
		<div class="fmn-details-priceAndAddToBasket">
			<div class="priceAndAddToBasket-fixWidth">
				<div class="priceAndAddToBasket-wrap">					
					<div class="fmn-details-price">
		<!-- 				<div class="fmn-details-price-single">Box of 4 rolls: <span class="fmn-common-price m-secondary"><span class="e-original">€10.00</span> <span class="e-special">€8.00</span></span></div> -->
		<!-- 				<div class="fmn-details-price-main">Total for 2: <span class="fmn-common-price"><span class="e-normal">€16.90</span></span></div> -->
		
						<div class="fmn-details-price-main">Общо за 2: <span class="fmn-common-price"><span class="e-normal">14.50 лв</span></span></div>
		
		<!-- 				<div class="fmn-details-price-main">Total for 2: <span class="fmn-common-price"><span class="e-original">€8.45</span> <span class="e-special">€16.90</span></span></div> -->
		<!-- 				<div class="fmn-details-price-save">You save: <span class="fmn-common-price m-secondary"><span class="e-special">€4.00</span></span></div> -->
					</div>
					
					
					
					<div class="fmn-details-addToBasket">
						<button type="submit" class="m-large"><!-- Zur Bestellung Hinzufügen -->Добави</button>
					</div>
				</div>
			</div>
		</div><!-- fmn-details-priceAndAddToBasket -->
		</div>

		
		<? /*
		<div class="fmn-detailsFixedFooter">
			<div class="fmn-fixWidth">
				<div class="detailsFixedFooter-wrap">
					<div class="detailsFixedFooter-price">
						<span class="price-label">Total for 2:</span>
	<!--
						<div class="price-qty fmn-form-numberBox">
							<a href="#"><span class="fmn-icon-minus"></span></a><span class="qty-number">2</span><a href="#"><span class="fmn-icon-plus"></span></a>
						</div>
	-->
						<span class="price-total fmn-common-price"><span class="e-original">€8.45</span> <span class="e-special">€16.90</span></span>
					</div>
					
					<div class="detailsFixedFooter-addToBasket">
						<button type="submit" class="m-large"><!-- Zur Bestellung Hinzufügen -->Добави</button>
					</div>
				</div>

			</div>
		</div><!-- fmn-details-priceAndAddToBasket -->
		*/ ?>
		
		
		
		
		
<!--
		<div class="fmn-section fmn-details-description">
			<h3 class="fmn-section-title">Description</h3>
			<p class="fmn-section-data">
				A Simple Italian salad with Tomatoes, Mozzarella, Basil, Oregano, Olive Oil and Salt. Fresh Neapolitan atmosphere right at the table in Trakia.
				A simple Italian salad. It features the colors of the Italian flag: green, white, and red. Usually served as a starter, not a side dish.
			</p>
		</div>
-->
									
		
<!--
		<div class="fmn-section fmn-details-ingredients">
			<h3 class="fmn-section-title">Ingredients</h3>
			<p class="fmn-section-data">
				Tomatoes, Mozzarella*, Basil, Oregano, Olive Oil*, Salt<br>
				* — ingredients containing allergens
			</p>
		</div>
-->
	
	</div><!-- fmn-details-infoAndActions -->
	
</div><!-- fmn-details -->