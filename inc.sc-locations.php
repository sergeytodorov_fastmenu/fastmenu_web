<div class="fmn-screen-content">

	<div class="fmn-screen-messages">	
		<div class="fmn-message">
			<p>Testing message above tiles</p>
		</div>
	</div>
	<div class="fmn-tiles">
	
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
		</div>
		<div class="i-tile-under">
			<span class="e-location">Roman Wall Market</span>
			<span class="e-worktime">Saturday 11:00 – 15:00</span>
			<span class="e-order"><a href="#" class="e-link">Order for July 30</a><span class="e-until">(until July 29)</span></span>
		</div>
		</div>
	 
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
		</div>
		<div class="i-tile-under">
			<span class="e-location">Business Park Market</span>
			<span class="e-worktime">Saturday 11:00 – 15:00</span>
			<span class="e-order"><a href="#" class="e-link">Order for July 30</a><span class="e-until">(until July 29)</span></span>
		</div>
		</div>
				
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
		 	<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
		</div>
		<div class="i-tile-under">
			<span class="e-location">Ivan Vazov Market Wednesday</span>
			<span class="e-worktime">Wednesday 11:00 – 15:00</span>
			<span class="e-order"><a href="#" class="e-link">Order for July 30</a><span class="e-until">(until July 29)</span></span>
		</div>
		</div>
									
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
		</div>
		<div class="i-tile-under">
			<span class="e-location">Ivan Vazov Market Saturday</span>
			<span class="e-worktime">Saturday 11:00 – 15:00</span>
			<span class="e-order"><a href="#" class="e-link">Order for July 30</a><span class="e-until">(until July 29)</span></span>
		</div>
		</div>
		
		<div class="i-tile-wrap">
		<div class="i-tile">
			<span class="e-photo" style="background-image: url(images/sample-image-1x1.png);"></span>
			<a href="<?=$fmn_next_page_url?>" class="e-toucher"></a>
		</div>
		<div class="i-tile-under">
			<span class="e-location">Borovo Market</span>
			<span class="e-worktime">Saturday 11:00 – 15:00</span>
			<span class="e-order"><a href="#" class="e-link">Order for July 30</a><span class="e-until">(until July 29)</span></span>
		</div>
		</div>

			
	</div><!-- tiles -->

</div><!-- fmn-screen-content -->