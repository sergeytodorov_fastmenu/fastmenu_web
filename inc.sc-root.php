<div class="fmn-screen-content-wrap m-fullWidth">

	<div class="fmn-screen-content-wrap-columns">

		<div class="fmn-screen-content-wrap-column m-oneThird">
			<h2 class="m-secondaryTitle">Welcome</h2>
			<div class="fmn-screen-intro">
				Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.		
			</div>			
		</div>
		
		<div class="fmn-screen-content-wrap-column m-oneThird">

			<h2 class="m-secondaryTitle">Worktimes</h2>
			<div class="fmn-portal-workTimes">
				
				<div class="i-workTimeDay">
					<div class="e-day">Monday</div>
					<div class="e-time">
						<div class="i-range">We're closed</div>
					</div>
				</div>
				<div class="i-workTimeDay">
					<div class="e-day">Tuesday</div>
					<div class="e-time">
						<div class="i-range">12:00–15:00</div>
						<div class="i-range">18:00–23:00</div>
					</div>
				</div>
				<div class="i-workTimeDay">
					<div class="e-day">Wednesday</div>
					<div class="e-time">
						<div class="i-range">12:00–15:00</div>
						<div class="i-range">18:00–23:00</div>
					</div>
				</div>
				<div class="i-workTimeDay">
					<div class="e-day">Thursday</div>
					<div class="e-time">
						<div class="i-range">12:00–15:00</div>
						<div class="i-range">18:00–23:00</div>
					</div>
				</div>
				<div class="i-workTimeDay">
					<div class="e-day">Friday</div>
					<div class="e-time">
						<div class="i-range">12:00–15:00</div>
						<div class="i-range">18:00–23:00</div>
					</div>
				</div>
				<div class="i-workTimeDay">
					<div class="e-day">Saturday</div>
					<div class="e-time">
						<div class="i-range">12:00–23:00</div>
					</div>
				</div>
				<div class="i-workTimeDay">
					<div class="e-day">Sunday</div>
					<div class="e-time">
						<div class="i-range">We're closed</div>
					</div>
				</div>
			</div><!-- fmn-workTime -->
		</div>



		<div class="fmn-screen-content-wrap-column m-oneThird">
			<h2 class="m-secondaryTitle">Contact Us</h2>
			<div class="fmn-portal-contact">
				<div class="i-contact">
					<a href="#" class="debug-clickTap"><span class="e-icon fmn-icon-store"></span><span class="e-label">Sofia, Bulgaria, 11 Ivan Vazov Str.</span></a>
				</div>
				<div class="i-contact">
					<a href="#" class="debug-clickTap"><span class="e-icon fmn-icon-phone"></span><span class="e-label">Call 0214341177</span></a>
				</div>
				<div class="i-contact">
					<a href="#" class="debug-clickTap"><span class="e-icon fmn-icon-envelope"></span><span class="e-label">Email info@portalfood.com</span></a>
				</div>
			</div><!-- fmn-callShortcut -->
		</div>

	</div>						

	
	<div class="fmn-portal-brands">
	
		<h2 class="m-secondaryTitle">Brands</h2>
					
		<div class="fmn-brands">
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-sakura.png">
					<span class="e-brand-label">Sakura</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-nandos.png">
					<span class="e-brand-label">Nando's</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-mickeyds.jpg">
					<span class="e-brand-label">McDonalds</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-steers.png">
					<span class="e-brand-label">Steers</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-steershalal.png">
					<span class="e-brand-label">Steers Halal</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-spur.png">
					<span class="e-brand-label">Spur</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-pizzaperfect.png">
					<span class="e-brand-label">Pizza Perfect</span>					
				</a>
			</div>
			<div class="i-brand-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-brand">
					<img class="e-brand-logo" src="images/sample-restaurant-panarottis.png">
					<span class="e-brand-label">Panarottis</span>					
				</a>
			</div>
		</div><!-- fmn-brands -->
	
	</div>

</div><!-- fmn-screen-content-wrap -->