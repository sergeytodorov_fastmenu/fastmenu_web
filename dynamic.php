<!DOCTYPE html>

<html>

	<head>
	
		<? require_once("include.htmlHead.php"); ?>
		
	</head>



	<?
		$allowed = array(
	    	
	    	"root" => array( 
		    	"pageId" => "root",
		    	"columns" => "one",
	    		"content" => "inc.sc-root.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "-",
	    		"screen_title" => "Welcome",
	    		"next_page_url" => "branches",
	    		"otl" => "no"
	    	),
	    	"branches" => array( 
		    	"pageId" => "branches",
		    	"columns" => "one",
	    		"content" => "inc.sc-branches.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "root",
	    		"screen_title" => "Branches",
	    		"next_page_url" => "locations",
	    		"otl" => "no"
	    	),
	    	"locations" => array( 
		    	"pageId" => "locations",
		    	"columns" => "two",
	    		"content" => "inc.sc-locations.php", 
	    		"sidebar_type" => "locations",
	    		"sidebar_title" => "Branches",
	    		"back_link" => "yes", 
	    		"back_link_url" => "root",
	    		"screen_title" => "Locations",
	    		"next_page_url" => "menus",
	    		"otl" => "no"
	    	),
	    	"start" => array(  
		    	"pageId" => "start",
		    	"columns" => "one",
	    		"content" => "inc.sc-start.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "-",
	    		"screen_title" => "Select Order Type",
	    		"next_page_url" => "menus",
	    		"page_type" => "start",
	    		"show_brand" => "yes",
	    		"otl" => "no"
	    	),

	    	"menus" => array( 
		    	"pageId" => "menus",
		    	"columns" => "one",
	    		"content" => "inc.sc-menus.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-", 
	    		"back_link" => "yes", 
	    		"back_link_url" => "root",
	    		"screen_title" => "Menus",
	    		"next_page_url" => "categories",
	    		"module_otl" => "available"
	    	),
	    	"categories" => array( 
		    	"pageId" => "categories",
		    	"columns" => "two",
	    		"content" => "inc.sc-categories.php", 
	    		"sidebar_type" => "categories",
	    		"sidebar_title" => "Menus",
	    		"back_link" => "yes", 
	    		"back_link_url" => "menus",
	    		"screen_title" => "Categories",
	    		"next_page_url" => "dishes&view=tilesCaptionInside",
	    		"module_otl" => "available"
	    	),
	    	"dishes" => array( 
		    	"pageId" => "dishes",
		    	"columns" => "two",
	    		"content" => "inc.sc-dishes.php", 
	    		"sidebar_type" => "dishes", 
	    		"sidebar_title" => "Categories",
	    		"back_link" => "yes", 
	    		"back_link_url" => "categories",
	    		"screen_title" => "Dishes",
	    		"next_page_url" => "details",
	    		"show_brand" => "yes",
	    		"module_otl" => "available"
	    	),
	    	"dishes-new" => array( 
		    	"pageId" => "dishes",
		    	"columns" => "two",
	    		"content" => "inc.sc-dishes-new.php", 
	    		"sidebar_type" => "dishes", 
	    		"sidebar_title" => "Categories",
	    		"back_link" => "yes", 
	    		"back_link_url" => "categories",
	    		"screen_title" => "Dishes",
	    		"next_page_url" => "details",
	    		"show_brand" => "yes",
	    		"module_otl" => "available"
	    	),
	    	"dishes-basket" => array( 
		    	"pageId" => "dishes",
		    	"columns" => "basket",
	    		"content" => "inc.sc-dishes.php", 
	    		"sidebar_type" => "basket", 
	    		"sidebar_title" => "Basket",
	    		"back_link" => "yes", 
	    		"back_link_url" => "categories",
	    		"screen_title" => "Dishes",
	    		"next_page_url" => "details",
	    		"show_brand" => "yes",
	    		"module_otl" => "available"
	    	),
	    	"details" => array( 
		    	"pageId" => "details",
		    	"columns" => "two",
	    		"content" => "inc.sc-details.php", 
	    		"sidebar_type" => "details", 
	    		"sidebar_title" => "Пица",
	    		"back_link" => "yes", 
	    		"back_link_url" => "dishes&view=tilesCaptionInside",
	    		"screen_title" => "Маргарита Фунги",
	    		"module_otl" => "available"
	    	),
	    	"details-all" => array( 
		    	"pageId" => "details-all",
		    	"columns" => "two",
	    		"content" => "inc.sc-details-all.php", 
	    		"sidebar_type" => "details", 
	    		"sidebar_title" => "Dishes",
	    		"back_link" => "yes", 
	    		"back_link_url" => "dishes&view=tilesCaptionInside",
	    		"screen_title" => "Caprese Salad with Long Title to Illustrate How Things Work",
	    		"module_otl" => "available"
	    	),
	    	"basket" => array( 
		    	"pageId" => "basket",
		    	"columns" => "one",
	    		"content" => "inc.sc-basket.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "menu", 
	    		"back_link_url" => "categories",
	    		"screen_title" => "Basket",
	    		"page_type" => "basket",
	    		"module_otl" => "available"
	    	),
	    	"basket-empty" => array( 
		    	"pageId" => "basket-empty",
		    	"columns" => "one",
	    		"content" => "inc.sc-basket-empty.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "",
	    		"screen_title" => "Basket",
	    		"page_type" => "basket",
	    		"module_otl" => "available"
	    	),
	    	"account-main" => array( 
		    	"pageId" => "account-main",
		    	"columns" => "one",
	    		"content" => "inc.sc-account-main.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "-",
	    		"screen_title" => "Account",
	    		"page_type" => "account"
	    	),
	    	"account-personal-info" => array( 
		    	"pageId" => "account-personal-info",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-personal-info.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "Personal Info",
	    		"page_type" => "account"
	    	),
	    	"account-login-methods" => array( 
		    	"pageId" => "account-login-methods",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-login-methods.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "Login Methods",
	    		"page_type" => "account"
	    	),
	    	"account-password" => array( 
		    	"pageId" => "account-password",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-password.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "Password",
	    		"page_type" => "account"
	    	),
	    	"account-my-addresses" => array( 
		    	"pageId" => "account-my-addresses",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-my-addresses.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "My Addresses",
	    		"page_type" => "account",
	    		"next_page_url" => "account-edit-address"
	    	),
	    	"account-edit-address" => array( 
		    	"pageId" => "account-edit-address",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-edit-address.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-my-addresses",
	    		"screen_title" => "Edit Address",
	    		"page_type" => "account",
	    		"next_page_url" => "account-my-addresses"
	    	),
	    	"account-my-orders" => array( 
		    	"pageId" => "account-my-orders",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-my-orders.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "My Orders",
	    		"page_type" => "account",
	    		"next_page_url" => "account-order-details"
	    	),
	    	"account-order-details" => array( 
		    	"pageId" => "account-order-details",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-order-details.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-my-orders",
	    		"screen_title" => "Order Details",
	    		"page_type" => "account"
	    	),
	    	"account-payment-details" => array( 
		    	"pageId" => "account-payment-details",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-payment-details.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "Payment Details",
	    		"page_type" => "account"
	    	),
	    	"account-payment-defaults" => array( 
		    	"pageId" => "account-payment-defaults",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-payment-defaults.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-payment-details",
	    		"screen_title" => "Payment Defaults",
	    		"page_type" => "account",
	    		"next_page_url" => "account-payment-details"
	    	),
	    	"account-company-invoice-details" => array( 
		    	"pageId" => "account-company-invoice-details",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-company-invoice-details.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-payment-details",
	    		"screen_title" => "Company Invoice Details",
	    		"page_type" => "account",
	    		"next_page_url" => "account-payment-details"
	    	),
	    	"account-company-ordering" => array( 
		    	"pageId" => "account-company-ordering",
		    	"columns" => "two",
	    		"content" => "inc.sc-account-company-ordering.php", 
	    		"sidebar_type" => "account", 
	    		"sidebar_title" => "Account",
	    		"back_link" => "yes", 
	    		"back_link_url" => "account-main",
	    		"screen_title" => "Company Ordering",
	    		"page_type" => "account"
	    	),
	    	"checkout-complete-address" => array( 
		    	"pageId" => "checkout-delivery-address",
		    	"columns" => "one",
	    		"content" => "inc.sc-checkout-complete-address.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "basket",
	    		"screen_title" => "Delivery Address",
	    		"page_type" => "checkout",
	    		"next_page_url" => "checkout-order-details&view=core",
	    		"checkout_step" => "1",
	    		"page_type" => "checkout"
	    	),
	    	"checkout-delivery-address" => array( 
		    	"pageId" => "checkout-delivery-address",
		    	"columns" => "one",
	    		"content" => "inc.sc-checkout-delivery-address.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "basket",
	    		"screen_title" => "Delivery Address",
	    		"page_type" => "checkout",
	    		"next_page_url" => "checkout-order-details",
	    		"checkout_step" => "1"
	    	),
	    	"checkout-specify-address" => array( 
		    	"pageId" => "checkout-specify-address",
		    	"columns" => "one",
	    		"content" => "inc.sc-checkout-specify-address.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "checkout-delivery-address",
	    		"screen_title" => "Specify Address",
	    		"page_type" => "checkout",
	    		"next_page_url" => "checkout-order-details",
	    		"checkout_step" => "1"
	    	),
	    	"checkout-order-details" => array( 
		    	"pageId" => "checkout-order-details",
		    	"columns" => "one",
	    		"content" => "inc.sc-checkout-order-details.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "checkout-complete-address&view=guest",
	    		"screen_title" => "Order Details",
	    		"page_type" => "checkout",
	    		"next_page_url" => "checkout-contact-info",
	    		"checkout_step" => "2"
	    	),
	    	"checkout-contact-info" => array( 
		    	"pageId" => "checkout-contact-info",
		    	"columns" => "one",
	    		"content" => "inc.sc-checkout-contact-info.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "checkout-order-details&view=core",
	    		"screen_title" => "Contact Info",
	    		"page_type" => "checkout",
	    		"next_page_url" => "checkout-payment-details&view=core",
	    		"checkout_step" => "3"
	    	),
	    	"checkout-payment-details" => array( 
		    	"pageId" => "checkout-payment-details",
		    	"columns" => "one",
	    		"content" => "inc.sc-checkout-payment-details.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "checkout-contact-info",
	    		"screen_title" => "Payment Details",
	    		"page_type" => "checkout",
	    		"next_page_url" => "review-order",
	    		"checkout_step" => "4"
	    	),
	    	"review-order" => array( 
		    	"pageId" => "review-order",
		    	"columns" => "one",
	    		"content" => "inc.sc-review-order.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "checkout-payment-details&view=core",
	    		"screen_title" => "Review Order",
	    		"page_type" => "checkout",
	    		"next_page_url" => "thank-you",
	    		"checkout_step" => "5"
	    	),
	    	"overlay" => array( 
		    	"pageId" => "overlay",
		    	"columns" => "one",
	    		"content" => "inc.sc-overlay.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"overlayPlainContent" => array( 
		    	"pageId" => "overlay",
		    	"columns" => "one",
	    		"content" => "inc.sc-overlay-plainContent.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"overlayFilter" => array( 
		    	"pageId" => "overlay",
		    	"columns" => "one",
	    		"content" => "inc.sc-overlay-filter.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"overlayModifiers" => array( 
		    	"pageId" => "overlay",
		    	"columns" => "one",
	    		"content" => "inc.sc-overlay-modifiers.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"overlayModifiersAll" => array( 
		    	"pageId" => "overlay",
		    	"columns" => "one",
	    		"content" => "inc.sc-overlay-modifiersAll.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"otl" => array( 
		    	"pageId" => "otl",
		    	"columns" => "one",
	    		"content" => "inc.sc-otl.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"otlEmpty" => array( 
		    	"pageId" => "otl",
		    	"columns" => "one",
	    		"content" => "inc.sc-otl-empty.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"otlDeliveryLoggedIn" => array( 
		    	"pageId" => "otl",
		    	"columns" => "one",
	    		"content" => "inc.sc-otl-deliveryLoggedIn.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"otlDeliveryNotLoggedIn" => array( 
		    	"pageId" => "otl",
		    	"columns" => "one",
	    		"content" => "inc.sc-otl-deliveryNotLoggedIn.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"otlTakeAway" => array( 
		    	"pageId" => "otl",
		    	"columns" => "one",
	    		"content" => "inc.sc-otl-takeAway.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"otlTakeAwaySingleLocation" => array( 
		    	"pageId" => "otl",
		    	"columns" => "one",
	    		"content" => "inc.sc-otl-takeAwaySingleLocation.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Overlay",
	    		"page_type" => "checkout"
	    	),
	    	"reserve-or-preorder" => array( 
		    	"pageId" => "reserve-or-preorder",
		    	"columns" => "one",
	    		"content" => "inc.sc-reserve-or-preorder.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Reserve or Preorder",
	    		"page_type" => "account",
	    		"next_page_url" => "menus"
	    	),
	    	"reservation" => array( 
		    	"pageId" => "reservation",
		    	"columns" => "one",
	    		"content" => "inc.sc-reservation.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Reservation",
	    		"page_type" => "account"
	    	),
	    	"login" => array( 
		    	"pageId" => "login",
		    	"columns" => "one",
	    		"content" => "inc.sc-login.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Login",
	    		"page_type" => "account"
	    	),
	    	"request-access" => array( 
		    	"pageId" => "request-access",
		    	"columns" => "one",
	    		"content" => "inc.sc-request-access.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "-",
	    		"screen_title" => "Request Access",
	    		"page_type" => "account",
	    		"next_page_url" => "login"
	    	),
	    	"register-company-details" => array( 
		    	"pageId" => "register-company-details",
		    	"columns" => "one",
	    		"content" => "inc.sc-register-company-details.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "register",
	    		"screen_title" => "Company Details",
	    		"page_type" => "account",
	    		"next_page_url" => "menus"
	    	),
	    	"enter-code" => array( 
		    	"pageId" => "enter-code",
		    	"columns" => "one",
	    		"content" => "inc.sc-enter-code.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "login",
	    		"screen_title" => "Enter Company Code",
	    		"page_type" => "account",
	    		"next_page_url" => "menus"
	    	),
	    	"code-contact-us" => array( 
		    	"pageId" => "code-contact-us",
		    	"columns" => "one",
	    		"content" => "inc.sc-code-contact-us.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "enter-code",
	    		"screen_title" => "Contact Us for Company Code",
	    		"page_type" => "account",
	    		"next_page_url" => "start"
	    	),
	    	"register" => array( 
		    	"pageId" => "register",
		    	"columns" => "one",
	    		"content" => "inc.sc-register.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Register",
	    		"page_type" => "account"
	    	),
	    	"thank-you" => array( 
		    	"pageId" => "thank-you",
		    	"columns" => "one",
	    		"content" => "inc.sc-thank-you.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Thank You",
	    		"page_type" => "checkout",
	    		"next_page_url" => "menus",
	    		"checkout_step" => "6"
	    	),
	    	"elements" => array( 
		    	"pageId" => "elements",
		    	"columns" => "one",
	    		"content" => "inc.sc-elements.php", 
	    		"sidebar_type" => "-", 
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "—",
	    		"screen_title" => "Elements",
	    		"page_type" => "account"
	    	),
	    	"events" => array(  
		    	"pageId" => "events",
		    	"columns" => "one",
	    		"content" => "inc.sc-events.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "-",
	    		"screen_title" => "Events",
	    		"next_page_url" => "event-details",
	    		"page_type" => "account"
	    	),
	    	"event-details" => array(  
		    	"pageId" => "event-details",
		    	"columns" => "one",
	    		"content" => "inc.sc-event-details.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "events",
	    		"screen_title" => "Event",
	    		"next_page_url" => "",
	    		"page_type" => "account"
	    	),
	    	"promos" => array(  
		    	"pageId" => "promos",
		    	"columns" => "one",
	    		"content" => "inc.sc-promos.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "no", 
	    		"back_link_url" => "-",
	    		"screen_title" => "Promos",
	    		"next_page_url" => "promo-details",
	    		"page_type" => "account"
	    	),
	    	"promo-details" => array( 
		    	"pageId" => "promo-details",
		    	"columns" => "one",
	    		"content" => "inc.sc-promo-details.php", 
	    		"sidebar_type" => "-",
	    		"sidebar_title" => "-",
	    		"back_link" => "yes", 
	    		"back_link_url" => "promos",
	    		"screen_title" => "Promo",
	    		"next_page_url" => "",
	    		"page_type" => "account"
	    	)
		);

		$fmn_page = $allowed[$_GET["page"]]["pageId"];
		$fmn_columns = $allowed[$_GET["page"]]["columns"];
		$fmn_content = $allowed[$_GET["page"]]["content"];
		$fmn_sidebar_type = $allowed[$_GET["page"]]["sidebar_type"];
		$fmn_sidebar_title = $allowed[$_GET["page"]]["sidebar_title"];
		$fmn_back_link = $allowed[$_GET["page"]]["back_link"];
		$fmn_back_link_url = "dynamic.php?page=".$allowed[$_GET["page"]]["back_link_url"];
		$fmn_screen_title = $allowed[$_GET["page"]]["screen_title"];
		$fmn_next_page_url = "dynamic.php?page=".$allowed[$_GET["page"]]["next_page_url"];
		$fmn_page_type = $allowed[$_GET["page"]]["page_type"];
		$fmn_show_brand = $allowed[$_GET["page"]]["show_brand"];
		$fmn_checkout_step = $allowed[$_GET["page"]]["checkout_step"];
		$fmn_otl = $allowed[$_GET["page"]]["otl"];
		$fmn_module_otl = $allowed[$_GET["page"]]["module_otl"];
		
		
	?>

	<body class="<?=$fmn_page?>">


<!--  	<? include("include.brand-frame.php"); ?> -->
	
	
	<div id="fmn-root">

	<? if ($fmn_columns == "one") { ?>
		

<? /*
		single column 

		<div class="fmn-fixWidth">
			
			<div class="fmn-page">
		
				<? include("include.header.php"); ?>
				
				<div class="fmn-page-body">
	
					<div class="fmn-screen-container">
						
						<div class="fmn-screen-header" id="fmnid-screen-header">
							<? include("include.screen-header.php"); ?>
						</div>
						
						<div class="fmn-screen-content">
		
							<div class="fmn-screen-content">
								<? include($fmn_content); ?>
							</div>
		
						</div>
						
					</div>
				
				</div>
			
			</div>
	
		</div>
*/ ?>


		
		<!--
		* single columns
		-->

		<div class="fmn-fixWidth">
			
			<div class="fmn-page">
		
				<? include("include.header.php"); ?>
					
				<div class="fmn-page-body">
					
					<div class="fmn-pageColumns m-singleCol">
					
						<div class="fmn-pageColumn m-mainCol">

							<div class="fmn-screen-header" id="fmnid-screen-header">
								<? include("include.screen-header.php"); ?>
							</div>
							
							<div class="fmn-screen-content">
								<? include($fmn_content); ?>
							</div>
							
						</div><!-- fmn-mainCol -->
	
					</div><!-- fmn-pageColumns -->	
	
				</div>
			
			</div>
				
		</div><!-- fmn-fixWidth -->
		
		
		
		
		
		
		
<? } else if ($fmn_columns == "two") { ?>

		<!--
		* two columns
		-->

		<div class="fmn-fixWidth">
			
			<div class="fmn-page">
		
				<? include("include.header.php"); ?>
					
				<div class="fmn-page-body">
					
					<div class="fmn-pageColumns">
					
						<div class="fmn-pageColumn m-leftCol">
							
							<div class="fmn-sidebar">
								<? include("include.screen-sidebar.php"); ?>
							</div>
							
						</div><!-- fmn-leftCol -->	
		
						<div class="fmn-pageColumn m-mainCol">
							<div class="fmn-screen-header" id="fmnid-screen-header">
								<? include("include.screen-header.php"); ?>
							</div>
							
							<div class="fmn-screen-content">
								<? include($fmn_content); ?>
							</div>
							
						</div><!-- fmn-mainCol -->
	
					</div><!-- fmn-pageColumns -->	
	
				</div>
			
			</div>
				
		</div><!-- fmn-fixWidth -->


	<? } else if ($fmn_columns == "basket") { ?>

		<!--
		* two columns
		-->

		<div class="fmn-fixWidth">
			
			<div class="fmn-page">
		
				<? include("include.header.php"); ?>
					
				<div class="fmn-page-body">
					
					<div class="fmn-pageColumns">
					
						<div class="fmn-pageColumn m-mainCol">
							<div class="fmn-screen-header" id="fmnid-screen-header">
								<? include("include.screen-header.php"); ?>
							</div>
							
							<div class="fmn-screen-content">
								<? include($fmn_content); ?>
							</div>
							
						</div><!-- fmn-mainCol -->

						<div class="fmn-pageColumn m-leftCol">
							
							<div class="fmn-sidebar">
								<? include("include.screen-sidebar.php"); ?>
							</div>
							
						</div><!-- fmn-leftCol -->	

	
					</div><!-- fmn-pageColumns -->	
	
				</div>
			
			</div>
				
		</div><!-- fmn-fixWidth -->

	<? } ?>

	<? include("include.debug.php"); ?>
	
		<div class="fmn-footer">
			
			<div class="fmn-fixWidth">
				
				<div class="footer-left">
					<div class="footer-copyright">© 2018 All rights reserved.</div>
					<div class="footer-links">
						<a href="javascript:void(0);">Terms & Conditions</a>
						<a href="javascript:void(0);">Privacy Policy</a>
						<a href="javascript:void(0);">Cookies</a>
					</div>
					<div class="footer-language">
						<a href="javascript:void(0);">Български</a>
						<a href="javascript:void(0);" class="mod-current">English</a>
					</div>
				</div>
				<div class="footer-right">
					<div class="footer-social">
						<a href="javascript:void(0);" class="social-facebook"><svg aria-labelledby="simpleicons-facebook-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title id="simpleicons-facebook-icon">Facebook icon</title><path d="M22.676 0H1.324C.593 0 0 .593 0 1.324v21.352C0 23.408.593 24 1.324 24h11.494v-9.294H9.689v-3.621h3.129V8.41c0-3.099 1.894-4.785 4.659-4.785 1.325 0 2.464.097 2.796.141v3.24h-1.921c-1.5 0-1.792.721-1.792 1.771v2.311h3.584l-.465 3.63H16.56V24h6.115c.733 0 1.325-.592 1.325-1.324V1.324C24 .593 23.408 0 22.676 0"/></svg></a>
						<a href="javascript:void(0);" class="social-instagram"><svg aria-labelledby="simpleicons-instagram-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title id="simpleicons-instagram-icon">Instagram icon</title><path d="M12 0C8.74 0 8.333.015 7.053.072 5.775.132 4.905.333 4.14.63c-.789.306-1.459.717-2.126 1.384S.935 3.35.63 4.14C.333 4.905.131 5.775.072 7.053.012 8.333 0 8.74 0 12s.015 3.667.072 4.947c.06 1.277.261 2.148.558 2.913.306.788.717 1.459 1.384 2.126.667.666 1.336 1.079 2.126 1.384.766.296 1.636.499 2.913.558C8.333 23.988 8.74 24 12 24s3.667-.015 4.947-.072c1.277-.06 2.148-.262 2.913-.558.788-.306 1.459-.718 2.126-1.384.666-.667 1.079-1.335 1.384-2.126.296-.765.499-1.636.558-2.913.06-1.28.072-1.687.072-4.947s-.015-3.667-.072-4.947c-.06-1.277-.262-2.149-.558-2.913-.306-.789-.718-1.459-1.384-2.126C21.319 1.347 20.651.935 19.86.63c-.765-.297-1.636-.499-2.913-.558C15.667.012 15.26 0 12 0zm0 2.16c3.203 0 3.585.016 4.85.071 1.17.055 1.805.249 2.227.415.562.217.96.477 1.382.896.419.42.679.819.896 1.381.164.422.36 1.057.413 2.227.057 1.266.07 1.646.07 4.85s-.015 3.585-.074 4.85c-.061 1.17-.256 1.805-.421 2.227-.224.562-.479.96-.899 1.382-.419.419-.824.679-1.38.896-.42.164-1.065.36-2.235.413-1.274.057-1.649.07-4.859.07-3.211 0-3.586-.015-4.859-.074-1.171-.061-1.816-.256-2.236-.421-.569-.224-.96-.479-1.379-.899-.421-.419-.69-.824-.9-1.38-.165-.42-.359-1.065-.42-2.235-.045-1.26-.061-1.649-.061-4.844 0-3.196.016-3.586.061-4.861.061-1.17.255-1.814.42-2.234.21-.57.479-.96.9-1.381.419-.419.81-.689 1.379-.898.42-.166 1.051-.361 2.221-.421 1.275-.045 1.65-.06 4.859-.06l.045.03zm0 3.678c-3.405 0-6.162 2.76-6.162 6.162 0 3.405 2.76 6.162 6.162 6.162 3.405 0 6.162-2.76 6.162-6.162 0-3.405-2.76-6.162-6.162-6.162zM12 16c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm7.846-10.405c0 .795-.646 1.44-1.44 1.44-.795 0-1.44-.646-1.44-1.44 0-.794.646-1.439 1.44-1.439.793-.001 1.44.645 1.44 1.439z"/></svg></a>
						<a href="javascript:void(0);" class="social-tripadvisor"><svg aria-labelledby="simpleicons-tripadvisor-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title id="simpleicons-tripadvisor-icon">TripAdvisor icon</title><path d="M23.011 9.532c.281-1.207 1.175-2.416 1.175-2.416h-4.012c-2.251-1.455-4.981-2.226-8.013-2.226-3.14 0-5.978.78-8.214 2.251H.186s.885 1.186 1.17 2.386C.624 10.534.189 11.749.189 13.084c0 3.316 2.697 6.008 6.012 6.008 1.891 0 3.571-.885 4.681-2.254l1.275 1.916 1.291-1.936c.57.736 1.32 1.336 2.205 1.74 1.455.66 3.092.736 4.592.18 3.106-1.154 4.696-4.621 3.556-7.726-.209-.556-.48-1.051-.81-1.485l.02.005zm-3.171 8.072c-1.2.445-2.505.395-3.67-.143-.824-.383-1.503-.982-1.988-1.727-.201-.299-.375-.623-.503-.971-.146-.395-.22-.803-.259-1.215-.074-.832.045-1.673.405-2.453.54-1.164 1.501-2.051 2.701-2.496 2.49-.914 5.25.361 6.166 2.841.916 2.481-.36 5.245-2.835 6.163h-.017zm-9.668-1.834c-.863 1.271-2.322 2.113-3.973 2.113-2.646 0-4.801-2.156-4.801-4.797 0-2.641 2.156-4.802 4.801-4.802s4.798 2.161 4.798 4.802c0 .164-.03.314-.048.479-.081.811-.341 1.576-.777 2.221v-.016zM3.15 13.023c0 1.641 1.336 2.971 2.971 2.971s2.968-1.33 2.968-2.971c0-1.635-1.333-2.964-2.966-2.964-1.636 0-2.971 1.329-2.971 2.964H3.15zm12.048 0c0 1.641 1.329 2.971 2.968 2.971 1.636 0 2.965-1.33 2.965-2.971 0-1.635-1.329-2.964-2.965-2.964-1.635 0-2.971 1.329-2.971 2.964h.003zm-11.022 0c0-1.071.869-1.943 1.936-1.943 1.064 0 1.949.873 1.949 1.943 0 1.076-.869 1.951-1.949 1.951-1.081 0-1.951-.875-1.951-1.951h.015zm12.033 0c0-1.071.869-1.943 1.949-1.943 1.066 0 1.937.873 1.937 1.943 0 1.076-.87 1.951-1.952 1.951-1.079 0-1.949-.875-1.949-1.951h.015zM12.156 5.94c2.161 0 4.111.389 5.822 1.162-.645.018-1.275.131-1.906.36-1.515.555-2.715 1.665-3.375 3.125-.315.66-.48 1.359-.541 2.065-.225-3.076-2.76-5.515-5.881-5.578C7.986 6.34 9.967 5.94 12.112 5.94h.044z"/></svg></a>
						<a href="javascript:void(0);" class="social-twitter"><svg aria-labelledby="simpleicons-twitter-icon" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title id="simpleicons-twitter-icon">Twitter icon</title><path d="M23.954 4.569c-.885.389-1.83.654-2.825.775 1.014-.611 1.794-1.574 2.163-2.723-.951.555-2.005.959-3.127 1.184-.896-.959-2.173-1.559-3.591-1.559-2.717 0-4.92 2.203-4.92 4.917 0 .39.045.765.127 1.124C7.691 8.094 4.066 6.13 1.64 3.161c-.427.722-.666 1.561-.666 2.475 0 1.71.87 3.213 2.188 4.096-.807-.026-1.566-.248-2.228-.616v.061c0 2.385 1.693 4.374 3.946 4.827-.413.111-.849.171-1.296.171-.314 0-.615-.03-.916-.086.631 1.953 2.445 3.377 4.604 3.417-1.68 1.319-3.809 2.105-6.102 2.105-.39 0-.779-.023-1.17-.067 2.189 1.394 4.768 2.209 7.557 2.209 9.054 0 13.999-7.496 13.999-13.986 0-.209 0-.42-.015-.63.961-.689 1.8-1.56 2.46-2.548l-.047-.02z"/></svg></a>
					</div>
				</div>
				
			</div>
			
		</div>
	
	</div>


			
	</body>

</html>
