	<div class="fmn-screen-content-wrap m-reducedWidth-twoThirdsWithSide">
		
		<div class="fmn-form">
					
			<div class="fmn-screen-content-wrap-columns">
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					<div class="fmn-form-option">
						<label>Company Name</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					 <div class="fmn-form-option">
						<label>Company ID (Bulstat)</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Company Accountable Person</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<div class="i-basic-value">
							<label><input type="checkbox"> I don't have a citizen ID (EGN)</label>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Citizen ID (EGN)</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
				</div>
				
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					
					<div class="fmn-form-option">
						<label>City</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option-columns">
						<div class="fmn-form-option-column fmn-twoThirds">
							<div class="fmn-form-option">
								<label>
									Company Address
									<span class="fmn-form-option-details">str, №, ent, fl, apt</span>
								</label>
								<div class="fmn-form-field">
									<textarea></textarea>
								</div>
							</div>
						</div>
						<div class="fmn-form-option-column fmn-oneThird">
							<div class="fmn-form-option">
								<label>Postal Code</label>
								<div class="fmn-form-field">
									<input type="text">
								</div>
							</div>
						</div>
					</div>
					
					
										
				</div>
				
			</div>
			
			
			
		</div><!-- fmn-form -->
		
	</div><!-- fmn-screen-content-wrap -->
	
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Save</button> -->
		<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Save</a>
		
	</div><!-- fmn-actions -->