<!-- <link rel="stylesheet" href="plugins/lightbox/lightbox.css" type="text/css" charset="utf-8" /> -->
<!-- <script src='plugins/lightbox/lightbox-2.6.min.js'></script> -->


<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->
<script src="plugins/select2/select2.min.js"></script>

<script>
/*
	$(document).ready(function() {
	    $('.js-example-basic-single').select2();
	});
*/

// 	$(window).on('resize load', function () { 	
	$(window).on('load', function () { 
	    $('.js-example-basic-single').select2({
		  placeholder: 'Натисни за да добавиш'
		});
	});
	
	$(window).on('resize load scroll', function () { 
		var fmnWh = $(window).height();
		var fmnWs = $(this).scrollTop();
		var fmnDfh = $(".fmn-details-priceAndAddToBasket-wrap").height();
		var fmnDfo = $(".fmn-details-priceAndAddToBasket-wrap").offset().top;
		var fmnWbe = fmnWh + fmnWs;
		var fmnDfbe = fmnDfo + fmnDfh;
		if (fmnWbe < fmnDfbe ) {
			$(".fmn-details-priceAndAddToBasket").addClass("mod-fixPosition");
		} else {
			$(".fmn-details-priceAndAddToBasket").removeClass("mod-fixPosition");
		}
	});

</script>


<div class="fmn-details">
	
	<div class="n-details-left">
		<div class="fmn-details-photo">
			<a class="e-photo" href="images/sample-dish-greek-large.jpg" style="background-image: url(images/sample-dish-caprese.jpg);" data-lightbox="gallery">
				<span class="e-zoom"><span class="e-icon fmn-icon-search"></span></span>
			</a>
		</div><!-- fmn-details-photo -->
		<div class="e-productCode">#ab818973</div>
	</div>
	
	<div class="n-details-right">
	
		<div class="fmn-details-options">

			<div class="fmn-message">
				<span class="e-icon fmn-icon-online-payment"></span> <span class="e-icontext">This item requires advance online payment.</span>
			</div>

			<div class="fmn-message">
				Before you can modify this item and add it to your basket, please <a href="#">specify your order type and location</a>
			</div>
			
			<div class="fmn-details-descriptionIngredientsAllergens">

				<div class="fmn-description">
					A simple Italian salad. It features the colors of the Italian flag: green, white, and red. Usually served as a starter, not a side dish.<br>
				</div>
				
				<div class="fmn-ingredients">
					Ingredients: tomatoes, mozzarella*, basil, oregano, olive oil* and salt<br>
				</div>
		
				<div class="fmn-allergens">
					<span class="allergens-title">Allergens:</span>
					<span class="i-allergen"><img class="e-icon" src="images/allergens/milk.png"><span class="e-caption">Dairy</span></span>
					<span class="i-allergen"><img class="e-icon" src="images/allergens/sulphites.png"><span class="e-caption">Sulphites</span></span>
					<span class="i-allergen"><img class="e-icon" src="images/allergens/fish.png"><span class="e-caption">Fish</span></span>
					<span class="i-allergen"><img class="e-icon" src="images/allergens/soya.png"><span class="e-caption">Soya</span></span>
				</div>										
				
			</div>

			<div class="fmn-form-option m-horizontal m-error">
				<label>Size</label>
				<div class="fmn-form-field">
					<span class="fmn-form-option-error">This is mandatory. Please select one</span>
					<div class="fmn-form-buttonArray">
						<button class="fmn-button m-invert m-twoLine" href="#"><span>Small</span><span class="e-subCaption">€12.45</span></button>
						<button class="fmn-button m-invert m-twoLine" href="#"><span>Medium</span><span class="e-subCaption">€18.45</span></button>
						<button class="fmn-button m-invert m-twoLine" href="#"><span>Large</span><span class="e-subCaption">€24.45</span></button>
					</div>
				</div>
			</div>
			
			<div class="fmn-message m-alert">
				This item is out of stock.
			</div>
			
			<div class="fmn-form-option m-horizontal m-error">
				<label>What do you like?</label>
				<div class="fmn-form-field">
					<span class="fmn-form-option-error">This is mandatory. Please select one</span>
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert" href="#"><span>Gold</span></a>
						<a class="fmn-button m-invert" href="#"><span>Silver</span></a>
						<a class="fmn-button m-invert" href="#"><span>Platinum</span></a>
						<a class="fmn-button m-invert" href="#"><span>Diamonds</span></a>
					</div>
				</div>
			</div>
			
			<div class="fmn-form-option m-horizontal">
				<label>Additions</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert m-twoLine" href="#"><span>Cheese</span><span class="e-subCaption">€2.45</span></a>
						<a class="fmn-button m-invert m-selected m-twoLine" href="#"><span>Ham</span><span class="e-subCaption">€1.45</span></a>
						<a class="fmn-button m-invert m-twoLine" href="#"><span>Coke</span><span class="e-subCaption">€4.45</span></a>
					</div>
					<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span></a>
				</div>
			</div>
			
			<div class="fmn-form-option m-horizontal">
				<label>Drop-down</label>
				<div class="fmn-form-field mod-select">
					<select style="">
						<option>Mitte</option>
						<option selected>Prenzlauer Berg</option>
					</select>
				</div>
			</div>
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Drop-down Prices</label>
				<div class="fmn-form-field mod-select">
					<select style="">
						<option>White Bread +€2.45</option>
						<option selected>Black Bread +€2.45</option>
						<option>Full-grain Bread +€2.45</option>
					</select>
				</div>
			</div>
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Добави още екстри (към пица 24 см)</label>
				<div class="fmn-form-field">
					<select class="js-example-basic-single"  name="states[]" multiple="multiple">
						<option>Замяна на тестото с лимец +1.35 лв</option>
						<option>Замяна на доматен сос със сметана +0.50 лв</option>
						<option>Прошуто крудо +3.75 лв</option>
						<option>Манатарки +2.00 лв</option>
						<option>Шунка френска +1.50 лв</option>
						<option>Песто Дженовезе 🌿 +1.50 лв</option>
						<option>Бекон +1.50 лв</option>
						<option>Пеперони салам 🌶 +1.50 лв</option>
						<option>Луканка +1.50 лв</option>
						<option>Лют зехтин Costa d`Oro 🌶 +1.50 лв</option>
						<option>Чоризо +1.50 лв</option>
						<option>Люти чушки Халапеньо 🌶 +1.50 лв</option>
						<option>Мляно св. & телешко +1.50 лв</option>
						<option>Люти чушки 🌶 +0.94 лв</option>
						<option>Пилешко месо +1.50 лв</option>
						<option>Ананас +1.50 лв</option>
						<option>Филе риба тон (1/2 кутия) +3.75 лв</option>
						<option>Чушки зелени пресни +0.94 лв</option>
						<option>Аншоа 🐟 +3.75 лв</option>
						<option>Домати пресни +0.94 лв</option>
						<option>Моцарела +1.50 лв</option>
						<option>Лук печен +0.94 лв</option>
						<option>Чедър сирене +1.50 лв</option>
						<option>Тиквички +0.94 лв</option>
						<option>Ементал +1.50 лв</option>
						<option>Царевица Bonduelle +0.94 лв</option>
						<option>Пармезан +1.50 лв</option>
						<option>Гъби печурки +0.94 лв</option>
						<option>Синьо сирене +1.50 лв</option>
						<option>Маслини черни +0.94 лв</option>
						<option>Топено сирене LB +1.50 лв</option>
						<option>Кисели краставички +0.94 лв</option>
						
					</select>
				</div>
			</div>
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Radios Two-cols</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray m-twoCol">
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double meat</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cheese</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double tomatoes</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cucumbers</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double snails</span></label>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Checks Two-cols</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray m-twoCol">
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double meat</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double tomatoes</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double snails</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>

					</div>
				</div>
			</div>
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Radios One-col</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray">
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double meat longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cheese longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double tomatoes longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cucumbers longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double snails longer caption</span></label>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Checks One-col</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray">
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double meat longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double tomatoes longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double snails longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>

					</div>
				</div>
			</div>
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Radios Two-cols Boxed</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray m-twoCol mod-boxed">
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double meat</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cheese</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double tomatoes</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cucumbers</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double snails</span></label>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Checks Two-cols Boxed</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray m-twoCol mod-boxed">
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double meat</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double tomatoes</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double snails</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>

					</div>
				</div>
			</div>
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Radios One-col Boxed</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray mod-boxed">
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double meat longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cheese longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double tomatoes longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier" checked=""> <span>Double cucumbers longer caption</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="radio" name="modifier"> <span>Double snails longer caption</span></label>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Checks One-col Boxed</label>
				<div class="fmn-form-field">
					<div class="fmn-form-simpleArray mod-boxed">
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double meat longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cheese longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double tomatoes longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier" checked=""> <span>Double cucumbers longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<div class="i-arrayValue">
							<label><input type="checkbox" name="modifier"> <span>Double snails longer caption</span><span class="e-value-detail">+€2.45</span></label>
						</div>
						<a href="#" class="fmn-form-option-clear"><span class="fmn-icon-delete"></span><span class="e-clear-label">Clear all</span></a>

					</div>
				</div>
			</div>
			
			
			
			<div class="fmn-form-option m-horizontal">
				<label>Disabled</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert m-twoLine m-disabled" href="#"><span>Cheese</span><span class="e-subCaption">€2.45</span></a>
						<a class="fmn-button m-invert m-twoLine m-disabled" href="#"><span>Ham</span><span class="e-subCaption">€1.45</span></a>
						<a class="fmn-button m-invert m-twoLine m-disabled" href="#"><span>Coke</span><span class="e-subCaption">€4.45</span></a>
					</div>
				</div>
			</div>

			<div class="fmn-form-option m-horizontal">
				<label>Mixed With and Without Price</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<button class="fmn-button m-invert m-twoLine" href="#"><span>This</span><span class="e-subCaption">€12.45</span></button>
						<button class="fmn-button m-invert m-twoLine" href="#"><span>The Longer That</span><span class="e-subCaption">€18.45</span></button>
						<button class="fmn-button m-invert m-twoLine" href="#"><span>Short</span><span class="e-subCaption">&nbsp;</span></button>
					</div>
				</div>
			</div>
			
			<div class="fmn-form-option m-horizontal">
				<label>Notes for This Dish</label>
				<div class="fmn-form-field">
					<textarea style="min-height: 60px;" placeholder="e.g. without onions"></textarea>
				</div>
			</div>
			
			<div class="fmn-form-option m-horizontal">
				<label>Quantity</label>
				<div class="fmn-form-field fmn-form-numberBox">
					<a href="#"><span class="fmn-icon-minus"></span></a><input class="" type="text" value="297"><a href="#"><span class="fmn-icon-plus"></span></a>
				</div>
				<span class="fmn-form-option-help">3 available</span>
			</div>

		</div><!-- fmn-details-options -->


		


		<? /*
			<div class="fmn-details-priceAndAddToBasket">
			<div class="priceAndAddToBasket-fixWidth">			
					
			<div class="fmn-details-price">
				<div class="fmn-details-price-main">Box of 4 rolls: <span class="fmn-common-price"><span class="e-original">€10.00</span> <span class="e-special">€8.00</span></span></div>
				<div class="fmn-details-price-save">You save: <span class="fmn-common-price m-secondary"><span class="e-special">€2.00</span></span></div>
			</div>
			
			
			
			<div class="fmn-details-addToBasket">
				
				
				<span class="fmn-labels">
							
					<span class="i-label">
						<span class="fmn-timer">
							<span class="e-icon fmn-icon-timer"></span>
							<span class="e-label">03:27:11</span>
						</span>
					</span>

			 	</span>
				
<!--
	removing this
	in favor of the above tag
	for consistency (the above is the same as in lists)
	and to allow more layout flexibility
	
				<span class="fmn-details-timer fmn-timer">
				 	<span class="e-icon fmn-icon-timer"></span>
				 	<span class="e-label">03:27:11</span>
			 	</span>
-->

	
			 	
				<button type="submit" class="m-large">Add to Basket</button>
			</div>
			</div>
		</div><!-- fmn-details-priceAndAddToBasket -->
			*/ ?>
		
		<div class="fmn-details-priceAndAddToBasket-wrap mod-noTemplate">
		<div class="fmn-details-priceAndAddToBasket">
			<div class="priceAndAddToBasket-fixWidth">
				<div class="priceAndAddToBasket-wrap">
			
					
						<div class="fmn-details-price">
							<div class="fmn-details-price-single">Box of 4 rolls: <span class="fmn-common-price m-secondary"><span class="e-original">€10.00</span> <span class="e-special">€8.00</span></span></div>
							<div class="fmn-details-price-main">Total for 2: <span class="fmn-common-price"><span class="e-original">€20.00</span> <span class="e-special">€16.00</span></span></div>
							<div class="fmn-details-price-save">You save: <span class="fmn-common-price m-secondary"><span class="e-special">€4.00</span></span></div>
						</div>
						
						
						
						<div class="fmn-details-addToBasket">
							
							<span class="fmn-labels">
										
								<span class="i-label">
									<span class="fmn-timer">
										<span class="e-icon fmn-icon-timer"></span>
										<span class="e-label">03:27:11</span>
									</span>
								</span>
			
						 	</span>
							
			<!--
				removing this
				in favor of the above tag
				for consistency (the above is the same as in lists)
				and to allow more layout flexibility
				
							<span class="fmn-details-timer fmn-timer">
							 	<span class="e-icon fmn-icon-timer"></span>
							 	<span class="e-label">03:27:11</span>
						 	</span>
			-->
						 	
							<button type="submit" class="m-large">Add to Basket</button>
						</div>
			
				</div>
			</div>
		</div><!-- fmn-details-priceAndAddToBasket -->
		</div>
		
<? /*		
*/ ?>

											
<!--
		<div class="fmn-section fmn-details-description">
			<h3 class="fmn-section-title">Description</h3>
			<p class="fmn-section-data">
				Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. 
			</p>
		</div>
		
		<div class="fmn-section fmn-details-ingredients">
			<h3 class="fmn-section-title">Ingredients</h3>
			<p class="fmn-section-data">
				Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
			</p>
		</div>
-->
		
<!--
		<div class="fmn-section fmn-details-allergens">
			<h3 class="fmn-section-title">Allergens</h3>
			<p class="fmn-section-data fmn-allergens">
				<span class="i-allergen"><img class="e-icon" src="images/allergens/gluten.png"><span class="e-caption">Gluten</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/crustaceans.png"><span class="e-caption">Crustaceans</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/eggs.png"><span class="e-caption">Eggs</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/fish.png"><span class="e-caption">Fish</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/peanuts.png"><span class="e-caption">Peanuts</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/soya.png"><span class="e-caption">Soya</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/milk.png"><span class="e-caption">Milk</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/nuts.png"><span class="e-caption">Nuts</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/celery.png"><span class="e-caption">Celery</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/mustard.png"><span class="e-caption">Mustard</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/sesame.png"><span class="e-caption">Sesame</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/sulphites.png"><span class="e-caption">Sulphites</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/lupin.png"><span class="e-caption">Lupin</span></span>
				<span class="i-allergen"><img class="e-icon" src="images/allergens/molluscs.png"><span class="e-caption">Molluscs</span></span>
			</p>										
		</div>
-->
		
		<? /*
		<div class="fmn-detailsFixedFooter">
			<div class="fmn-fixWidth">
				<div class="detailsFixedFooter-wrap">
					<div class="detailsFixedFooter-price">
						<span class="price-label">Total for 2:</span>
	<!--
						<div class="price-qty fmn-form-numberBox">
							<a href="#"><span class="fmn-icon-minus"></span></a><span class="qty-number">2</span><a href="#"><span class="fmn-icon-plus"></span></a>
						</div>
	-->
						<span class="price-total fmn-common-price"><span class="e-original">€8.45</span> <span class="e-special">€16.90</span></span>
					</div>
					
					<div class="detailsFixedFooter-addToBasket">
						<button type="submit" class="m-large"><!-- Zur Bestellung Hinzufügen -->Добави</button>
					</div>
				</div>

			</div>
		</div><!-- fmn-details-priceAndAddToBasket -->
		*/ ?>
		
		
	
	</div><!-- fmn-details-infoAndActions -->
	
</div><!-- fmn-details -->