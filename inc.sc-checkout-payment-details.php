<? /*
	$views = "available";
	$allowedViews = array(
    	"core" => array( 
	    	"visibleView" => "core"
    	),
    	"all" => array( 
	    	"visibleView" => "all"
    	)
	);
	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];
*/ ?>




	<div class="fmn-screen-content-wrap m-checkout m-paymentDetails">
		
		<div class="fmn-form">

			<div class="fmn-paymentWrap">

				<script>
					function setPayment(myPayment) {
						$(".debug-paymentType").removeClass("mod-selected");
						$(".debug-paymentType-"+myPayment).addClass("mod-selected");	
					}
				</script>

				<div class="fmn-form-visualRadioGroup">
				
					<div class="vrg-item mod-selected debug-paymentType debug-paymentType-cash" onclick="javascript:setPayment('cash');">
						<div class="itm-iconCaption">
							<div class="itm-icon">
								<svg class="icn-svg" viewBox="0 0 28 28" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
							            <rect x="2.5" y="6.5" width="23" height="14"></rect>
							            <circle cx="14" cy="13.5" r="3"></circle>
							            <circle cx="7" cy="11" r="1"></circle>
							            <circle cx="21" cy="16" r="1"></circle>
							            <path d="M23.5,16.5 C23.5,17.605 22.604,18.5 21.5,18.5 L6.5,18.5 C5.396,18.5 4.5,17.605 4.5,16.5 L4.5,10.5 C4.5,9.396 5.396,8.5 6.5,8.5 L21.5,8.5 C22.604,8.5 23.5,9.396 23.5,10.5 L23.5,16.5 Z"></path>
								    </g>
								</svg>
								
							</div>
							<div class="itm-caption">Pay cash on location</div>
						</div>
					</div>
					<div class="vrg-item debug-paymentType debug-paymentType-card" onclick="javascript:setPayment('card');">
						<div class="itm-iconCaption">	
							<div class="itm-icon">
								<svg class="icn-svg" viewBox="0 0 28 28" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
							            <path d="M15,23.5 L7.5,23.5 C4.738,23.5 2.5,21.262 2.5,18.5 L2.5,15.166 C2.5,9.5 6.5,8.5 8.5,8.5 C9.461,8.5 10.863,8.5 11.5,8.5 C12.472,8.5 13.5,9 13.5,10 L13.5,10.5 C13.5,11.5 12.5,12 11.5,12 L9.5,12 C9.5,12 8.5,15.5 6.5,15.5"></path>
							            <path d="M12.5,17.5 L19,17.5 C19.828,17.5 20.5,18.172 20.5,19 C20.5,19.829 19.828,20.5 19,20.5 L15,20.5"></path>
							            <path d="M12.5,20.5 L15,20.5 C15.828,20.5 16.5,21.172 16.5,22 C16.5,22.829 15.828,23.5 15,23.5 L13.5,23.5"></path>
							            <path d="M25.5,9.5 L13.396,9.5"></path>
							            <path d="M7.5,8.578 L7.5,7 C7.5,6.172 8.172,5.5 9,5.5 L24,5.5 C24.828,5.5 25.5,6.172 25.5,7 L25.5,16 C25.5,16.829 24.828,17.5 24,17.5 L9,17.5 C8.172,17.5 7.5,16.829 7.5,16 L7.5,15.188"></path>
							            <path d="M23.5,11.5 L20.5,11.5"></path>
								    </g>
								</svg>


							</div>
							<div class="itm-caption">Pay by card on location</div>
							
						</div>
						<div class="itm-graphics">
							<span class="gra-item"><img src="images/payment-sumup.png" style="width: 70px; height: 19px;"></span>
							<span class="gra-item mod-card"><img src="images/payment-visa.png"></span>
							<span class="gra-item mod-card"><img src="images/payment-mastercard.png"></span>
							<span class="gra-item mod-card"><img src="images/payment-maestro.png"></span>
						</div>
					</div>
					<div class="vrg-item debug-paymentType debug-paymentType-online" onclick="javascript:setPayment('online');">
						<div class="itm-iconCaption">
							<div class="itm-icon">
								<svg class="icn-svg" viewBox="0 0 28 28" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								    <g>
							            <path d="M11.5,6.5 L24.5,6.5"></path>
							            <path d="M19.5,8.5 L14.5,8.5"></path>
							            <path d="M17.5,10.5 L14.5,10.5"></path>
							            <path d="M14.5,16.5 L14.5,23.5 C14.5,24.604 13.604,25.5 12.5,25.5 L5.5,25.5 C4.396,25.5 3.5,24.604 3.5,23.5 L3.5,8.5 C3.5,7.395 4.396,6.5 5.5,6.5 L8.5,6.5"></path>
							            <path d="M8.5,10.5 L3.5,10.5"></path>
							            <path d="M14.5,21.5 L3.5,21.5"></path>
							            <path d="M7.5,8.5 L8.5,8.5"></path>
							            <path d="M9.5,23.5 C9.5,23.775 9.276,24 9,24 C8.724,24 8.5,23.775 8.5,23.5 C8.5,23.223 8.724,23 9,23 C9.276,23 9.5,23.223 9.5,23.5 Z" id="Shape" stroke="#000000" stroke-linejoin="round"></path>
							            <path d="M24.5,11.5 C24.5,12.604 23.604,13.5 22.5,13.5 L13.5,13.5 C12.396,13.5 11.5,12.604 11.5,11.5 L11.5,5.5 C11.5,4.395 12.396,3.5 13.5,3.5 L22.5,3.5 C23.604,3.5 24.5,4.395 24.5,5.5 L24.5,11.5 Z"></path>
								    </g>
								</svg>


							</div>
							<div class="itm-caption">Pay online</div>
							
						</div>
						<div class="itm-graphics">
							<span class="gra-item"><img src="images/payment-epay.png" style="width: 54px; height: 16px;"></span>
							<span class="gra-item mod-card"><img src="images/payment-visa.png"></span>
							<span class="gra-item mod-card"><img src="images/payment-mastercard.png"></span>
							<span class="gra-item mod-card"><img src="images/payment-maestro.png"></span>
						</div>
					</div>
				
				</div>

				<div class="fmn-content-section m-center">
					<p>
						Your delivery charge will be 3.89 lv.<br>
						<span class="n-small">Select card or online payment for a special lower 2.89 lv delivery charge.</span>
					</p>
				</div>

				<div class="fmn-form-option m-double-space m-center">
					<span class="fm-checkbox">
						<label for="check3">
							<input type="checkbox" name="1" id="check3" checked=""/>
							<span class="fm-checkbox-icon mod-empty"></span>
							<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
							<span class="fm-checkbox-label">Save this payment method as default for my profile</span>
						</label>
					</span>
				</div>
			</div>


			<!--		
			<div class="fmn-form-option">
				<label>Payment Method</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert" href="#"><span>Pay on Location</span></a>
						<a class="fmn-button m-invert m-selected" href="#"><span>ePay</span></a>
						<a class="fmn-button m-invert" href="#"><span>Credit Card</span></a>
					</div>
				</div>
				<div class="fmn-form-option-disclaimer">You’ll be prompted for your card details after order is submitted. Note that payment will appear under Web Technology EOOD</div>
			</div>
											
			<div class="fmn-form-option debug-view debug-view-core">
				<label>Invoice Type</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert m-selected" href="#"><span>None</span></a>
						<a class="fmn-button m-invert" href="#"><span>Individual</span></a>
						<a class="fmn-button m-invert" href="#"><span>Company</span></a>
					</div>
				</div>
			</div>

			<div class="fmn-form-option debug-view debug-view-all">
				<label>Invoice Type</label>
				<div class="fmn-form-field">
					<div class="fmn-form-buttonArray">
						<a class="fmn-button m-invert" href="#"><span>None</span></a>
						<a class="fmn-button m-invert" href="#"><span>Individual</span></a>
						<a class="fmn-button m-invert m-selected" href="#"><span>Company</span></a>
					</div>
				</div>
			</div>

			
			<div class="fmn-form-option">

					<span class="fm-checkbox">
						<label for="check1">
							<input type="checkbox" name="1" id="check1" checked=""/>
							<span class="fm-checkbox-icon mod-empty"></span>
							<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
							<span class="fm-checkbox-label">Save these as my default payment method and invoice type</span>
						</label>
					</span>
				<div class="i-basic-value">					
					<label><input type="checkbox"> Save these as my default payment method and invoice type</label>
				</div>
			</div>
			
			
			<div class="fmn-form-option m-double-space debug-view debug-view-all">
				
				<div class="fm-radios">
					<span class="fm-radio">
						<label for="radio1">
							<input type="radio" name="invoiceDetails" id="radio1" checked=""/>
							<span class="fm-radio-icon mod-empty"></span>
							<span class="fm-radio-icon mod-checked"><i></i></span>
							<span class="fm-radio-label">Use invoice details saved in my profile</span>
						</label>
					</span>
					<span class="fm-radio">
						<label for="radio2">
							<input type="radio" name="invoiceDetails" id="radio2" checked=""/>
							<span class="fm-radio-icon mod-empty"></span>
							<span class="fm-radio-icon mod-checked"><i></i></span>
							<span class="fm-radio-label">Customize invoice details</span>
						</label>
					</span>
				</div>
				<div class="i-basic-value">
					<label><input type="radio"> Use invoice details saved in my profile</label>
				</div>
				<div class="i-basic-value">
					<label><input type="radio" checked=""> Customize invoice details</label>
				</div>
				
			</div>
			
			
			<div class="fmn-screen-content-wrap-columns debug-view debug-view-all">
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					<div class="fmn-form-option">
						<label>Company Name</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					 <div class="fmn-form-option">
						<label>Company ID (Bulstat)</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Company Accountable Person</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>

					<div class="fmn-form-option">
						<span class="fm-checkbox">
							<label for="check2">
								<input type="checkbox" name="1" id="check2" checked=""/>
								<span class="fm-checkbox-icon mod-empty"></span>
								<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
								<span class="fm-checkbox-label">I don't have a citizen ID (EGN)</span>
							</label>
						</span>
						<div class="i-basic-value">
							<label><input type="checkbox"> I don't have a citizen ID (EGN)</label>
						</div>
					</div>
					
					<div class="fmn-form-option">
						<label>Citizen ID (EGN)</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					
				</div>
				
				<div class="fmn-screen-content-wrap-column m-oneHalf">
					
					<div class="fmn-form-option">
						<label>City</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
					
					<div class="fmn-form-option-columns">
						<div class="fmn-form-option-column fmn-twoThirds">
							<div class="fmn-form-option">
								<label>
									Company Address
									<span class="fmn-form-option-details">str, №, ent, fl, apt</span>
								</label>
								<div class="fmn-form-field">
									<textarea></textarea>
								</div>
							</div>
						</div>
						<div class="fmn-form-option-column fmn-oneThird">
							<div class="fmn-form-option">
								<label>Postal Code</label>
								<div class="fmn-form-field">
									<input type="text">
								</div>
							</div>
						</div>
					</div>
					
					
					
					<div class="fmn-form-option m-double-space">
						<span class="fm-checkbox">
							<label for="check3">
								<input type="checkbox" name="1" id="check3" checked=""/>
								<span class="fm-checkbox-icon mod-empty"></span>
								<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
								<span class="fm-checkbox-label">Replace invoice details in profile</span>
							</label>
						</span>
						<div class="i-basic-value">
							<label><input type="checkbox"> Replace invoice details in profile</label>
						</div>
					</div>
					
				</div>
				
			</div>
-->			
			
			
		</div><!-- fmn-form -->
		
	</div><!-- fmn-screen-content-wrap -->
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Next</button> -->
		<a  href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Next: Review & Confirm</a>
		
	</div><!-- fmn-actions -->						





<script>
/*
	$(document).ready(function() {
		visibleView = "<?=$fmn_visibleView?>";
		$(".debug-view").css("display", "none");
		$(".debug-view-"+visibleView).css("display", "block");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+visibleView).addClass("selected");

	});
	
	function setView(myView) {
		$(".debug-view").css("display", "none");
		$(".debug-view-"+myView).css("display", "block");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+myView).addClass("selected");			
	}
*/
</script>


<!--

<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('core');" class="debug-view-link debug-view-link-core">Core <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('all');" class="debug-view-link debug-view-link-all">All <span class="debug-visible">Current</span></a>
</div>

-->