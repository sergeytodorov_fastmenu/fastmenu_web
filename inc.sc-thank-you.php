<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">
	<div class="fmn-content-section m-center">
		<p>Your order was successfully submitted.</p>
		<p>We will contact you with questions if any, otherwise expect your order at the specified time.</p>
		<p>Thanks and bon appétit!</p>
	</div>
</div>

<div class="fmn-actions m-center">
	<a href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Back to Menus</a>
</div><!-- fmn-actions -->
