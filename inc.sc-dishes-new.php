<?
	$dishes = array(
		array(						
			"name" => "Caprese",
			"image" => "images/sample-dish-caprese.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Tomatoes, Mozzarella, Basil, Oregano, Olive Oil, Salt",
			"with-sub-items" => "no",
			"sub-1-in-basket" => "yes",
			"sub-1-in-basket-count" => "3",
			"sub-1-timer" => "yes",
			"sub-1-special-ends" => "03:27:11",
			"hasDetails" => "no"
		),
		array(			
			"name" => "Greek Salad",
			"image" => "images/sample-dish-greek.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Tomatoes, Cucumbers, Onions, Feta Cheese, Olive Oil, Oregano, Chilli, And Also Pita Bread, Mixed Greens, Tomatoes, Radish, Onions, Cucumbers, Olive Oil",
			"with-sub-items" => "no",
			"sub-1-out-of-stock" => "yes"
		),
		array(			
			"name" => "Tabouli",
			"image" => "images/sample-dish-tabouli.jpg",
			"special" => "yes",
			"instock" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Parsley, Onions, Tomatoes, Lemon, Olive Oil, Bulgur, Pepper",
			"in-basket" => "yes",
			"in-basket-count" => "1"
		),

		array(			
			"name" => "Fattoush",
			"image" => "images/sample-dish-fattoush.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Pita Bread, Mixed Greens, Tomatoes, Radish, Onions, Cucumbers, Olive Oil"
		),

		array(			
			"name" => "Tzatziki",
			"image" => "images/sample-dish-tzatziki.jpg",
			"special" => "yes",
			"timer" => "no",			
			"special-ends" => "03:27:11",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Greek Yogurt, Cucumbers, Olive Oil"
		),
		array(			
			"name" => "Italian Salad",
			"image" => "images/sample-dish-italian.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Tomatoes, Cucumbers, Onions, Feta Cheese, Olive Oil, Oregano, Chilli"
		),
		array(			
			"name" => "Russian Salad",
			"image" => "images/sample-dish-russian.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Potatoes, Mayonnaise, Ham, Green Peas",
			"with-sub-items" => "no",
			"sub-1-timer" => "yes",
			"sub-1-special-ends" => "03:27:11",
			"pay-online" => "no"
		),
		array(		
			"name" => "Caesar Salad",
			"image" => "images/sample-dish-caesar.jpg",
			"special" => "no",
			"price" => "€8.45",
			"ingredients" => "Romaine Lettuce, Croutons, Cheese*, Olive Oil, Lemon Juice, Egg*, Worchestershire Sauce*",
			"instock" => "yes",
			"timer" => "no",			
			"special-ends" => "03:27:11",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"in-basket" => "yes",
			"in-basket-count" => "2",
			"with-sub-items" => "no",
			"pay-online" => "no"
		),
		array(			
			"name" => "Green Salad",
			"image" => "images/sample-dish-green.jpg",
			"special" => "yes",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Green Salad, Onions, Cucumbers, Egg, Olive Oil"
		),
		array(			
			"name" => "Soba Noodle Salad",
			"image" => "images/sample-dish-soba.jpg",
			"special" => "yes",
			"timer" => "no",	
			"special-ends" => "3 days",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Soba Noodles, Cilantro, Bell Peppers, Bean Sprouts, Grapeseed Oil, Soy Sauce"
		),
		array(			
			"name" => "Egg Salad",
			"image" => "images/sample-dish-egg.jpg",
			"special" => "yes",
			"timer" => "no",	
			"special-ends" => "01:17:32",
			"price-original" => "€8.45",
			"price-special" => "€6.45",
			"ingredients" => "Eggs, Mayonnaise, Dijon Mustard, Paprika, Onions, Salt, Pepper",
			"pay-online" => "no"
		)
	);
?>




	<div class="fmn-productsListPhoto">
			
		<? for ($i = 0; $i < count($dishes); $i++) { ?>
				
					<? if ($i == 0) { ?>
						<div class="productsListPhoto-separator">
							<span class="separator-caption">Mediterranean Salads</span>
						</div>
					<? } ?>
					
					<? if ($i == 3) { ?>
						<div class="productsListPhoto-separator">
							<span class="separator-caption">Mediterranean Salads</span>
						</div>
					<? } ?>

					<a href="<?=$fmn_next_page_url?>" class="i-productsListPhoto-line">
			
						<? if ($dishes[$i]["image"] != null ) { ?>
							<span class="line-photo"><span class="photo-image" style="background-image: url(<?=$dishes[$i]["image"]?>);"></span></span>
						<? } else { } /* end if */ ?>
						
						<span class="line-details">
						
							<span class="details-name"><?=$dishes[$i]["name"]?></span>
							<span class="details-ingredients"><?=$dishes[$i]["ingredients"]?></span>								

						</span>

						<span class="line-priceBuy">
		
							<span class="priceBuy-price">
								<? if ($dishes[$i]["special"] == "no") { ?>
									<span class="fmn-common-price">
										<span class="e-normal"><?=$dishes[$i]["price"]?></span>
									</span>
								<? } else if ($dishes[$i]["special"] == "yes") { ?>
									<span class="fmn-common-price">
										<span class="e-original"><?=$dishes[$i]["price-original"]?></span>
										<span class="e-special"><?=$dishes[$i]["price-special"]?></span>
									</span>
								<? } /* end if */ ?>
							</span>
				
								
								
								<? if ($dishes[$i]["in-basket"] == "yes") { ?>
									<span class="priceBuy-inBasket">
										<span class="inBasket-icon fmn-icon-in-basket"></span><span class="inBasket-caption"><?=$dishes[$i]["in-basket-count"]?></span>
									</span>
								<? } else { ?> 
									<span class="priceBuy-buy">
										<span class="buy-icon fmn-icon-add-to-basket"></span><!-- <span class="buy-caption">Buy</span> -->
									</span>
								<? } /* end if */ ?>
																
								

						
						</span>
						
					</a>

		<? } /* end for */ ?>  	
		
	</div><!-- fmn-productsListLargePhoto -->









