<?
	$views = "available";
	
	$allowedViews = array(
    	"core" => array( 
	    	"visibleView" => "core"
    	),
    	"all" => array( 
	    	"visibleView" => "all"
    	)
	);

	$fmn_visibleView = $allowedViews[$_GET["view"]]["visibleView"];
	
?>
 
<script>

	$(document).ready(function() {
		visibleView = "<?=$fmn_visibleView?>";
		defaultView = "core";
		if (visibleView == "") {
			$(".debug-view").addClass("debug-view-hidden");
			$(".debug-view-"+defaultView).removeClass("debug-view-hidden");	
			$(".debug-view-link").removeClass("selected");
			$(".debug-view-link-"+defaultView).addClass("selected");
		} else {
			$(".debug-view").addClass("debug-view-hidden");
			$(".debug-view-"+visibleView).removeClass("debug-view-hidden");	
			$(".debug-view-link").removeClass("selected");
			$(".debug-view-link-"+visibleView).addClass("selected");

		}
	});
	
	function setView(myView) {
		$(".debug-view").addClass("debug-view-hidden");
		$(".debug-view-"+myView).removeClass("debug-view-hidden");	
		$(".debug-view-link").removeClass("selected");
		$(".debug-view-link-"+myView).addClass("selected");			
	}

</script>

<style>
	.debug-view-hidden {
		display: none !important;
	}
</style>

<div class="debug debug-overlay" id="debug-views">
	<a href="javascript:setView('core');" class="debug-view-link debug-view-link-core">Core Elements <span class="debug-visible">Current</span></a>
	<a href="javascript:setView('all');" class="debug-view-link debug-view-link-all">All Elements <span class="debug-visible">Current</span></a>
</div>









  


<div class="fmn-basket">

<!--
	<div class="fmn-form-option">
		<div class="fmn-orderTypeLocation">
			
			<span class="n-location">
				<span class="n-icon fmn-icon-store"></span>
				<span class="n-caption">
					<b>Take-away</b> from <b>Flying Lotus</b> 
				</span>
				<a class="n-link" href="">Change</a>
			</span>
			
			<span class="n-date m-highlight">
				<span class="n-icon fmn-icon-calendar-check"></span>
				<span class="n-caption">
					for <b>Monday</b>, April 11
				</span>
				<a class="n-link" href="">See Details</a>
			</span>
			
		</div>
	</div>
-->


	<div class="fmn-list">
		
		<div class="i-list-line m-header">
			<span class="i-list-line-cell m-label"></span>
			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox">
					<label for="check0">
						<input type="checkbox" name="1" id="check0"/>
						<span class="fm-checkbox-icon mod-empty"></span>
						<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
					</label>
				</span>
			</span>
			<span class="i-list-line-wrap m-photoName">
				<span class="i-list-line-cell m-photo">photo</span>
				<span class="i-list-line-cell m-name">name and details</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price">price</span>
				<span class="i-list-line-cell m-qty">qty</span>
				<span class="i-list-line-cell m-lineTotal">total</span>
			</span>
			<span class="i-list-line-cell m-actions"></span>
		</div>
		
		<div class="i-list-line m-row m-justAdded">
			
			<span class="fmn-labels">
			 	<span class="i-label">
				 	<span class="fmn-justAdded">
					 	<span class="e-label">Just added!</span>
				 	</span>
			 	</span>

		 	</span>
		 	
		 	
<!--
			<span class="i-list-line-cell m-label">
				<span class="i-list-line-cell-label m-justAdded">Just Added!</span>
			</span>
-->
			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox"><label for="check1"><input type="checkbox" name="1" id="check1"/><span class="fm-checkbox-icon mod-empty"></span><span class="fm-checkbox-icon mod-checked"><i></i><i></i></span></label></span>
			</span>
			<span class="i-list-line-wrap m-photoName">
				<span class="i-list-line-cell m-photo"><a href="dynamic.php?page=details"><img src="images/sample-dish-green.jpg"></a></span>
				<span class="i-list-line-cell m-name">
					<a href="dynamic.php?page=details">Green Salad with a Convoluted Long Name</a>
 					<span class="e-detail"><!-- <span class="e-dataLabel">details:</span> -->(no onions, but otherwise super long details)</span>
 				</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€8338.45</span> <span class="e-special">€4481.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
				<span class="i-list-line-cell m-qty">
					<a href="#"><span class="fmn-icon-minus"></span></a><!-- <input class="m-small" type="text" value="2"> --><span class="qty-number">22</span><a href="#"><span class="fmn-icon-plus"></span></a>
				</span>
				<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span><span class="fmn-common-price"><span class="e-normal">€3163.90</span></span></span>
			</span>
			<span class="i-list-line-cell m-actions">
				<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
			</span>
		</div>
		
		<div class="i-list-line m-row m-noLongerAvailable debug-view debug-view-all">
			
			<span class="fmn-labels">
			 	<span class="i-label">
				 	<span class="fmn-noLongerAvailable">
					 	<span class="e-label">This item is no longer available.</span>
				 	</span>
			 	</span>
		 	</span>
<!--
			<span class="i-list-line-cell m-label">
				<span class="i-list-line-cell-label m-noLongerAvailable">This item is no longer available.</span>
			</span>
-->
			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox"><label for="check2"><input type="checkbox" name="1" id="check2"/><span class="fm-checkbox-icon mod-empty"></span><span class="fm-checkbox-icon mod-checked"><i></i><i></i></span></label></span>
			</span>

			<span class="i-list-line-wrap m-photoName noPhoto">
				<span class="i-list-line-cell m-name">
					<a href="dynamic.php?page=details">Pizza Margheritta</a>
					<span class="e-detail"><!-- <span class="e-dataLabel">details:</span> -->(small + garlic sauce)</span>
				</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€18.45</span> <span class="e-special">€11.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
				<span class="i-list-line-cell m-qty">
					<!-- <input class="m-small" type="text" value="2"> --><span class="qty-number">2</span>
				</span>
				<span class="i-list-line-cell m-lineTotal">—</span>
			</span>
			<span class="i-list-line-cell m-actions">
				<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
			</span>
		</div>
		
		<div class="i-list-line m-row">
			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox"><label for="check3"><input type="checkbox" name="1" id="check3"/><span class="fm-checkbox-icon mod-empty"></span><span class="fm-checkbox-icon mod-checked"><i></i><i></i></span></label></span>
			</span>
			<span class="i-list-line-wrap m-photoName">
				<span class="i-list-line-cell m-photo"><a href="dynamic.php?page=details"><img src="images/sample-category-tile-veal.jpg"></a></span>
				<span class="i-list-line-cell m-name">
					<a href="dynamic.php?page=details">Veal Pepper Steak</a>
					<span class="e-detail"><!-- <span class="e-dataLabel">details:</span> -->(well done)</span>
				</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
				<span class="i-list-line-cell m-qty">
					<a href="#"><span class="fmn-icon-minus"></span></a><!-- <input class="m-small" type="text" value="2"> --><span class="qty-number">2</span><a href="#"><span class="fmn-icon-plus"></span></a>
					<span class="e-max debug-view debug-view-all">3 available</span>
				</span>
				<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span><span class="fmn-common-price"><span class="e-normal">€23.90</span></span></span>
			</span>
			<span class="i-list-line-cell m-actions">
				<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
			</span>
		</div>
		
		<div class="i-list-line m-row">
			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox"><label for="check4"><input type="checkbox" name="1" id="check4" checked=""/><span class="fm-checkbox-icon mod-empty"></span><span class="fm-checkbox-icon mod-checked"><i></i><i></i></span></label></span>
			</span>
			<span class="i-list-line-wrap m-photoName noPhoto">
				<span class="i-list-line-cell m-name">
					<a href="dynamic.php?page=details">Veal Pepper Steak</a>
					<span class="e-detail"><!-- <span class="e-dataLabel">details:</span>  -->(medium rare)</span>
				</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
				<span class="i-list-line-cell m-qty">
					<a href="#"><span class="fmn-icon-minus"></span></a><!-- <input class="m-small" type="text" value="2"> --><span class="qty-number">2</span><a href="#"><span class="fmn-icon-plus"></span></a>
					<span class="e-max debug-view debug-view-all">3 available</span>
				</span>
				<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span><span class="fmn-common-price"><span class="e-normal">€23.90</span></span></span>
			</span>
			<span class="i-list-line-cell m-actions">
				<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
			</span>
		</div>
		
		<div class="i-list-line m-row">
			
			<span class="fmn-labels debug-view debug-view-all">
			 	<span class="i-label">
				 	<span class="fmn-payOnline">
						<span class="e-icon fmn-icon-online-payment"></span>
					 	<span class="e-label">Pay Online</span>
				 	</span>
			 	</span>
		 	
		 	</span>



			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox"><label for="check5"><input type="checkbox" name="1" id="check5"/><span class="fm-checkbox-icon mod-empty"></span><span class="fm-checkbox-icon mod-checked"><i></i><i></i></span></label></span>
			</span>
			<span class="i-list-line-wrap m-photoName">
				<span class="i-list-line-cell m-photo"><a href="dynamic.php?page=details"><img src="images/sample-category-tile-fish.jpg"></a></span>
				<span class="i-list-line-cell m-name">
					<a href="dynamic.php?page=details">Cod Fillet</a>
					<span class="e-detail"></span>
				</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-normal">€18.45</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
				<span class="i-list-line-cell m-qty">
					<a href="#"><span class="fmn-icon-minus"></span></a><!-- <input class="m-small" type="text" value="2"> --><span class="qty-number">2</span><a href="#"><span class="fmn-icon-plus"></span></a>
				</span>
				<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span><span class="fmn-common-price"><span class="e-normal">€23.90</span></span></span>
			</span>
			<span class="i-list-line-cell m-actions">
				<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
			</span>
		</div>
		
		<div class="i-list-line m-row m-adjustedQuantity debug-view debug-view-all">
			
			<span class="fmn-labels">
			 	<span class="i-label">
				 	<span class="fmn-adjustedQuantity">
					 	<span class="e-label">Item Quantity Adjusted</span>
				 	</span>
			 	</span>
		 	</span>

<!--
			<span class="i-list-line-cell m-label">
				<span class="i-list-line-cell-label m-adjustedQuantity">Item Quantity Adjusted</span><span class="i-list-line-cell-label m-requiresAdvancePayment"><span class="e-icon fmn-icon-online-payment"></span><span>Pay Online</span></span>
			</span>
-->
			<span class="i-list-line-cell m-checkbox">
				<span class="fm-checkbox"><label for="check6"><input type="checkbox" name="1" id="check6"/><span class="fm-checkbox-icon mod-empty"></span><span class="fm-checkbox-icon mod-checked"><i></i><i></i></span></label></span>
			</span>
			<span class="i-list-line-wrap m-photoName">
				<span class="i-list-line-cell m-photo"><a href="dynamic.php?page=details"><img src="images/sample-dish-green.jpg"></a></span>
				<span class="i-list-line-cell m-name">
					<a href="dynamic.php?page=details">Green Salad with a Convoluted Long Name</a>
					<span class="e-detail"><!-- <span class="e-dataLabel">details:</span>  -->(no onions, but otherwise super long details)</span>
				</span>
			</span>
			<span class="i-list-line-wrap m-priceQtyTotal">
				<span class="i-list-line-cell m-price"><span class="fmn-common-price"><span class="e-original">€2318.45</span> <span class="e-special">€3311.95</span></span><span class="e-mobileLabel">&nbsp; ×</span></span>
				<span class="i-list-line-cell m-qty">
					<a href="#"><span class="fmn-icon-minus"></span></a><!-- <!-- <input class="m-small" type="text" value="2"> --><span class="qty-number">2</span><a href="#"><span class="fmn-icon-plus"></span></a>
				</span>
				<span class="i-list-line-cell m-lineTotal"><span class="e-mobileLabel">=&nbsp;</span><span class="fmn-common-price"><span class="e-normal">€5423.90</span></span></span>
			</span>
			<span class="i-list-line-cell m-actions">
				<a href="#" class="e-action-delete"><span class="fmn-icon-delete"></span></a>
			</span>
		</div>
		
		
	</div><!-- fmn-list -->
	



	<div class="fmn-basket-footer">
		
		<div class="fmn-basket-footer-secondaryActions">
<!-- 			<a href="#" class="tap fmn-button m-small m-invert m-destructive debug-view debug-view-all">Delete Selected</a> -->
			
			<div class="fmn-basket-promoCode">
				
				<div class="fmn-form-option">
					<label>Promo Code</label>
					<div class="fmn-form-field">
						<input type="text" style="width: 160px;"><a href="#" class="fmn-button">Apply</a>
					</div>
				</div>
				
				<div class="fmn-form-option debug-view debug-view-all">
					<label>Promo Code</label>
					<div class="fmn-form-field">
						<input type="text" style="width: 160px;" value="VALIDPROMO" disabled><a href="#" class="promoCode-clear m-destructive">Clear</a>
					</div>
				</div>
				
			</div>
			
		</div>
		
		<div class="fmn-basket-footer-data">
			
			<div class="i-basket-footer-wrap m-basket">
				<div class="i-basket-footer-line m-basket">
					<span class="i-basket-footer-line-cell m-label">
						Basket
						
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€40.00</span></span></span>
				</div>
				<div class="i-basket-footer-line m-discount debug-view debug-view-all">
					<span class="i-basket-footer-line-cell m-label">
						XX% discount for XXXXXX orders above €XX.XX*
						<span class="e-note">
							<span class="e-special">You are just €XX.XX away from a XX% discount*.</span> <a href="#">See details</a>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€6.00</span></span></span>
				</div>
<!--
				<div class="i-basket-footer-line subtotal">
					<span class="i-basket-footer-line-cell m-label">Sub-total</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€40.00</span></span></span>
				</div>
-->
			</div>
			
			
			
			<? /*
				
				 ignore these for now
				 we may need them later, but not now
				 
				 
			
			<div class="i-basket-footer-wrap basket">
				<div class="i-basket-footer-line basket">
					<span class="i-basket-footer-line-cell m-label">
						Basket
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€40.00</span></span></span>
				</div>
				<div class="i-basket-footer-line discount">
					<span class="i-basket-footer-line-cell m-label">
						<span class="e-note">
							<span class="e-special">You are just €10.00 away from a 10% discount*.</span> <a href="#">See details</a>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"></span>
				</div>
				<div class="i-basket-footer-line subtotal">
					<span class="i-basket-footer-line-cell m-label">Sub-total</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€40.00</span></span></span>
				</div>
			</div>
			
			
			<div class="i-basket-footer-wrap basket">
				<div class="i-basket-footer-line basket">
					<span class="i-basket-footer-line-cell m-label">
						Basket
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€140.00</span></span></span>
				</div>
				<div class="i-basket-footer-line discount">
					<span class="i-basket-footer-line-cell m-label">
						<span class="e-note">
							<span class="e-special">You are just €50.00 away from a 10% discount*.</span> <a href="#">See details</a>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"></span>
				</div>
				<div class="i-basket-footer-line subtotal">
					<span class="i-basket-footer-line-cell m-label">Sub-total</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€140.00</span></span></span>
				</div>
			</div>
			
			
			
						
			
			
			<div class="i-basket-footer-wrap basket">
				<div class="i-basket-footer-line basket">
					<span class="i-basket-footer-line-cell m-label">
						Basket
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€60.00</span></span></span>
				</div>
				<div class="i-basket-footer-line discount">
					<span class="i-basket-footer-line-cell m-label">
						10% discount for Delivery orders above €50.00*<br>
						<span class="e-note">
							<span class="e-special">You are just €40.00 away from a 20% discount*.</span> <a href="#">See details</a>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€6.00</span></span></span>
				</div>
				<div class="i-basket-footer-line subtotal">
					<span class="i-basket-footer-line-cell m-label">Sub-total</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€54.00</span></span></span>
				</div>
			</div>


			<div class="i-basket-footer-wrap basket">
				<div class="i-basket-footer-line basket">
					<span class="i-basket-footer-line-cell m-label">
						Basket
<!--
						<span class="e-note">
							<span class="e-special">You are just XX.XX lv away from a XX% discount*.</span> <a href="#">See details</a>
						</span>
-->
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€120.00</span></span></span>
				</div>
				<div class="i-basket-footer-line discount">
					<span class="i-basket-footer-line-cell m-label">
						20% discount for Delivery orders above €100.00*<br>
						<span class="e-note">
							<a href="#">See details</a>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€24.00</span></span></span>
				</div>
				<div class="i-basket-footer-line subtotal">
					<span class="i-basket-footer-line-cell m-label">Sub-total</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€96.00</span></span></span>
				</div>
			</div>
			
			*/ ?>
			
			
			
			
			
			<div class="i-basket-footer-wrap m-delivery">
				<div class="i-basket-footer-line m-delivery">
					<span class="i-basket-footer-line-cell m-label">
						Delivery to XXXXXX for orders above €XX.XX<br>
						<span class="e-note">
							<span class="e-special">Your are just €XX.XX away from a special €XX.XX delivery fee.</span> <a href="#">See details</a>
							<? /* I don't think this is needed anymore
								<a href="#">Specify Another Location</a>
							*/ ?>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">€2.00</span></span></span>
				</div>
			</div>
			<div class="i-basket-footer-wrap m-promoCode debug-view debug-view-all">
				<div class="i-basket-footer-line m-promoCode">
					<span class="i-basket-footer-line-cell m-label">Promo Code VALIDPROMO</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-special">–€4.00</span></span></span>
				</div>
			</div>
			<div class="i-basket-footer-wrap m-total">
				<div class="i-basket-footer-line m-total">
					<span class="i-basket-footer-line-cell m-label">
						Total
						<span class="e-note">
							<span class="e-special">You save €32.00*</span>
						</span>
					</span>
					<span class="i-basket-footer-line-cell m-value"><span class="fmn-common-price"><span class="e-normal">€38.00</span></span></span>
				</div>
			</div>
			<div class="i-basket-footer-wrap m-note">
				<div class="i-basket-footer-line m-note">
					<span class="i-basket-footer-line-span">
						* Items which are already discounted are not further discounted by order total promos
					</span>
				</div>
				
			</div>

		
		</div>
		
	</div><!-- fmn-basket-footer -->
	
	<div class="fmn-actions m-right">

		<div class="n-actions-alert-wrap debug-view debug-view-all">
			<div class="n-actions-alert">Your basket is less than the €40.00 minimum for delivery orders.</div>
		</div>
		<div class="n-actions-alert-wrap debug-view debug-view-all">
			<div class="n-actions-note">Your basket contains items which require advance payment. You will be prompted to pay online.</div>
		</div>

		
		<a href="dynamic.php?page=menus">Continue Shopping</a>
<!-- 		<button class="m-large" value="Checkout">Checkout</button> -->
			<a href="dynamic.php?page=checkout-complete-address" class="fmn-button m-large disabled" value="Checkout">Checkout</a>
<!-- 		<input type="submit" class="m-large" value="Checkout" disabled=""> -->
		
	</div><!-- fmn-actions -->
	

</div><!-- fmn-basket -->