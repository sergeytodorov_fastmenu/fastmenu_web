<?

	$otl = "available";
		
	$allowedOtl = array(
    	"notSet" => array( 
	    	"visibleOtl" => "notSet"
    	),
    	"deliveryWithDate" => array( 
	    	"visibleOtl" => "deliveryWithDate"
    	),
    	"deliveryNoDate" => array( 
	    	"visibleOtl" => "deliveryNoDate"
    	),
    	"takeAway" => array( 
	    	"visibleOtl" => "takeAway"
    	),
    	"notAvailable" => array( 
	    	"visibleOtl" => "notAvailable"
    	)

	);

	$fmn_visibleOtl = $allowedOtl[$_GET["otl"]]["visibleOtl"];
	
?>

<div class="debug debug-overlay" id="debug-otl">
	<a href="javascript:setOtl('notSet');" class="debug-otl-link debug-otl-link-notSet">Not Set <span class="debug-visible">Current</span></a>
	<a href="javascript:setOtl('deliveryNoDate');" class="debug-otl-link debug-otl-link-deliveryNoDate">Delivery No Date <span class="debug-visible">Current</span></a>
	<a href="javascript:setOtl('deliveryWithDate');" class="debug-otl-link debug-otl-link-deliveryWithDate">Delivery With Date <span class="debug-visible">Current</span></a>
	<a href="javascript:setOtl('takeAway');" class="debug-otl-link debug-otl-link-takeAway">Take Away <span class="debug-visible">Current</span></a>
	<a href="javascript:setOtl('notAvailable');" class="debug-otl-link debug-otl-link-notAvailable">Not Available <span class="debug-visible">Current</span></a>
</div>



<style>
	#fmn-root #otl-notSet,
	#fmn-root #otl-deliveryNoDate,
	#fmn-root #otl-deliveryWithDate,
	#fmn-root #otl-takeAway,
	#fmn-root #otl-notAvailable {
/* 		display: none; */
	}
	.debug-otl-hidden {
		display: none !important;
	}
</style>

<script>
	$(document).ready(function() {
		visibleOtl = "<?=$fmn_visibleOtl?>";
		$(".debug-otl").addClass('debug-otl-hidden');
		if (visibleOtl == "") {
			$("#otl-notSet").removeClass('debug-otl-hidden');
			$(".debug-otl-link-notSet").addClass("selected");
		} else {
			$("#otl-"+visibleOtl).removeClass('debug-otl-hidden');
			$(".debug-otl-link-"+visibleOtl).addClass("selected");
		}
	});	
	
	function setOtl(myOtl) {
		$(".debug-otl").addClass("debug-otl-hidden");
		$("#otl-"+myOtl).removeClass("debug-otl-hidden");
		$(".debug-otl-link").removeClass("selected");
		$(".debug-otl-link-"+myOtl).addClass("selected");
	}
</script>






<!--
<div class="fmn-fixed-header m-smart-scroll">
	<div class="fmn-fixWidth">
		<div class="fmn-fixed-header-left" id="fmnid-app-location-scrolled"></div>
		<div class="fmn-fixed-header-right" id="fmnid-toolbar-scrolled"></div>
	</div>
</div>
-->







<? if ($fmn_page_type != "checkout") { ?>


<div class="fmn-header mod-noTemplate">

	<div class="fmn-fixedHeaderMat">

		<div class="fmn-header-logo">
			<img src="images/sample-logo-pizzaBox.png" />
		</div>
	
		
		<div class="fmn-toolbar-wrap">
			<div class="fmn-toolbar-not-scrolled" id="fmnid-toolbar-not-scrolled"></div>
			<div class="fmn-toolbar" id="fmnid-toolbar">		
			
				<? if ($fmn_screen_title == "Login" or $fmn_screen_title == "Register") { ?>
	
					<a href="dynamic.php?page=login" class="i-toolbarItem m-login"><span class="e-icon fmn-icon-login"></span><span class="e-label">Login</span></a>
	
				<? } else { ?>
	
					<a href="dynamic.php?page=account-main" class="i-toolbarItem m-account"><span class="e-icon fmn-icon-profile"></span><span class="e-label">Account</span></a>
	
				<? } ?>
	
	<!-- 			<a href="#" class="i-toolbarItem m-search"><span class="e-icon fmn-icon-search"></span><span class="e-label">Search</span></a> -->
	
	<!--
				<a href="dynamic.php?page=basket" class="i-toolbarItem m-basket">
					<span class="e-icon fmn-icon-basket"></span><span class="e-label">Basket</span>
					<span class="e-count">37</span>
					<? if ($fmn_screen_title == "Caprese Salad with Long Title to Illustrate How Things Work") { ?> 
						<span class="e-callout"><span class="e-text">2 × Pizza Margheritta added</span></span>
					<? } ?>
				</a>
	-->
	
				<div class="i-toolbarItem m-basket">
					<a href="dynamic.php?page=basket">
						<span class="e-icon fmn-icon-basket"></span><span class="e-label">Basket</span>
						<span class="e-count">37</span>
						<? if ($fmn_screen_title == "Caprese Salad with Long Title to Illustrate How Things Work") { ?> 
							<span class="e-callout"><span class="e-text">2 × Pizza Margheritta added</span></span>
						<? } ?>
						
					</a>
	
					<div class="fmn-quickBasket">
						
						<div class="quickBasket-line">
							<span class="line-qty">2</span>
							<span class="line-times">×</span>
							<span class="line-name">
								Green Salad with a Convoluted Long Name
								<span class="name-detail">(no onions, but otherwise super long details)</span>
							</span>
							<span class="line-price fmn-common-price">
								<span class="e-original">€2318.45</span>
								<span class="e-special">€3311.95</span>
							</span>
						</div>
						<div class="quickBasket-line">
							<span class="line-qty">2</span>
							<span class="line-times">×</span>
							<span class="line-name">
								Veal Pepper Steak
								<span class="name-detail">(well done)</span>
							</span>
							<span class="line-price fmn-common-price">
								<span class="e-normal">€18.45</span>
							</span>
						</div>
						
						<div class="quickBasket-line">
							<span class="line-qty">2</span>
							<span class="line-times">×</span> 
							<span class="line-name">
								Veal Pepper Steak
								<span class="name-detail">(medium rare)</span>
							</span>
							<span class="line-price fmn-common-price">
								<span class="e-normal">€18.45</span>
							</span>
						</div>
						<div class="quickBasket-line">
							<span class="line-qty">20</span>
							<span class="line-times">×</span>
							<span class="line-name">				
								Cod Fillet
							</span>
							<span class="line-price fmn-common-price">
								<span class="e-normal">€18.45</span>
							</span>  
						</div>
						<div class="quickBasket-line">
							<span class="line-qty">2</span>
							<span class="line-times">×</span> 
							<span class="line-name">
								Green Salad with a Convoluted Long Name
								<span class="name-detail">(no onions, but otherwise super long details)</span>
							</span>
							<span class="line-price fmn-common-price">
								<span class="e-normal">€2318.45</span>
							</span>
						</div>
						
						<div class="quickBasket-total">
							<span class="total-label">Basket Total</span>
							<span class="total-amount fmn-common-price">
								<span class="e-normal">€19388.45</span>
							</span> 					
						</div>
						
						<a href="dynamic.php?page=basket" class="fmn-button">Continue</a>
						
						
						
					</div>
					
					
				</div>
	
				
	
	
			</div>
			
			
			
		</div>
	</div>



<!-- 	<div class="fmn-header-left"> -->

		<?
// 			if ($fmn_show_brand == "yes") { } else {
		?>
<!--
			<div class="fmn-merchant">
				Flying Lotus
 			</div>
-->
		<?				
// 			}
		?>


		

		<? if ($fmn_page_type == "start") { ?>

		<? /*
		<? } else if ($fmn_page_type == "checkout") { ?>

			<div class="fmn-breadcrumbs">
				<a href="dynamic.php?page=basket" class="i-breadcrumb m-link">Back to Basket</a>
			</div>
		*/ ?>						

		<? /*
		<? } else if ($fmn_page_type == "basket") { ?>

			<div class="fmn-breadcrumbs">
				<a href="dynamic.php?page=menus" class="i-breadcrumb m-link">Back to Menu</a>
			</div>
		*/ ?>

		<? } else if ($fmn_page_type == "account") { ?>

			<div class="fmn-accountBack">
				<a href="dynamic.php?page=categories">Back to Ordering</a>
			</div>

		<? } else { ?>
		
					<div class="debug-otl" id="otl-deliveryNoDate">
						<div class="fmn-orderTypeLocation mod-delivery">
							<div class="orderTypeLocation-wrap">
								<span class="n-location">
									<span class="n-icon fmn-icon-delivery"></span>
									<span class="n-caption">
										<span class="caption-primary">Delivery</span><span class="caption-bullet"></span><span class="caption-secondary">11, Ivan Vazov Str.</span>
									</span>
									<a class="n-link fmn-button m-invert m-smaller" href="dynamic.php?page=otlDeliveryLoggedIn">Edit</a>
								</span>
							</div>
						</div>
					</div>

					<div class="debug-otl" id="otl-deliveryWithDate">
						<div class="fmn-orderTypeLocation mod-delivery mod-withDate">
							<div class="orderTypeLocation-wrap">
								<span class="n-location">
									<span class="n-icon fmn-icon-delivery"></span>
									<span class="n-caption">
										<span class="caption-primary">Delivery</span><span class="caption-bullet"></span><span class="caption-secondary">11, Ivan Vazov Str. Very Long Caption Like Reaaaaaaaly Long Caption Here</span>
									</span>
									<a class="n-link fmn-button m-invert m-smaller" href="dynamic.php?page=otlDeliveryLoggedIn">Edit</a>
								</span>
								<span class="n-date">
									<span class="n-icon fmn-icon-calendar-check"></span>
									<span class="n-caption">
										<span class="caption-primary">Sept 20</span>
									</span>
								</span>
							</div>
						</div>
					</div>


					<div class="debug-otl" id="otl-takeAway">
						<div class="fmn-orderTypeLocation mod-takeAway">
							<div class="orderTypeLocation-wrap">
								<span class="n-location">
									<span class="n-icon fmn-icon-take-away"></span>
									<span class="n-caption">
										<span class="caption-primary">Take Away</span><span class="caption-bullet"></span><span class="caption-secondary">Flying Lotus</span>
									</span>
									<a class="n-link fmn-button m-invert m-smaller" href="dynamic.php?page=otlTakeAway">Edit</a>
								</span>
							</div>
						</div>
					</div>

					<div class="debug-otl" id="otl-notSet">
						<div class="fmn-orderTypeLocation mod-notSet">
							<div class="orderTypeLocation-wrap">
								<span class="n-location">
									<span class="n-icon fmn-icon-store"></span>
									<span class="n-caption">
										<span class="caption-primary">Select order type</span>
									</span>
									<a class="n-link fmn-button m-invert m-smaller" href="dynamic.php?page=otlEmpty">Select</a>
								</span>
							</div>
						</div>
					</div>
					
					<div class="debug-otl" id="otl-notAvailable">
						<div class="fmn-orderTypeLocation mod-notAvailable">
							<div class="orderTypeLocation-wrap">
								<span class="n-location">
									<span class="n-icon fmn-icon-forbidden"></span>
									<span class="n-caption">
										<span class="caption-primary">No orders accepted currently.</span>
										<span class="caption-secondary">Please check back later.</span>
									</span>
									<a class="n-link fmn-button m-invert m-smaller" href="">Details</a>
								</span>
							</div>
						</div>
					</div>

<!--

			<div class="fmn-breadcrumbs">
				<a href="dynamic.php?page=root" class="i-breadcrumb m-link">Root</a>
				<span class="i-breadcrumb m-separator">/</span>
				<a href="dynamic.php?page=branches" class="i-breadcrumb m-link">Branches</a>
				<span class="i-breadcrumb m-separator">/</span>
				<a href="dynamic.php?page=locations" class="i-breadcrumb m-link">Locations</a>
				<span class="i-breadcrumb m-separator">/</span>
				<a href="dynamic.php?page=start" class="i-breadcrumb m-link">Start</a>
				<span class="i-breadcrumb m-separator">/</span>
				<a href="dynamic.php?page=menus" class="i-breadcrumb m-link">Menus</a>
				<span class="i-breadcrumb m-separator">/</span>
				<a href="dynamic.php?page=categories" class="i-breadcrumb m-link">Categories</a>
				<span class="i-breadcrumb m-separator">/</span>
				<a href="dynamic.php?page=dishes" class="i-breadcrumb m-link">Dishes</a>
				<span class="i-breadcrumb m-separator">/</span>
				<span class="i-breadcrumb m-current">Details</span>
			</div>
			
-->
			
			
			

			
		<? } ?>





<!-- 	</div> -->


</div><!-- fmn-header -->


<? /* if ($fmn_page_type == "basket") { ?>

	<div class="fmn-breadcrumbs">
		<a href="dynamic.php?page=menus" class="i-breadcrumb m-link">Back to Menu</a>
	</div>

<? } else {} */ ?>

<?
	if ($fmn_screen_title == "Elements") {
?>		

<div class="fmn-screen-messages">

	<div class="fmn-message">
		<p>basic screen message. <a href="#">they can contain links</a></p>
	</div>
	
	<div class="fmn-message m-success">
		there can be multiple messages. this is a success message
	</div>
	
	<div class="fmn-message m-alert">
		this is an alert message
	</div>

	<div class="fmn-message">
		<p class="e-messageTitle">this is a message title</p>
		<p>this is a sample message with title, copy, details <a href="#">and link</a></p>
		<p class="e-disclaimer">message details</p>
	</div>

</div>

<?		
	}
?>





<? } else { ?>

<? } ?>


<? if ($fmn_page_type == "checkout") { ?>

	<div class="fmn-header mod-checkout">
		<div class="fmn-fixedHeaderMat">
		
			<div class="fmn-header-logo">
				<img src="images/sample-logo-pizzaBox.png" />
			</div>
			<div class="fmn-checkoutBack">
				<a href="dynamic.php?page=basket" class="i-link m-link"><span class="link-back">Back to</span> Basket</a>
				<a href="dynamic.php?page=categories" class="i-link m-link"><span class="link-back">Back to</span> Menu</a>
			</div>
			
			</div>
		
	</div>



<? } else { ?>

<? } ?>


