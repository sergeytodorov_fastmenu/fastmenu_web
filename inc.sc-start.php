<div class="fmn-screen-content-wrap m-reducedWidth-twoThirds">
	
	<div class="fmn-start">
	
		<div class="fmn-blocks m-straightLinks m-withArrow m-iconTop m-threeCols">

			<div class="i-block-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-block">
					<span class="e-icon fmn-icon-delivery"></span><span class="e-icon fmn-icon-take-away"></span>
					<h4>Start a Delivery or <span class="c-nobr">Take-away Order</span></h4>
					<span class="e-rightArrow"><span class="e-icon fmn-icon-arrow-right"></span></span>
				</a>
			</div>
			
			<div class="i-block-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-block">
					<span class="e-icon fmn-icon-menu"></span><span class="e-icon fmn-icon-pre-order"></span>
					<h4>Start a Pre-order <span class="c-nobr">with Reservation</span></h4>
					<span class="e-rightArrow"><span class="e-icon fmn-icon-arrow-right"></span></span>
				</a>
			</div>
			
			<div class="i-block-wrap">
				<a href="<?=$fmn_next_page_url?>" class="i-block">
					<span class="e-icon fmn-icon-office"></span>
					<h4>Start a Company <span class="c-nobr">Delivery Order</span></h4>
					<span class="e-rightArrow"><span class="e-icon fmn-icon-arrow-right"></span></span>
				</a>
			</div>
			
		</div><!-- fmn-blocks -->

	</div>

</div>
