<div class="fmn-overlay fmn-otl mod-new">
	
	<div class="fmn-overlay-dimmer"></div>
	
	<div class="fmn-overlay-box m-smart-scroll">

		<div class="overlay-header">
			<h3 class="header-title">Order Type & Location</h3>
			<a href="#" class="header-close"><span class="fmn-icon-delete"></span></a>
		</div>
		<div class="overlay-content">



			<div class="fmn-form">
				
				<div class="otl-fixWidth fmn-form-option">
					<label>Order Type</label>
					<div class="fmn-form-buttonArray m-fullWidth m-2">
						<a class="fmn-button m-invert" href="dynamic.php?page=otlDeliveryNotLoggedIn"><span>Delivery</span></a>
						<a class="fmn-button m-invert m-selected" href="#"><span>Take-away</span></a>
					</div>
				</div>
				
				<div class="fmn-form-radioArray">
					
					<label for="radio1" class="array-value fmn-address otl-fixWidth" id="fmn-address-1" onclick="fmnSetCurrentAddress('1');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
<!-- 								<input type="radio" id="radio1" name="address"> -->
									<span class="fm-radio">
										<label for="radio1">
											<input type="radio" name="address" id="radio1"/>
											<span class="fm-radio-icon mod-empty"></span>
											<span class="fm-radio-icon mod-checked"><i></i></span>
										</label>
									</span>
							</span>
							<label class="fieldAndLabel-label" for="radio1">
								<span class="label-title">Flying Lotus</span>
								<span class="label-details">7, Main Street, Taito, Tokyo, Japan, accepts orders between 09:30 — 20:00</span>
							</label>
						</div>
					</label>
					<label for="radio3" class="array-value fmn-address otl-fixWidth" id="fmn-address-3" onclick="fmnSetCurrentAddress('3');">
						<div class="value-fieldAndLabel">
							<span class="fieldAndLabel-field">
								<span class="fm-radio">
									<label for="radio3">
										<input type="radio" name="address" id="radio3"/>
										<span class="fm-radio-icon mod-empty"></span>
										<span class="fm-radio-icon mod-checked"><i></i></span>
									</label>
								</span>

							
							</span>
							<label class="fieldAndLabel-label" for="radio3">
								<span class="label-title">Green Fish</span>
								<span class="label-details">
									7, Second Street, Shibuya, Tokyo, Japan accepts orders between 09:00 — 18:00
								</span>
							</label>
						</div>
					</label>
					
					
				</div>
										
			</div>
			
			
			
		</div><!-- overlay-content -->
		<div class="overlay-footer">
			<div class="fmn-actions m-right mod-overlay">
				<a href="#">Cancel</a>
				<input type="submit" class="m-large" value="Confirm">
			</div><!-- fmn-actions -->		
		</div><!-- overlay-footer -->

	</div><!-- fmn-overlay-box -->

</div><!-- fmn-overlay -->
