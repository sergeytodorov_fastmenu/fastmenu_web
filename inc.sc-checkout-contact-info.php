	<div class="fmn-screen-content-wrap m-reducedWidth-oneThird m-checkout m-contactInfo">
		
		<div class="fmn-form">
			
			<div class="fmn-form-option-columns">
				<div class="fmn-form-option-column fmn-oneHalf">
					<div class="fmn-form-option">
						<label>First Name</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
				</div>
				<div class="fmn-form-option-column fmn-oneHalf">
					<div class="fmn-form-option">
						<label>Last Name</label>
						<div class="fmn-form-field">
							<input type="text">
						</div>
					</div>
				</div>
			</div>
			
<!--
			<div class="fmn-form-option">
				<label>
					Contact Name
					<span class="fmn-form-option-details">We'll need to know who we're delivering to</span>
				</label>
				<div class="fmn-form-field">
					<input type="text">
				</div>
			</div>
-->
			
			<div class="fmn-form-option">
				<label>Phone Number</label>
				<div class="fmn-form-field">
					<input type="text">
				</div>
				<span class="fmn-form-option-help">please follow format XXX XXX XXXX</span>
			</div>
			
			<div class="fmn-form-option">
				<label>Email Address</label>
				<div class="fmn-form-field">
					<input type="text">
				</div>
			</div>
			
			<div class="fmn-form-option">
				<span class="fm-checkbox">
					<label for="check1">
						<input type="checkbox" name="1" id="check1" checked=""/>
						<span class="fm-checkbox-icon mod-empty"></span>
						<span class="fm-checkbox-icon mod-checked"><i></i><i></i></span>
						<span class="fm-checkbox-label">Replace the contact info in my account</span>
					</label>
				</span>
<!--
				<div class="i-basic-value">
					<label><input type="checkbox"> Replace the contact info in my account</label>
				</div>
-->
			</div>
					
			
		</div><!-- fmn-form -->
	
	</div><!-- fmn-screen-content-wrap -->
	<div class="fmn-actions m-right">
		
<!-- 		<button class="m-large" value="Checkout">Next</button> -->
		<a  href="<?=$fmn_next_page_url?>" class="fmn-button m-large">Next: Payment</a>
		
	</div><!-- fmn-actions -->

