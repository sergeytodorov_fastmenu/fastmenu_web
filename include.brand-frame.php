<div class="fmn-brand-frame">
	<div class="fmn-fixWidth">
		<div class="fmn-brand-frame-inner">
		
		<div class="n-brand-frame-left">
			
			<div class="fmn-brand-logo">
				
				
				
<svg class="n-logo" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 160 160" enable-background="new 0 0 160 160" xml:space="preserve">
<g>
	<g>
		<circle cx="80" cy="80" r="80"/>
		<g>
			<path fill="#FFFFFF" d="M125.7,76.8H72.2c-1.4,4-3,7.8-4.8,11.4h30.9V78l8.8,0.5c-0.1,0.7-0.6,1.2-2,1.4v8.3h18.7v6.1h-18.7v22.1
				c0,4.1-1,6-4.2,7c-3.2,1.1-8.7,1.2-17.5,1.2c-0.4-1.9-1.5-4.5-2.4-6.3c3.6,0.1,6.9,0.1,9.5,0.1c7.5,0,7.8-0.2,7.8-2.1v-22H64
				c-6.1,10.2-14,18.6-24.4,25.1c-1.1-1.4-3.7-3.8-5.4-5.1c8.8-5.1,15.8-11.8,21.3-20H36.6v-6.1h22.7c1.9-3.6,3.7-7.4,5.2-11.4h-30
				v-6h32.2c0.9-2.8,1.7-5.6,2.5-8.6H46.9v-5.7h23.7l1.8-8.7H41.3v-5.9h32c0.6-3.4,1.1-6.8,1.5-10.4l9.3,1.5
				c-0.2,0.8-0.9,1.3-2.3,1.4c-0.3,2.5-0.7,5-1.1,7.5h38.9v5.9H79.7c-0.5,3-1.1,5.9-1.7,8.7h35.2v5.7H76.6c-0.7,3-1.5,5.8-2.4,8.6
				h51.5V76.8z M73.8,117.1c-2.9-4-9.1-10.4-14.3-14.8l5.4-3.8c5.1,4.3,11.4,10.3,14.5,14.3L73.8,117.1z"/>
		</g>
	</g>
</g>
</svg>

				
				
				
				

			</div>
			
			<div class="fmn-brand-nav">
				<a class="i-nav-item" href="dynamic.php?page=start">Order</a>
				<a class="i-nav-item" href="dynamic.php?page=events">Events</a>
				<a class="i-nav-item" href="dynamic.php?page=promos">Promos</a>
			</div>
			
		</div>
		<div class="n-brand-frame-right">

			<div class="fmn-languages">
				<a class="i-language m-selected" href="#"><img src="images/flag-bulgarian.png"></a>
				<a class="i-language" href="#"><img src="images/flag-english.png"></a>
				<a class="i-language" href="#"><img src="images/flag-german.png"></a>
			</div>
			
		</div>
		
		</div>
	</div>
</div>